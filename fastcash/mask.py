from collections import namedtuple

Mask = namedtuple("Mask", [
    ### attributes
    "mask",
    "tuple",
    "k",
    "w",
    ])

def check_mask(mask, symmetric=True, k=0, w=0):
    if not isinstance(mask, str):
        raise TypeError(f"mask must be of type str, not {type(mask)}")
    if symmetric and not mask == mask[::-1]:
        raise ValueError(f"mask '{mask}'' is not symmetric")
    if not (mask[0] == '#' and mask[-1] == '#'):
        raise ValueError(f"first and last characters of mask '{mask}' must be '#'")
    if k > 0 and mask.count('#') != k:
        raise ValueError(f"mask '{mask}' does not have k={k} #s.")
    if w > 0 and len(mask) != w:
        raise ValueError(f"mask '{mask}' does not have width w={w}.")

def contiguous_mask(k):
    return "".join("#" for i in range(k))

def mask_to_tuple(mask, symmetric=True):
    check_mask(mask, symmetric=symmetric)
    return tuple([i for i, c in enumerate(mask) if c == '#'])

def tuple_to_mask(tmask, symmetric=True):
    w = max(tmask) + 1
    k = len(tmask)
    mask = "".join(['#' if i in tmask else '_' for i in range(w)])
    check_mask(mask, symmetric=symmetric, k=k, w=w)
    return mask

def create_mask(shape):
    mask = dict()
    if isinstance(shape, int): # contiguous k-mer
        mask["k"] = shape
        mask["w"] = shape
        mask["mask"] = contiguous_mask(shape)
        mask["tuple"] = mask_to_tuple(mask["mask"])

    elif isinstance(shape, str): # mask
        mask["mask"] = shape
        mask["tuple"] = mask_to_tuple(shape)
        mask["w"] = len(mask["mask"])
        mask["k"] = len(mask["tuple"])
    elif isinstance(shape, tuple): # tuple
        mask["tuple"] = shape
        mask["mask"] = tuple_to_mask(mask["tuple"])
        mask["w"] = len(mask["mask"])
        mask["k"] = len(mask["tuple"])
    else:
        assert False, "Wrong k-mer format"

    check_mask(mask["mask"], k=mask["k"], w=mask["w"])
    return Mask(**mask)



