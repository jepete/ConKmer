"""
conkmer.values.strongunique

bit 0 (value 1): counter bit
0 = occurs once (unique)
1 = occurs twice or more (non-unique)

bit 1 (value 2): weak bit
0 = no Hamming distance 1 neighbor
1 = Hamming distance 1 neighbor exists

Therefore the following values exist:
00: strongly unique
01: non-unique (but no Hamming-distance-1 neighbor)
10: weakly unique
11: non-unique (and has a Hamming-distance-1 neighbor)
(what we want is 0)
"""


from collections import namedtuple

import numpy as np
from numba import njit, uint64, int64, boolean


ValueInfo = namedtuple("ValueInfo", [
    "NVALUES",
    "RCMODE",
    "value_from_name",
    "update",
    "bits",
    ])


def initialize():
    bits = 2
    rcmode = "max"
    nvalues = 1 << int(bits)

    @njit(nogil=True, inline='always', locals=dict(
            old=uint64, new=uint64, result=uint64))
    def update(old, new):
        """
        update(uint64, uint64) -> uint64
        Update old value (stored) with a new value (from current seq.).
        Return upated value.
        """
        # an update with another value of 0 should set the 1-bit.
        result = old | (new if new else uint64(1))
        return result

    # we always store a value of 0 initially (if k-mer is seen first)
    value_from_name = lambda name, onetwo=1: 0

    return ValueInfo(
        NVALUES = nvalues,
        RCMODE = rcmode,
        value_from_name = value_from_name,
        update = update,
        bits = bits
        )
