from snakemake.shell import shell

extra = snakemake.params.get("extra", "")
log = snakemake.log_fmt_shell(stdout=True, stderr=True)

shell(
    "(OMP_NUM_THREADS={snakemake.threads} delly cnv"
    " -o {snakemake.output}"
    " --genome {snakemake.input.ref}"
    " {extra}"
    " --mappability {snakemake.input.mappability_map}"
    " {snakemake.input.alns}"
    " ) {log}"
)
