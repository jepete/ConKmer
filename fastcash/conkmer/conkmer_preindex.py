"""
conkmer_preindex:
Build index of all k-mers from reference genome(s) and mark strongly unique k-mers.
"""
import sys
from os.path import exists
from importlib import import_module
import numpy as np

from .conkmer_weak import mark_weak_kmers
from ..mathutils import print_histogram, print_histogram_tail
from ..srhash import get_npages
from ..hashio import save_hash
from ..parameters import get_valueset_parameters, parse_parameters
from ..builders import build_from_fasta
from ..builders_subtables import parallel_build_from_fasta
from ..lowlevel.debug import define_debugfunctions
debugprint, timestamp = define_debugfunctions(debug=True, times=True)


DEFAULT_HASHTYPE = "3c_fbcbvb"


# build preindex #########################################

def build_index_fasta(args):
    buildtime = timestamp(msg='Start to build and fill preindex')
    # obtain the parameters
    # dest="kmersize" for argument --mask in fastcash_main.py
    P = get_valueset_parameters(args.valueset, shp=args.kmersize, rcmode=args.rcmode)
    (values, valuestr, rcmode, mask, parameters) = P
    k = mask.k
    shp = mask.tuple

    if not isinstance(k, int):
        print(f"Error: k-mer size k not given; k={k}")
        sys.exit(1)

    debugprint(f"# Imported value set '{valuestr}'.")
    debugprint(f"# Dataset parameters: {parameters}")
    parameters = parse_parameters(parameters, args)
    debugprint(f"# Updated parameters: {parameters}")
    (nobjects, hashtype, aligned, hashfuncs, pagesize, nfingerprints, fill) = parameters

    # create the hash table
    subtables = args.subtables
    if hashtype == "default":
        hashtype = DEFAULT_HASHTYPE
    if subtables:
        if not hashtype.startswith("s"):
            hashtype = "s" + hashtype  # make it a subtable hash type
    else:
        if hashtype.startswith("s"):
            hashtype = hashtype[1:]  # make it a non-subtable hash type

    hashmodule = import_module("..hash_" + hashtype, __package__)
    debugprint(f"# Use hash type {hashtype}")
    build_hash = hashmodule.build_hash
    universe = int(4**k)
    nvalues = values.NVALUES
    value_update = values.update  # see fastcash/values/strongunique.py
    n = get_npages(nobjects, pagesize, fill) * pagesize
    debugprint(f"# Allocating hash table for {n:,} objects, functions '{hashfuncs}'...")
    h = build_hash(universe, n, subtables, pagesize,
                   hashfuncs, nvalues, value_update,
                   aligned=aligned, nfingerprints=nfingerprints,
                   maxwalk=args.maxwalk, shortcutbits=args.shortcutbits)
    print(f"# Memory for hash table: {h.mem_bytes/(2**20):.3f} MB")
    debugprint(f"# Info:  rcmode={rcmode}, walkseed={args.walkseed}")

    # fill the hash table: store all k-mers from genome
    if h.subtables:
        build = parallel_build_from_fasta
    else:
        build = build_from_fasta

    value_from_name = values.value_from_name  # see fastcash/values/strongunique.py
    (total, failed, walkstats) = build(
        args.genome, shp, h, value_from_name,
        rcmode=rcmode, walkseed=args.walkseed, maxfailures=args.maxfailures)

    if failed:
        return (h, total, failed, walkstats, valuestr, mask, rcmode)

    timestamp(buildtime, msg="Time to build and fill preindex")

    # calculate shortcut bits
    calc_shortcutbits = h.compute_shortcut_bits(h.hashtable)
    if args.shortcutbits > 0:
        startshort = timestamp(msg=f'Begin calculating shortcut bits ({args.shortcutbits})...')
        calc_shortcutbits(h.hashtable)
        timestamp(msg='Done calculating shortcut bits.')
        timestamp(startshort, msg="Time calculating shortcut bits")

    # mark weak k-mers (there is at least one k-mer in this reference with a hamming distance of 1)
    debugprint(f'# Number of threads for computing weak k-mers: {args.threads}')
    mark_weak_kmers(h, args)  # call goes to conkmer_weak.py and from there to xengsort_weak.py

    return (h, total, failed, walkstats, valuestr, mask, rcmode)


def main(args):
    starttime = timestamp(msg="Stating ConKmer preindex...")
    for g in args.genome:
        assert exists(g), f"It seems that the genome file {g} does not exist."
    assert 0 < args.fill < 1, f"Desired fill rate of the hash table needs to be between 0 and 1,"\
                              f" but {args.fill=}"
    source = f"FASTA(s): {args.genome}"
    preindex_time = timestamp(msg=f"Building preindex '{args.preindex}' from {source}.")
    (h, total, failed, walkstats, valueset, mask, rcmode) = build_index_fasta(args)
    k = mask.k
    shp = mask.tuple

    if failed == 0:  # 'not failed' should do the same
        timestamp(preindex_time, msg=f"SUCCESS, processed {total:,} {k}-mers of shape {shp} in")
        preindex_writing_time = timestamp(msg=f"Writing preindex file'{args.preindex}'...")
        save_hash(args.preindex, h, valueset, additional=dict(k=k, shp=shp, walkseed=args.walkseed,
                                                              rcmode=rcmode, fillrate=args.fill,
                                                              valueset=valueset))
        timestamp(preindex_writing_time, msg="Finished writing of preindex file to disk")
    else:
        timestamp(preindex_time, msg=f"FAILED for {failed:,}/{total:,} processed k-mers")
        print(f"WARNING: Preindex file '{args.preindex}'' will NOT be written.")

    show_statistics = not args.nostatistics
    if show_statistics:
        statstime = timestamp(msg="Collecting statistics...\n")
        valuehist, fillhist, choicehist, shortcuthist = h.get_statistics(h.hashtable)
        if args.longwalkstats:
            print_histogram(walkstats, title="Walk length statistics:", shorttitle="walk",
                            fractions=".", average=True)
        else:
            print_histogram_tail(walkstats, [1, 2, 10], title="Extreme walk lengths:",
                                 shorttitle="walk", average=True)
        print("# Combined statsitcs for all subtables")
        print_histogram(np.sum(valuehist, axis=0), title="Value statistics:",
                        shorttitle="values", fractions="%")
        print_histogram(np.sum(fillhist, axis=0), title="Page fill statistics:",
                        shorttitle="fill", fractions="%", average=True, nonzerofrac=True)
        print_histogram(np.sum(choicehist, axis=0), title="Choice statistics:",
                        shorttitle="choice", fractions="%+", average="+")
        if args.shortcutbits > 0:
            print_histogram(np.sum(shortcuthist, axis=0), title="shortcut bits statistics:",
                            shorttitle="shortcutbits", fractions="%")
        timestamp(statstime, msg="Seconds needed for statistics")

    timestamp(starttime, msg="total time [sec]")
    timestamp(starttime, msg="total time [min]", minutes=True)
    timestamp(msg="Done.")
    if failed:
        print("FAILED! see above messages!")
        sys.exit(1)
