import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Patch
from matplotlib.collections import BrokenBarHCollection
from numba import njit, uint32
from fastcash.zarrutils import load_from_zgroup
from fastcash.lowlevel.intbitarray import intbitarray
from fastcash.lowlevel.debug import define_debugfunctions
debugprint, timestamp = define_debugfunctions(debug=True, times=True)


def make_local_gapstatistics(get, goodstate, acceptable_gaplength):
    """
    :param get: getter function of the namedtuple bitvector
    :param goodstate: all values >= goodstate in the bitvectors are considered as 'good'
                      (e.g. [strongly] unique)
    :param acceptable_gaplength: max. gap length between values >= goodstate such that all positions
                            in a gap with length <= acceptable_gaplength are considered as 'good'
                            (typically k-1)
    """
    @njit(locals=dict(counter=uint32, good_counter=uint32,
                      current_gaplen=uint32, max_gaplen=uint32))
    def local_gapstatistics(ba, start, end, results):
        """
        :param ba: bitvector array
        :param start: start position
        :param end: end position
        :param results: numpy array with three positions to store number of 'good' positions,
                        the maximal gap length of non-'good' positions and the number of
                        positions covered by at least one k-mer of length acceptable_gaplength+1
        """
        counter = 0
        good_counter = 0
        current_gaplen = 0
        max_gaplen = 0
        for i in range(start, end):
            kmerstate = get(ba, i)
            if kmerstate >= goodstate:
                counter += 1
                if current_gaplen <= acceptable_gaplength:
                    good_counter += current_gaplen
                if current_gaplen > max_gaplen:
                    max_gaplen = current_gaplen
                current_gaplen = 0
            else:
                current_gaplen += 1
        if current_gaplen > max_gaplen:
            max_gaplen = current_gaplen
        results[0] = counter
        results[1] = max_gaplen
        results[2] = counter + good_counter
    return local_gapstatistics


def plot_unique_kmers(goodstate):
    """
    :param goodstate: all values >= goodstate in the bitvectors are considered as 'good' (e.g. [strongly] unique)
    """
    info = load_from_zgroup(snakemake.input.bitvectors, 'info')
    names_to_sizes = info['names_to_sizes']
    bitvectors = load_from_zgroup(snakemake.input.bitvectors, 'onebit')
    ideo = pd.DataFrame(columns=['chrom', 'start', 'end', 'percentageStrUnique', 'largestGap', 'percInclAcceptableGaps'])

    for name, array in bitvectors.items():
        # skip scaffolds etc., adapted to naming scheme of ensembl release 106
        if ('_' in name) or ('.' in name and 'U00096' not in name) or ('M' in name) or ('hs37d5' in name):
            continue
        size = names_to_sizes[name]
        b = intbitarray(size, 1, init=array)
        name = name.replace('chr', '')  # delete 'chr' from sequence / chromosome names
        w = int(snakemake.wildcards.k[-2:])  # assuming that gapped k-mers are noted as e.g. 27w39
        local_gapstatistics = make_local_gapstatistics(b.get, goodstate, w-1)
        stepsize = snakemake.params.stepsize
        for startpos in range(0, b.size, stepsize):
            endpos = startpos+stepsize
            results = np.zeros(3, dtype=np.uint32)
            if endpos >= b.size:
                # special treatment for last interval < stepsize
                endpos = b.size
                stepsize = b.size - startpos
            local_gapstatistics(b.array, startpos, endpos, results)
            percentageStrUnique = results[0] / stepsize
            percentageGood = results[2] / stepsize
            # append new line to ideo for interval from startpos to endpos
            ideo.loc[len(ideo)] = [name, startpos, endpos, percentageStrUnique, results[1], percentageGood]
    return ideo


def chromosome_collections(df, y_positions, height, color_column_id):
    """
    based on https://www.biostars.org/p/147364/

    Yields BrokenBarHCollection of features that can be added to an Axes object.

    :param df (pandas.DataFrame): Must at least have columns ['chrom', 'start', 'end', 'color'].
                                  If no column 'width', it will be calculated from start/end.
    :param y_positions (dict): keys are chromosomes, values are y-value at which to anchor
                               the BrokenBarHCollection
    :param height (float): height of each BrokenBarHCollection
    """
    if 'width' not in df.columns:
        df['width'] = df['end'] - df['start']
    for chrom, group in df.groupby('chrom'):
        yrange = (y_positions[chrom], height)
        xranges = group[['start', 'width']].values
        yield BrokenBarHCollection(xranges, yrange, facecolors=group[color_column_id])


def plot_ideograms(ideo, legend, legend_title, filename, color_column_id):
    """
    :param ideo: data frame containing at least the first three columns 'chrom', 'start', 'end'
                 and a corresponding color column whose column index is given as 'color_column_id'
    :param legend: legend to plot (typically an arrangement of Patches)
    :param legend_title: title of the legend
    :param filename: file path to store the resulting plot at
    :param color_column_id: index of the column in 'ideo' where the color names to use are stored
    """
    chrom_height = 1  # height of each ideogram
    chrom_spacing = 0.5  # spacing between consecutive ideograms
    # Decide which chromosomes to use
    chromosome_list = ['%s' % i for i in range(1, 23)] + ['X', 'Y']
    # Keep track of the y positions for ideograms and the center of each ideogram (which is where we'll put the ytick labels)
    ybase = 0
    chrom_ybase = {}
    chrom_centers = {}
    # Iterate in reverse so that items in the beginning of `chromosome_list` will appear at the top of the plot
    for chrom in chromosome_list[::-1]:
        chrom_ybase[chrom] = ybase
        chrom_centers[chrom] = ybase + chrom_height / 2
        ybase += chrom_height + chrom_spacing
    fig = plt.figure(figsize=(8.5, 5), tight_layout=True)  # plot width, height (in inches)
    ax = fig.add_subplot(111)
    plt.margins(x=0.005, y=0.01)
    ax.legend(handles=legend, loc='lower right', title=legend_title, prop={'family': 'DejaVu Sans Mono'})
    for collection in chromosome_collections(ideo, chrom_ybase, chrom_height, color_column_id):
        ax.add_collection(collection)
    # Axes tweaking
    ax.set_yticks([chrom_centers[i] for i in chromosome_list])
    ax.set_yticklabels(chromosome_list)
    ax.axis('tight')
    # plt.yticks(rotation=180)
    plt.ylabel("chromosome")
    # plt.xticks(rotation=90)
    plt.xlabel("position")  # , rotation=180)
    plt.savefig(filename, transparent=True)


def prepare_ideograms(ideo):
    """
    :param ideo: data frame containing at least the first three columns 'chrom', 'start', 'end'
                 plus some data to plot
    """
    # Add a new column for width
    ideo['width'] = ideo.end - ideo.start
    # https://matplotlib.org/2.0.2/examples/color/named_colors.html
    color_lookup_perc = {
        '0': 'darkred',
        '10': 'firebrick',
        '20': 'red',
        '30': 'orange',  # alternatives: darkorange, orangered
        '40': 'gold',
        '50': 'yellow',
        '60': 'greenyellow',
        '70': 'limegreen',
        '80': 'green',
        '90': 'darkgreen',
        '100': 'blue'
    }
    color_column_id_perc = 'colors_perc'
    ideo[color_column_id_perc] = ideo['percentageStrUnique'].apply(lambda x: 'black' if x < 0.000000001 else color_lookup_perc[str(int(x*10)*10)])
    # print("\nFrequency of color values for k-mer percentages:")
    valueCounts_perc = ideo[color_column_id_perc].value_counts()
    # print(valueCounts_perc)
    # Number of spaces might need to be adjusted:  # TODO: Better solution
    legend_perc = [Patch(color='blue', label=f"100           {(valueCounts_perc['blue'] if 'blue' in valueCounts_perc else 0):,} x"),
                   Patch(color='darkgreen', label=f"[90,100)      {(valueCounts_perc['darkgreen'] if 'darkgreen' in valueCounts_perc else 0):,} x"),
                   Patch(color='green', label=f"[80, 90)     {(valueCounts_perc['green'] if 'green' in valueCounts_perc else 0):,} x"),
                   Patch(color='limegreen', label=f"[70, 80)  {(valueCounts_perc['limegreen'] if 'limegreen' in valueCounts_perc else 0):,} x"),
                   Patch(color='greenyellow', label=f"[60, 70)  {(valueCounts_perc['greenyellow'] if 'greenyellow' in valueCounts_perc else 0):,} x"),
                   Patch(color='yellow', label=f"[50, 60)  {(valueCounts_perc['yellow'] if 'yellow' in valueCounts_perc else 0):,} x"),
                   Patch(color='gold', label=f"[40, 50)    {(valueCounts_perc['gold'] if 'gold' in valueCounts_perc else 0):,} x"),
                   Patch(color='orange', label=f"[30, 40)    {(valueCounts_perc['orange'] if 'orange' in valueCounts_perc else 0):,} x"),
                   Patch(color='red', label=f"[20, 30)    {(valueCounts_perc['red'] if 'red' in valueCounts_perc else 0):,} x"),
                   Patch(color='firebrick', label=f"[10, 20)    {(valueCounts_perc['firebrick'] if 'firebrick' in valueCounts_perc else 0):,} x"),
                   Patch(color='darkred', label=f"( 0, 10)    {(valueCounts_perc['darkred'] if 'darkred' in valueCounts_perc else 0):,} x"),
                   Patch(color='black', label=f"  0         {(valueCounts_perc['black'] if 'black' in valueCounts_perc else 0):,} x")]

    color_column_id_perc_acc = 'colors_perc_acc'
    ideo[color_column_id_perc_acc] = ideo['percInclAcceptableGaps'].apply(lambda x: 'black' if x < 0.000000001 else color_lookup_perc[str(int(x*10)*10)])
    # print("\nFrequency of color values for k-mer percentages including acceptable gaps:")
    valueCounts_perc_acc = ideo[color_column_id_perc_acc].value_counts()
    # print(valueCounts_perc_acc)
    legend_perc_acc = [Patch(color='blue', label=f"100           {(valueCounts_perc_acc['blue'] if 'blue' in valueCounts_perc_acc else 0):,}"),
                       Patch(color='darkgreen', label=f"[90,100)  {(valueCounts_perc_acc['darkgreen'] if 'darkgreen' in valueCounts_perc_acc else 0):,}"),
                       Patch(color='green', label=f"[80, 90)  {(valueCounts_perc_acc['green'] if 'green' in valueCounts_perc_acc else 0):,}"),
                       Patch(color='limegreen', label=f"[70, 80)  {(valueCounts_perc_acc['limegreen'] if 'limegreen' in valueCounts_perc_acc else 0):,}"),
                       Patch(color='greenyellow', label=f"[60, 70)    {(valueCounts_perc_acc['greenyellow'] if 'greenyellow' in valueCounts_perc_acc else 0):,}"),
                       Patch(color='yellow', label=f"[50, 60)    {(valueCounts_perc_acc['yellow'] if 'yellow' in valueCounts_perc_acc else 0):,}"),
                       Patch(color='gold', label=f"[40, 50)    {(valueCounts_perc_acc['gold'] if 'gold' in valueCounts_perc_acc else 0):,}"),
                       Patch(color='orange', label=f"[30, 40)     {(valueCounts_perc_acc['orange'] if 'orange' in valueCounts_perc_acc else 0):,}"),
                       Patch(color='red', label=f"[20, 30)     {(valueCounts_perc_acc['red'] if 'red' in valueCounts_perc_acc else 0):,}"),
                       Patch(color='firebrick', label=f"[10, 20)    {(valueCounts_perc_acc['firebrick'] if 'firebrick' in valueCounts_perc_acc else 0):,}"),
                       Patch(color='darkred', label=f"( 0, 10)  {(valueCounts_perc_acc['darkred'] if 'darkred' in valueCounts_perc_acc else 0):,}"),
                       Patch(color='black', label=f"  0         {(valueCounts_perc_acc['black'] if 'black' in valueCounts_perc_acc else 0):,}")]

    color_column_id_maxGap = 'colors_gaps'
    ideo[color_column_id_maxGap] = ideo['largestGap'].apply(lambda x: 'black' if x == snakemake.params.stepsize
                                                            else 'darkgreen' if x < 250
                                                            else 'green' if x < 500
                                                            else 'limegreen' if x < 1000
                                                            else 'greenyellow' if x < 2000
                                                            else 'yellow' if x < 3000
                                                            else 'gold' if x < 5000
                                                            else 'orange' if x < 10000
                                                            else 'red' if x < 25000
                                                            else 'firebrick' if x < 50000
                                                            else 'darkred')
    # print("\nFrequency of color values for largest gap:")
    valueCounts_maxGap = ideo[color_column_id_maxGap].value_counts()
    # print(valueCounts_maxGap)
    stats = ideo.describe()
    # print(stats)
    title_gaps = f"maximal gap size in each\n200 Kbp block in bp\n(mean: {stats['largestGap']['mean']:,.1f}; median: {int(stats['largestGap']['50%']):,})"
    legend_gaps = [Patch(color='darkgreen', label=f"      0-    249      {(valueCounts_maxGap['darkgreen'] if 'darkgreen' in valueCounts_maxGap else 0):,} x"),
                   Patch(color='green', label=f"    250-    499  {(valueCounts_maxGap['green'] if 'green' in valueCounts_maxGap else 0):,} x"),
                   Patch(color='limegreen', label=f"    500-    999  {(valueCounts_maxGap['limegreen'] if 'limegreen' in valueCounts_maxGap else 0):,} x"),
                   Patch(color='greenyellow', label=f"  1,000-  1,999  {(valueCounts_maxGap['greenyellow'] if 'greenyellow' in valueCounts_maxGap else 0):,} x"),
                   Patch(color='yellow', label=f"  2,000-  2,999  {(valueCounts_maxGap['yellow'] if 'yellow' in valueCounts_maxGap else 0):,} x"),
                   Patch(color='gold', label=f"  3,000-  4,999  {(valueCounts_maxGap['gold'] if 'gold' in valueCounts_maxGap else 0):,} x"),
                   Patch(color='orange', label=f"  5,000-  9,999    {(valueCounts_maxGap['orange'] if 'orange' in valueCounts_maxGap else 0):,} x"),
                   Patch(color='red', label=f" 10,000- 24,999    {(valueCounts_maxGap['red'] if 'red' in valueCounts_maxGap else 0):,} x"),
                   Patch(color='firebrick', label=f" 25,000- 49,999    {(valueCounts_maxGap['firebrick'] if 'firebrick' in valueCounts_maxGap else 0):,} x"),
                   Patch(color='darkred', label=f" 50,000-199,999    {(valueCounts_maxGap['darkred'] if 'darkred' in valueCounts_maxGap else 0):,} x"),
                   Patch(color='black', label=f"200,000            {(valueCounts_maxGap['black'] if 'black' in valueCounts_maxGap else 0):,} x")]

    mean_median = f"(mean: {(stats['percentageStrUnique']['mean']*100):.2f}; median: {(stats['percentageStrUnique']['50%']*100):.2f})"
    title_perc = f"proportion of strongly unique\n{snakemake.wildcards.k}-mers in each 200 Kbp block in %\n{mean_median}"
    mean_median_acc = f"mean: {(stats['percInclAcceptableGaps']['mean']*100):.2f} %\nmedian: {(stats['percInclAcceptableGaps']['50%']*100):.2f} %"
    title_perc_acc = f"{mean_median_acc}\n\nvalues in %  |  # occurences"
    plot_ideograms(ideo, legend_perc, title_perc, snakemake.output.ideogram_perc, color_column_id_perc)
    plot_ideograms(ideo, legend_perc_acc, title_perc_acc, snakemake.output.ideogram_perc_acc, color_column_id_perc_acc)
    plot_ideograms(ideo, legend_gaps, title_gaps, snakemake.output.ideogram_gaps, color_column_id_maxGap)


def dummyplot(filename):
    warning = "E. coli is not yet supported for this kind of plot."
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.text(0.1, 1, warning, fontsize=14)
    plt.savefig(filename)


def main():
    if snakemake.wildcards.refname == "ecoli":
        # This should be avoided
        dummyplot(snakemake.output.ideogram_perc)
        dummyplot(snakemake.output.ideogram_perc_acc)
        dummyplot(snakemake.output.ideogram_gaps)
        return
    ideo = plot_unique_kmers(1)
    prepare_ideograms(ideo)


if __name__ == "__main__":
    main()
