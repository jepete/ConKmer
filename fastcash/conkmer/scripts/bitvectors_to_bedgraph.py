import os
from fastcash.lowlevel.intbitarray import intbitarray
from fastcash.lowlevel.debug import define_debugfunctions
from fastcash.zarrutils import load_from_zgroup
debugprint, timestamp = define_debugfunctions(debug=True, times=True)


def write_bedgraph_files():
    # Load bitvectors from index step
    bitvectors = load_from_zgroup(snakemake.input.bitvectors, 'onebit')
    info = load_from_zgroup(snakemake.input.bitvectors, 'info')
    names_to_sizes = info['names_to_sizes']

    is_ecoli = "ecoli" in snakemake.params.refname

    if not os.path.isdir(snakemake.output.folder):
        os.makedirs(snakemake.output.folder)

    for name, array in bitvectors.items():
        chrom = ""
        if not is_ecoli:  # skip scaffolds etc.
            if ('_' in name) or ('.' in name) or ('M' in name) or ('hs37d5' in name):
                continue
            chrom = "" if "chr" in name else "chr"
        size = names_to_sizes[name]
        bv = intbitarray(size, 1, init=array)  # it was bitarray, but bitarray constructor does not support init yet

        timestamp(msg=f"Processing sequence {name} with size {size:,} ...")
        bedGraph_outname = f"{snakemake.output.folder}/{chrom}{name}.bedgraph"

        with open(bedGraph_outname, "w") as outfile:
            # https://genome.ucsc.edu/goldenPath/help/bedgraph.html
            # outfile.write("track type=bedGraph\n") # mandatory but causes problems in IGV
            for i in range(0, size):
                if bv.get(bv.array, i):
                    if is_ecoli:
                        outfile.write(f"{name}\t{i}\t{i+1}\t1\n")
                    else:
                        # IGV needs chr preceding the sequence name
                        outfile.write(f"{chrom}{name}\t{i}\t{i+1}\t1\n")


def main():
    starttime = timestamp(msg="Start bedGraph construction from bit vectors")
    write_bedgraph_files()
    timestamp(starttime, msg="time sec")
    timestamp(starttime, msg="time min", minutes=True)
    timestamp(msg="Done.")


if __name__ == "__main__":
    main()
