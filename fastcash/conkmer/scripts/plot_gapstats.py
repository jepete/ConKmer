import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker


def create_gapstats_plot(input, output):
    tsv = pd.read_csv(input, sep="\t")
    tsv = tsv.iloc[1:, :2]
    scatterplot = tsv.plot.scatter(x='gap', y='frequency', s=2, alpha=0.5,
                                   xlabel="gap length (including Ns)", ylabel="frequency",
                                   logy=True, logx=True, figsize=(9, 5.5), legend=False)
    locmaj = matplotlib.ticker.LogLocator(base=10,numticks=8) 
    scatterplot.yaxis.set_major_locator(locmaj)  # Why does it not work?
    plt.grid(axis='both', linestyle=':')
    plt.tight_layout()
    scatterplot.set_xlim(0.901, 4*(10**7))
    scatterplot.set_ylim(0.901, 4.1*(10**8))
    scatterplot.set_axisbelow(True)
    scatterplot.figure.savefig(output, transparent=True)


def main():
    create_gapstats_plot(snakemake.input.gapstats, snakemake.output.gapplot)


if __name__ == "__main__":
    main()
