import os
import subprocess
import pandas as pd


def main():
    temp_path = snakemake.params.temp_path
    ten_input_files = snakemake.input.files
    final_output = snakemake.output.intersection

    if len(ten_input_files) != 10:
        print(f"This script is specific for pairwise intersection of exactly ten BED-like TSV files down to one final intersection, but {len(ten_input_files)} files were given.")
        return

    if not os.path.isdir(snakemake.params.temp_path):
        os.makedirs(snakemake.params.temp_path)

    # be careful: only the first three columns are intersections, the 4th (CNV size), 5th (int CN) and 6th (float CN) are copied from the source files

    # alternative: use bedtools multiinter and filtering for entries with a 10 / only 1s
    subprocess.run(f"bedtools intersect -a {ten_input_files[0]} -b {ten_input_files[1]} > {temp_path}0_1.tsv", shell=True)
    subprocess.run(f"bedtools intersect -a {ten_input_files[2]} -b {ten_input_files[3]} > {temp_path}2_3.tsv", shell=True)
    subprocess.run(f"bedtools intersect -a {ten_input_files[4]} -b {ten_input_files[5]} > {temp_path}4_5.tsv", shell=True)
    subprocess.run(f"bedtools intersect -a {ten_input_files[6]} -b {ten_input_files[7]} > {temp_path}6_7.tsv", shell=True)
    subprocess.run(f"bedtools intersect -a {ten_input_files[8]} -b {ten_input_files[9]} > {temp_path}8_9.tsv", shell=True)

    subprocess.run(f"bedtools intersect -a {temp_path}0_1.tsv -b {temp_path}2_3.tsv > {temp_path}0_1_2_3.tsv", shell=True)
    subprocess.run(f"bedtools intersect -a {temp_path}4_5.tsv -b {temp_path}6_7.tsv > {temp_path}4_5_6_7.tsv", shell=True)
    subprocess.run(f"bedtools intersect -a {temp_path}0_1_2_3.tsv -b {temp_path}4_5_6_7.tsv > {temp_path}0-7.tsv", shell=True)
    subprocess.run(f"bedtools intersect -a {temp_path}8_9.tsv -b {temp_path}0-7.tsv > {temp_path}0-9.tsv", shell=True)

    subprocess.run(f"""awk 'BEGIN{{FS=OFS="\t"}} {{$3=$3 FS $3-$2; $4=""; $5=""; $6=""}}1' {temp_path}0-9.tsv | sed 's/[ \t]*$//' > {temp_path}0-9_with_correct_sizes.tsv""", shell=True)

    # intersect final_output separatly with every file from ten_input_files using -wo
    dfs = []
    last_len = -1
    for i in range(0, 10):
        subprocess.run(f"bedtools intersect -wo -a {temp_path}0-9_with_correct_sizes.tsv -b {ten_input_files[i]} > {temp_path}{i}.tsv", shell=True)
        subprocess.run(f"wc -l {temp_path}{i}.tsv", shell=True)
        df = pd.read_csv(f"{temp_path}{i}.tsv", sep="\t", header=None)
        if last_len != -1:
            assert last_len == len(df), "All ten files should contain the same number of lines"
        last_len = len(df)
        dfs.append(df)

    # read [0-9].tsv using pandas, check number of lines/rows (hopefully identical) and write those > 50 bp with same CN to final_output
    with open(final_output, "w") as outfile:
        for i in range(0, len(df)):
            for j in range(0, 10):
                assert dfs[j][3][i] == dfs[j][10][i], f"sizes should be equal for {ten_input_files[j]}"
            if dfs[0][8][i] == dfs[1][8][i] == dfs[2][8][i] == dfs[3][8][i] == dfs[4][8][i] == dfs[5][8][i] == dfs[6][8][i] == dfs[7][8][i] == dfs[8][8][i] == dfs[9][8][i]:
                if dfs[0][3][i] > 50 and "X" not in dfs[0][0][i] and "Y" not in dfs[0][0][i]:  # filter out intersection < 50 bp
                    outline = f"{dfs[0][0][i]}\t{dfs[0][1][i]}\t{dfs[0][2][i]}\t{dfs[0][3][i]}\t{dfs[0][8][i]}\n"
                    outfile.write(outline)
                    # if int(snakemake.wildcards.wsize) > 1000 and dfs[0][8][i] > 1: print(outline)

    # read final_output to output some statistics about it
    df_intersection = pd.read_csv(final_output, sep="\t", header=None)
    print(f"\nStatistics of numerical columns of {final_output}:")
    print(df_intersection.describe())
    print("\nValue counts of column chr:")
    print(df_intersection[0].value_counts())
    print("\nValue counts of CN column:")
    print(df_intersection[4].value_counts())


if __name__ == "__main__":
    main()
