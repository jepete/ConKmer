import numpy as np
import matplotlib.pyplot as plt


def create_kmer_coverage_plot(inputfiles, conkmer_bp, conkmer_bp_CNu2, conkmer_intervals,
                              comp_conkmer_delly_allCN_bp, comp_conkmer_cn_too_low_bp,
                              comp_conkmer_delly_CNu2_intervals, comp_conkmer_delly_CNu2_bp,
                              comp_delly_conkmer_CNu2_bp):
    ival_dict = dict()
    alphas = snakemake.params.alphas
    windowsizes = snakemake.params.windowsizes
    alphas_dict = dict()
    for i, alpha in enumerate(alphas):
        alphas_dict[alpha] = i
    for i, z in enumerate(windowsizes):
        ival_dict[z] = i
    conkmer_bp_sum_all_cn = np.zeros((len(windowsizes), len(alphas)), dtype=np.uint32)
    conkmer_bp_sum_cn_unequal2 = np.zeros((len(windowsizes), len(alphas)), dtype=np.uint32)
    conkmer_cnu2_intervals = np.zeros((len(windowsizes), len(alphas)), dtype=np.float32)
    conkmer_all_cn_bp = np.zeros((len(windowsizes), len(alphas)), dtype=np.float32)
    conkmer_cn_too_low_bp = np.zeros((len(windowsizes), len(alphas)), dtype=np.float32)
    conkmer_cnu2_delly_intervals_percentage = np.zeros((len(windowsizes), len(alphas)), dtype=np.float32)
    conkmer_cn_unequal2_bp = np.zeros((len(windowsizes), len(alphas)), dtype=np.float32)
    delly_cn_unequal2_bp = np.zeros((len(windowsizes), len(alphas)), dtype=np.float32)
    for statfile in inputfiles:
        statfile_parts = statfile.split("_")
        ivalsize = int(statfile_parts[6])
        factoris = int(statfile_parts[7])  # bad idea if file names change
        with open(statfile, "r") as file:
            lines = file.read().splitlines()
            assert lines
            while lines:
                line = lines.pop(0)
                if "Sum of the base pairs of all ConKmer intervals (including CN 2): " in line:
                    line_values = line.split(" ")
                    value = int(line_values[-1].replace(',', ''))
                    conkmer_bp_sum_all_cn[ival_dict[ivalsize]][alphas_dict[factoris]] = value
                elif "Sum of the base pairs of all ConKmer intervals with a CN != 2: " in line:
                    line_values = line.split(" ")
                    value = int(line_values[-1].replace(',', ''))
                    conkmer_bp_sum_cn_unequal2[ival_dict[ivalsize]][alphas_dict[factoris]] = value
                elif "Number of ConKmer intervals with a CN != 2: " in line:
                    value = int((line[line.find(start := "Number of ConKmer intervals with a CN != 2: ") + len(start):]).replace(',', ''))
                    conkmer_cnu2_intervals[ival_dict[ivalsize]][alphas_dict[factoris]] = value
                elif "ConKmer CN matches with Delly CN in " in line:  # of all ConKmer bp
                    value = float(line[line.find(start := "ConKmer CN matches with Delly CN in ") + len(start):line.find(" % ")])
                    conkmer_all_cn_bp[ival_dict[ivalsize]][alphas_dict[factoris]] = value
                elif "ConKmer CN too low compared with Delly CN in " in line:  # of all delly_conkmer_intersection bp
                    value = float(line[line.find(start := "ConKmer CN too low compared with Delly CN in ") + len(start):line.find(" % ")])
                    conkmer_cn_too_low_bp[ival_dict[ivalsize]][alphas_dict[factoris]] = value
                elif "ConKmer intervals with CN != 2 supported with the same CN by all intersecting Delly intervals: " in line:
                    value = float(line[line.find(start := "ConKmer intervals with CN != 2 supported with the same CN by all intersecting Delly intervals: ") + len(start):line.find(" %")])
                    conkmer_cnu2_delly_intervals_percentage[ival_dict[ivalsize]][alphas_dict[factoris]] = value
                elif "ConKmer CN != 2 matches with Delly CN in " in line:  # of all ConKmer bp with CN != 2
                    value = float(line[line.find(start := "ConKmer CN != 2 matches with Delly CN in ") + len(start):line.find(" % ")])
                    conkmer_cn_unequal2_bp[ival_dict[ivalsize]][alphas_dict[factoris]] = value
                elif "Delly CN != 2 matches with ConKmer CN in " in line:  # of all Delly bp with CN != 2
                    value = float(line[line.find(start := "Delly CN != 2 matches with ConKmer CN in ") + len(start):line.find(" % ")])
                    delly_cn_unequal2_bp[ival_dict[ivalsize]][alphas_dict[factoris]] = value

    colors = ["#0072B2", "#E69F00", "#009E73", "#D55E00", "#56B4E9", "black"]  # 6 colors for 6 different alphas, increase if necessary
    markers = ['o', 'v', 's', 'h', 'd', '^']  # 6 markers for 6 different alphas, increase if necessary
    labels = alphas  # ['3', '5', '8', '20', '50', 'none']
    figure_size = (9, 4.8)
    marker_size = 6
    legend_title = "window size factor\nfor gap insertion ($\\alpha$)"
    x_label = "window size ($z$)"

    # Yes, this is duplicated code and I am aware of the disadvantages:

    scatterplot_bp = plt.figure(figsize=figure_size, tight_layout=True)
    plt.xscale('symlog')
    for i in range(len(alphas)):
        plt.plot(windowsizes, conkmer_bp_sum_all_cn[:, i], marker=markers[i], markersize=marker_size,
                 color=colors[i], linestyle="--", label=labels[i])
    plt.legend(loc='lower right', title=legend_title)
    # plt.title("bp with a CN assigned by ConKmer")
    plt.xlabel(x_label)
    plt.ylabel("basepairs")
    myxlabels = map(str, windowsizes)
    plt.xticks(ticks=windowsizes, labels=myxlabels)
    plt.yticks(np.arange(1_900_000_000, 3_100_000_001, 100_000_000))
    plt.grid(axis='y', linestyle=':')
    scatterplot_bp.figure.savefig(conkmer_bp, transparent=True)

    scatterplot_bp_CNu2 = plt.figure(figsize=figure_size, tight_layout=True)
    plt.xscale('symlog')
    for i in range(len(alphas)):
        plt.plot(windowsizes, conkmer_bp_sum_cn_unequal2[:, i], marker=markers[i], markersize=marker_size,
                 color=colors[i], linestyle="--", label=labels[i])
    plt.legend(loc='upper right', title=legend_title)
    # plt.title("bp with a CN != 2 assigned by ConKmer")
    plt.xlabel(x_label)
    plt.ylabel("basepairs")
    myxlabels = map(str, windowsizes)
    plt.xticks(ticks=windowsizes, labels=myxlabels)
    plt.yticks(np.arange(0, 1_300_000_001, 100_000_000))
    plt.grid(axis='y', linestyle=':')
    scatterplot_bp_CNu2.figure.savefig(conkmer_bp_CNu2, transparent=True)

    scatterplot_number = plt.figure(figsize=figure_size, tight_layout=True)
    plt.xscale('symlog')
    plt.yscale('symlog')
    for i in range(len(alphas)):
        plt.plot(windowsizes, conkmer_cnu2_intervals[:, i], marker=markers[i], markersize=marker_size,
                 color=colors[i], linestyle="--", label=labels[i])
    plt.legend(loc='upper right', title=legend_title)
    plt.xlabel(x_label)
    plt.ylabel("number of intervals")
    myxlabels = map(str, windowsizes)
    plt.xticks(ticks=windowsizes, labels=myxlabels)
    # plt.yticks(np.arange(2_100_000_000, 3_100_000_001, 100_000_000))
    plt.yticks([100, 1000, 10000, 100000, 1000000, 10000000])
    plt.grid(axis='y', linestyle=':')
    scatterplot_number.figure.savefig(conkmer_intervals, transparent=True)

    scatterplot = plt.figure(figsize=figure_size, tight_layout=True)
    plt.xscale('symlog')
    for i in range(len(alphas)):
        plt.plot(windowsizes, conkmer_all_cn_bp[:, i], marker=markers[i], markersize=marker_size,
                 color=colors[i], linestyle="--", label=labels[i])
    plt.legend(loc='lower right', title=legend_title)
    # plt.title("Delly intervals supported by ConKmer")
    plt.xlabel(x_label)
    plt.ylabel("%")
    myxlabels = map(str, windowsizes)
    plt.xticks(ticks=windowsizes, labels=myxlabels)
    plt.yticks(np.arange(64, 101, 2))
    plt.grid(axis='y', linestyle=':')
    scatterplot.figure.savefig(comp_conkmer_delly_allCN_bp, transparent=True)

    scatterplot_too_low = plt.figure(figsize=figure_size, tight_layout=True)
    plt.xscale('symlog')
    for i in range(len(alphas)):
        plt.plot(windowsizes, conkmer_cn_too_low_bp[:, i], marker=markers[i], markersize=marker_size,
                 color=colors[i], linestyle="--", label=labels[i])
    plt.legend(loc='upper right', title=legend_title)
    # plt.title("Proportion of ConKmer intervals with too low copy number compared to Delly")
    plt.xlabel(x_label)
    plt.ylabel("ConKmer bp with too low CN / all Delly intersection bp [%]")
    myxlabels = map(str, windowsizes)
    plt.xticks(ticks=windowsizes, labels=myxlabels)
    plt.yticks(np.arange(0.0, 30.1, 2.0))
    plt.grid(axis='y', linestyle=':')
    scatterplot_too_low.figure.savefig(comp_conkmer_cn_too_low_bp, transparent=True)

    scatterplot_interesting_ones = plt.figure(figsize=figure_size, tight_layout=True)
    plt.xscale('symlog')
    for i in range(len(alphas)):
        plt.plot(windowsizes, conkmer_cnu2_delly_intervals_percentage[:, i], marker=markers[i], markersize=marker_size,
                 color=colors[i], linestyle="--", label=labels[i])
    plt.legend(loc='lower left', title=legend_title)
    # plt.title("ConKmer intervals with CN != 2 supported with the same CN by all intersecting Delly intervals")
    plt.xlabel(x_label)
    plt.ylabel("%")
    myxlabels = map(str, windowsizes)
    plt.xticks(ticks=windowsizes, labels=myxlabels)
    plt.yticks(np.arange(62, 101, 2))  # 94.5, 99.1, 0.25
    plt.grid(axis='y', linestyle=':')
    scatterplot_interesting_ones.figure.savefig(comp_conkmer_delly_CNu2_intervals, transparent=True)

    scatterplot_CNu2 = plt.figure(figsize=figure_size, tight_layout=True)
    plt.xscale('symlog')
    for i in range(len(alphas)):
        plt.plot(windowsizes, conkmer_cn_unequal2_bp[:, i], marker=markers[i], markersize=marker_size,
                 color=colors[i], linestyle="--", label=labels[i])
    plt.legend(loc='upper left', title=legend_title)
    # plt.title("ConKmer intervals with CN != 2 supported by Delly")
    plt.xlabel(x_label)
    plt.ylabel("%")
    myxlabels = map(str, windowsizes)
    plt.xticks(ticks=windowsizes, labels=myxlabels)
    plt.yticks(np.arange(10, 61, 5))
    plt.grid(axis='y', linestyle=':')
    scatterplot_CNu2.figure.savefig(comp_conkmer_delly_CNu2_bp, transparent=True)

    scatterplot_delly_CNu2 = plt.figure(figsize=figure_size, tight_layout=True)
    plt.xscale('symlog')
    for i in range(len(alphas)):
        plt.plot(windowsizes, delly_cn_unequal2_bp[:, i], marker=markers[i], markersize=marker_size,
                 color=colors[i], linestyle="--", label=labels[i])
    plt.legend(loc='lower right', title=legend_title)
    # plt.title("Delly intervals with CN != 2 supported by ConKmer")
    plt.xlabel(x_label)
    plt.ylabel("%")
    myxlabels = map(str, windowsizes)
    plt.xticks(ticks=windowsizes, labels=myxlabels)
    plt.yticks(np.arange(40, 77, 2))
    plt.grid(axis='y', linestyle=':')
    scatterplot_delly_CNu2.figure.savefig(comp_delly_conkmer_CNu2_bp, transparent=True)


def main():
    create_kmer_coverage_plot(snakemake.input.statfiles, snakemake.output.conkmer_bp, snakemake.output.conkmer_bp_CNu2,
                              snakemake.output.conkmer_intervals, snakemake.output.comp_conkmer_delly_allCN_bp,
                              snakemake.output.comp_conkmer_cn_too_low_bp, snakemake.output.comp_conkmer_delly_CNu2_intervals,
                              snakemake.output.comp_conkmer_delly_CNu2_bp, snakemake.output.comp_delly_conkmer_CNu2_bp)


if __name__ == "__main__":
    main()
