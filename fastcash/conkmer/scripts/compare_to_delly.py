import sys
import pandas as pd

'''
example line:
    0      1       2     3    4     5       6       7        8       9  10      11    12
chr10 134435  140689  6254    2   2.0   chr10   49144   556221  507077  2   1.9544  6254
'''


def compute_statistics(a_b_intersections, reportsize=0):
    """
    :param reportsize: if != 0: size of an interval in bp to report due to differing CNs
    """
    match_bps = mismatch_bps = 0
    cn_too_high_bps = cn_too_low_bps = 0
    one_cn_too_high_bps = one_cn_too_low_bps = 0
    matches = mismatches = 0
    matches_a_cn_unequal2 = mismatches_a_cn_unequal2 = 0
    matching_intersections = mismatching_intersections = 0
    a_cn_unequal2_bps = a_cn_unequal2_matching_bps = 0

    last_a_start_index = -1
    last_a_cn = -1
    flag_mismatch = False

    for i, row in a_b_intersections.iterrows():
        a_cn = row[4]
        b_cn = row[10]
        a_start_index = row[2]
        intersection_bps = row.iat[-1]

        if reportsize and row[4] != row[10] and row[3] > reportsize and row[9] > reportsize and row[3]/row[9] > 0.8 and row[3]/row[9] < 1.2:
            print(f"ConKmer: {row[0]}:{row[1]}-{row[2]} CN {row[4]}")
            print(f"Delly: {row[6]}:{row[7]}-{row[8]} CN {row[10]}")
            print(f"{row[0]}:{min(row[1],row[7])}-{max(row[2],row[8])}\n")

        if a_cn == b_cn:
            match_bps += intersection_bps
            matching_intersections += 1
        else:
            mismatch_bps += intersection_bps
            mismatching_intersections += 1
            if a_cn < b_cn:
                cn_too_high_bps += intersection_bps
                if a_cn+1 == b_cn:
                    one_cn_too_high_bps += intersection_bps
            elif a_cn > b_cn:
                cn_too_low_bps += intersection_bps
                if a_cn == b_cn+1:
                    one_cn_too_low_bps += intersection_bps

        if a_cn != 2:
            a_cn_unequal2_bps += intersection_bps
            if a_cn == b_cn:
                a_cn_unequal2_matching_bps += intersection_bps

        if a_start_index != last_a_start_index:  # new A interval
            if flag_mismatch:
                mismatches += 1
                if last_a_cn != 2:
                    mismatches_a_cn_unequal2 += 1
            else:
                matches += 1
                if last_a_cn != 2:
                    matches_a_cn_unequal2 += 1
            flag_mismatch = False
        else:  # same A CNV as previous one intersected by more than one B CNV
            if a_cn != b_cn:
                flag_mismatch = True

        last_a_start_index = a_start_index
        last_a_cn = a_cn

    # once for last CNV intersection
    if flag_mismatch:
        mismatches += 1
        if last_a_cn != 2:
            mismatches_a_cn_unequal2 += 1
    else:
        matches += 1
        if last_a_cn != 2:
            matches_a_cn_unequal2 += 1

    return matches, mismatches, matching_intersections, mismatching_intersections,\
        cn_too_high_bps, cn_too_low_bps, one_cn_too_high_bps, one_cn_too_low_bps,\
        match_bps, mismatch_bps, a_cn_unequal2_bps, a_cn_unequal2_matching_bps,\
        matches_a_cn_unequal2, mismatches_a_cn_unequal2


def main():
    delly_conkmer_intersection = pd.read_csv(sys.argv[1], sep="\t", header=None)
    delly_all = pd.read_csv(sys.argv[2], sep="\t", header=None)
    conkmer_all = pd.read_csv(sys.argv[3], sep="\t", header=None)  # slow for large TSV files -> do it with awk?
    conkmer_delly_intersection = pd.read_csv(sys.argv[4], sep="\t", header=None)

    orig_stdout = sys.stdout  # store original stdout object for later
    sys.stdout = open(sys.argv[5], 'w')  # redirect all prints to this file

    print("Delly:")
    print(f"Number of Delly intervals (including CN 2): {len(delly_all):,}")
    delly_bps = delly_all[3].sum()
    print(f"Sum of the base pairs of all Delly intervals (including CN 2): {delly_bps:,}")
    delly_cnvs_cn_unequal2 = len(delly_all[delly_all[4] != 2])
    print(f"Number of Delly intervals with a CN != 2: {delly_cnvs_cn_unequal2:,}")
    dellyCNunequal2_bp_sum = (delly_all[delly_all[4] != 2])[3].sum()
    print(f"Sum of the base pairs of all Delly intervals with a CN != 2: {dellyCNunequal2_bp_sum:,}")
    max_cn = int(sys.argv[-1])  # snakemake.params.max_cn
    for cn in range(0, max_cn+1):
        delly_cn_bp_sum = (delly_all[delly_all[4] == cn])[3].sum()
        print(f"Sum of the base pairs of all Delly intervals with a CN of {cn}: {delly_cn_bp_sum:,}")
    delly_cn_bp_sum = (delly_all[delly_all[4] > max_cn])[3].sum()
    print(f"Sum of the base pairs of all Delly intervals with a CN >= {max_cn+1}: {delly_cn_bp_sum:,}")

    print("\nConKmer:")
    print(f"Number of ConKmer intervals (including CN 2): {len(conkmer_all):,}")
    conkmer_bps = conkmer_all[3].sum()
    print(f"Sum of the base pairs of all ConKmer intervals (including CN 2): {conkmer_bps:,}")
    conkmer_cnvs_cn_unequal2 = len(conkmer_all[conkmer_all[4] != 2])
    print(f"Number of ConKmer intervals with a CN != 2: {conkmer_cnvs_cn_unequal2:,}")
    conkmerCNunequal2_bp_sum = (conkmer_all[conkmer_all[4] != 2])[3].sum()
    print(f"Sum of the base pairs of all ConKmer intervals with a CN != 2: {conkmerCNunequal2_bp_sum:,}")
    for cn in range(0, max_cn+1):
        conkmer_cn_bp_sum = (conkmer_all[conkmer_all[4] == cn])[3].sum()
        print(f"Sum of the base pairs of all ConKmer intervals with a CN of {cn}: {conkmer_cn_bp_sum:,}")
    conkmer_cn_bp_sum = (conkmer_all[conkmer_all[4] > max_cn])[3].sum()
    print(f"Sum of the base pairs of all ConKmer intervals with a CN >= {max_cn+1}: {conkmer_cn_bp_sum:,}")

    print("\n----------")

    matches, mismatches, matching_intersections, mismatching_intersections,\
        cn_too_high_bps, cn_too_low_bps, one_cn_too_high_bps, one_cn_too_low_bps,\
        match_bps, mismatch_bps, a_cn_unequal2_bps, a_cn_unequal2_matching_bps,\
        matches_a_cn_unequal2, mismatches_a_cn_unequal2 = \
        compute_statistics(delly_conkmer_intersection)

    overlap_bp_sum = delly_conkmer_intersection[12].sum()

    print(f"\nDelly intervals (intersecting with ConKmer) for which all intersecting ConKmer intervals report the same CN: {matches:,} "
          f"({((matches/(matches+mismatches))*100):.2f} % of {(matches+mismatches):,})")
    print(f"Proportion of all {len(delly_all):,} Delly intervals supported with the same CN by all intersecting ConKmer intervals: "
          f"{((matches/len(delly_all))*100):.2f} %")
    print(f"Delly CN matches with ConKmer CN in {((match_bps/delly_bps)*100):.2f} % of all Delly bp.")
    print(f"ConKmer CN too low compared with Delly CN in {((cn_too_low_bps/overlap_bp_sum)*100):.2f} % of all delly_conkmer_intersection bp.")
    print(f"ConKmer CN too high compared with Delly CN in {((cn_too_high_bps/overlap_bp_sum)*100):.2f} % of all delly_conkmer_intersection bp.")
    print(f"ConKmer CN one too low compared with Delly CN in {((one_cn_too_low_bps/overlap_bp_sum)*100):.2f} % of all delly_conkmer_intersection bp.")
    print(f"ConKmer CN one too high compared with Delly CN in {((one_cn_too_high_bps/overlap_bp_sum)*100):.2f} % of all delly_conkmer_intersection bp.")

    print(f"\nDelly intervals with CN != 2 (intersecting with ConKmer) for which all intersecting ConKmer intervals report the same CN: {matches_a_cn_unequal2:,} "
          f"({((matches_a_cn_unequal2/(matches_a_cn_unequal2+mismatches_a_cn_unequal2))*100):.2f} % of {(matches_a_cn_unequal2+mismatches_a_cn_unequal2):,})")
    print(f"Proportion of all {delly_cnvs_cn_unequal2:,} Delly intervals with CN != 2 supported with the same CN by all intersecting ConKmer intervals: "
          f"{((matches_a_cn_unequal2/delly_cnvs_cn_unequal2)*100):.2f} %")
    print(f"Sum of the base pairs of the intersection of Delly intervals with CN != 2 and ConKmer intervals: {a_cn_unequal2_bps:,}")
    print(f"Delly CN != 2 matches with ConKmer CN in "
          f"{((a_cn_unequal2_matching_bps/a_cn_unequal2_bps)*100):.2f} % of all intersection bp.")
    print(f"Delly CN != 2 matches with ConKmer CN in "
          f"{((a_cn_unequal2_matching_bps/dellyCNunequal2_bp_sum)*100):.2f} % of all Delly bp with a CN != 2.")

    len_delly_conkmer_intersection = len(delly_conkmer_intersection)
    delly_conkmer_matching_intersections = matching_intersections
    delly_conkmer_overlap_bp_sum = overlap_bp_sum
    delly_conkmer_match_bps = match_bps
    delly_conkmer_mismatch_bps = mismatch_bps
    delly_conkmer_a_cn_unequal2_matching_bps = a_cn_unequal2_matching_bps

    print("\n----------\n")

    matches, mismatches, matching_intersections, mismatching_intersections,\
        cn_too_high_bps, cn_too_low_bps, one_cn_too_high_bps, one_cn_too_low_bps,\
        match_bps, mismatch_bps, a_cn_unequal2_bps, a_cn_unequal2_matching_bps,\
        matches_a_cn_unequal2, mismatches_a_cn_unequal2 = \
        compute_statistics(conkmer_delly_intersection, reportsize=40_000)

    overlap_bp_sum = conkmer_delly_intersection[12].sum()

    print(f"\nConKmer intervals (intersecting with Delly) for which all intersecting Delly intervals report the same CN: {matches:,} "
          f"({((matches/(matches+mismatches))*100):.2f} % of {(matches+mismatches):,})")
    print(f"Proportion of all {len(conkmer_all):,} ConKmer intervals supported with the same CN by all intersecting Delly intervals: "
          f"{((matches/len(conkmer_all))*100):.2f} %")
    print(f"ConKmer CN matches with Delly CN in {((match_bps/conkmer_bps)*100):.2f} % of all ConKmer bp.")

    print(f"\nConKmer intervals with CN != 2 (intersecting with Delly) for which all intersecting Delly intervals report the same CN: {matches_a_cn_unequal2:,} "
          f"({((matches_a_cn_unequal2/(matches_a_cn_unequal2+mismatches_a_cn_unequal2))*100):.2f} % of {(matches_a_cn_unequal2+mismatches_a_cn_unequal2):,})")
    print(f"Proportion of all {conkmer_cnvs_cn_unequal2:,} ConKmer intervals with CN != 2 supported with the same CN by all intersecting Delly intervals: "
          f"{((matches_a_cn_unequal2/conkmer_cnvs_cn_unequal2)*100):.2f} %")
    print(f"Sum of the base pairs of the intersection of ConKmer intervals with CN != 2 and Delly intervals: {a_cn_unequal2_bps:,}")
    print(f"ConKmer CN != 2 matches with Delly CN in "
          f"{((a_cn_unequal2_matching_bps/a_cn_unequal2_bps)*100):.2f} % of all conkmer_delly_intersection bp.")
    print(f"ConKmer CN != 2 matches with Delly CN in "
          f"{((a_cn_unequal2_matching_bps/conkmerCNunequal2_bp_sum)*100):.2f} % of all ConKmer bp with a CN != 2.")

    assert len_delly_conkmer_intersection == len(conkmer_delly_intersection)
    assert delly_conkmer_matching_intersections == matching_intersections
    assert delly_conkmer_overlap_bp_sum == overlap_bp_sum
    assert delly_conkmer_match_bps == match_bps
    assert delly_conkmer_mismatch_bps == mismatch_bps
    assert delly_conkmer_a_cn_unequal2_matching_bps == a_cn_unequal2_matching_bps
    print("\n----------")

    print(f"\nNumber of intersections between ConKmer and Delly (including CN 2): {len(conkmer_delly_intersection):,}")
    print(f"ConKmer CN == Delly CN for {matching_intersections:,} intersections "
          f"({((matching_intersections/len(conkmer_delly_intersection))*100):.2f} %)")
    print(f"Sum of the base pairs of the intersection of Delly and ConKmer intervals (including CN 2): {overlap_bp_sum:,}")
    print(f"ConKmer CN matches with Delly CN in {((match_bps/overlap_bp_sum)*100):.2f} % of all intersection bp.")
    print(f"ConKmer CN mismatches with Delly CN in {((mismatch_bps/overlap_bp_sum)*100):.2f}"
          f" % of all intersection bp.")

    print(f"\n\n\nStatistics of numerical columns of intersection between ConKmer and Delly results: {sys.argv[4]}")
    print(conkmer_delly_intersection.describe())

    print(f"\nStatistics of numerical columns of intersection between Delly and ConKmer results: {sys.argv[1]}")
    print(delly_conkmer_intersection.describe())
    print("\nValue counts of column chr (delly_conkmer_intersection):")
    print(delly_conkmer_intersection[0].value_counts())
    print("\nValue counts of column CN (Delly part of delly_conkmer_intersection):")
    print(delly_conkmer_intersection[4].value_counts())
    print("\nValue counts of column CN (ConKmer part of delly_conkmer_intersection):")
    print(delly_conkmer_intersection[10].value_counts())
    print("\nValue counts of 12th column (ConKmer interval size origin (delly_conkmer_intersection)):")
    print(delly_conkmer_intersection[12].value_counts())

    print(f"\n\nStatistics of numerical columns of Delly results: {sys.argv[2]}")
    print(delly_all.describe())
    print("\nValue counts of column chr (Delly):")
    print(delly_all[0].value_counts())
    print("\nValue counts of column CN (Delly):")
    print(delly_all[4].value_counts())

    print(f"\n\nStatistics of numerical columns of ConKmer results: {sys.argv[3]}")
    print(conkmer_all.describe())
    print("\nValue counts of column chr (ConKmer):")
    print(conkmer_all[0].value_counts())
    print("\nValue counts of column CN (ConKmer):")
    print(conkmer_all[4].value_counts())

    sys.stdout.close()  # ordinary file object
    sys.stdout = orig_stdout  # restore print commands to interactive prompt


if __name__ == "__main__":
    main()
