import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D


def plot_histogramm():
    with open(snakemake.input.raw_valuestats, "r") as raw_file, open(snakemake.params.gc_corrected_valuestats, "r") as gc_corrected_file:
        k = snakemake.wildcards.k
        upper_bound = snakemake.params.counts_to_plot
        plt.figure(figsize=[8, 4.5], tight_layout=True)
        plt.margins(x=0.01, tight=True)
        plt.yscale('log')
        plt.grid(axis='y', linestyle=':')
        file_color_pairs = [(raw_file, "blue"), (gc_corrected_file, "red")]
        for file, color in file_color_pairs:
            lines = file.read().splitlines()
            table = np.zeros((len(lines), 2), dtype=np.uint32)
            if lines:
                line = lines.pop(0)
                for i, line in enumerate(lines):
                    line_entrys = line.split()
                    count = int(line_entrys[0][:-1])
                    table[i][0] = count
                    occurrences = int(line_entrys[1])
                    table[i][1] = occurrences

            df = pd.DataFrame(data=table, columns=["$k$-mer count", "occurrences"])

            sigma = df['occurrences'][(upper_bound+1):].sum()
            kmertype = "unique" if snakemake.params.weak else "strongly unique"
            print(f"{color}: There are {sigma:,} {kmertype} {k}-mers with a count above {upper_bound}.")
            max_count = df["$k$-mer count"].iloc[-1]
            if max_count == 0:  # remove empty last line
                df = df[:-1]
            max_count = df["$k$-mer count"].iloc[-1]

            reduced_df = df.head(upper_bound + 1)
            plt.scatter(x=reduced_df["$k$-mer count"], y=reduced_df["occurrences"], c=color, s=4)
        # ax.set_axisbelow(True)
        plt.ylabel(f"number of distinct {k}-mers")
        plt.xlabel(f"{k}-mer count")
        stepsize = 10 if upper_bound < 200 else 50
        plt.xticks(np.arange(0, len(reduced_df["$k$-mer count"]) + 1, stepsize))
        legend_elements = [Line2D([0], [0], marker='o', color='w', label=f'raw {k}-mer counts', markerfacecolor='blue', markersize=4),
                           Line2D([0], [0], marker='o', color='w', label=f'GC bias corrected {k}-mer counts', markerfacecolor='red', markersize=4)]
        plt.legend(handles=legend_elements, loc='upper right')
        plt.savefig(snakemake.output.combinedplot, transparent=True)


def main():
    plot_histogramm()


if __name__ == "__main__":
    main()
