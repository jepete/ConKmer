

def write_intersection_bedfile(smaller_is_file, larger_is_file):
    """
    example line: chr start end CN
    chr1    889996  912349  1
    """
    with open(larger_is_file, "r") as larger_file, open(smaller_is_file, "r") as smaller_file, open(snakemake.output.cn_intervals_intersection, "w") as outfile:
        larger_lines = larger_file.read().splitlines()
        smaller_lines = smaller_file.read().splitlines()
        assert len(smaller_lines) > len(larger_lines)  # for debugging

        smaller_line = smaller_lines.pop(0)
        smaller_values = smaller_line.split()
        chrom_smaller = smaller_values[0]
        start_smaller = int(smaller_values[1])
        end_smaller = int(smaller_values[2])
        cn_smaller = int(smaller_values[4])

        while larger_lines:
            larger_line = larger_lines.pop(0)
            larger_values = larger_line.split()
            chrom_larger = larger_values[0]
            start_larger = int(larger_values[1])
            end_larger = int(larger_values[2])
            cn_larger = int(larger_values[4])
            if cn_larger == 2:
                continue
            while chrom_larger == chrom_smaller and start_larger >= end_smaller and smaller_lines:
                outfile.write(smaller_line + "\ts\n")
                smaller_line = smaller_lines.pop(0)
                smaller_values = smaller_line.split()
                chrom_smaller = smaller_values[0]
                start_smaller = int(smaller_values[1])
                end_smaller = int(smaller_values[2])
                cn_smaller = int(smaller_values[4])

            if chrom_larger == chrom_smaller and (start_larger <= end_smaller or start_larger <= start_smaller):
                assert cn_larger != 2
                # End smaller window size CNV earlier (at start_larger) to avoid a gap in the intersection
                if start_larger <= end_smaller and start_smaller < start_larger:
                    gap_filler = f"{chrom_smaller}\t{start_smaller}\t{start_larger}\t{start_larger-start_smaller}\t{cn_smaller}\tsmaller_shortened\n"
                    outfile.write(gap_filler)
                outfile.write(larger_line + "\tlarger\n")  # to indicate that this interval with CN != 2 was detected by the larger interval size
                while end_larger > start_smaller and chrom_larger == chrom_smaller and smaller_lines:
                    smaller_line = smaller_lines.pop(0)
                    smaller_values = smaller_line.split()
                    chrom_smaller = smaller_values[0]
                    start_smaller = int(smaller_values[1])
                    end_smaller = int(smaller_values[2])
                    cn_smaller = int(smaller_values[4])
                continue

        while smaller_lines:
            smaller_line = smaller_lines.pop(0)
            outfile.write(smaller_line + "\ts\n")


def main():
    if not snakemake.params.do_intersection:
        with open(snakemake.output.cn_intervals_intersection, 'w'):
            pass  # To avoid missing file exception by Snakemake
        return
    if snakemake.params.intervalsize < snakemake.params.second_intervalsize:
        write_intersection_bedfile(snakemake.input.cn_intervals, snakemake.input.second_cn_intervals)
    elif snakemake.params.intervalsize > snakemake.params.second_intervalsize:
        write_intersection_bedfile(snakemake.input.second_cn_intervals, snakemake.input.cn_intervals)
    else:
        print(f"WARNING: {snakemake.params.intervalsize=} == {snakemake.params.second_intervalsize=}, no intersection necessary")


if __name__ == "__main__":
    main()
