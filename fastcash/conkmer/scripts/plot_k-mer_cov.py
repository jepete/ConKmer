import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def create_kmer_coverage_plot(inputfiles, output):
    table = np.zeros((32, 5))
    for log in inputfiles:
        k = int(log[log.find(start := "_k") + 2:log.find(start := "_k") + 4])
        table[k][0] = k
        with open(log, "r") as file:
            lines = file.read().splitlines()
        assert lines
        while lines:
            line = lines.pop(0)
            if "$" in line:
                value = float(line[line.find(start := "$=") + len(start):line.find(" %")])
                if "strongly" in log:
                    if "positions of the given genome ($=" in line:
                        table[k][4] = value
                    elif "N-free" in line and "-mers of the given genome ($=" in line:
                        table[k][3] = value
                    else:
                        print("It seems that we've found a wrong line")
                else:
                    if "positions of the given genome ($=" in line:
                        table[k][2] = value
                    elif "N-free" in line and "-mers of the given genome ($=" in line:
                        table[k][1] = value
                    else:
                        print("It seems that we've found a wrong line")
        file.close()

    assert len(inputfiles) % 2 == 0
    print(f"{len(inputfiles)=}")
    matrix = np.zeros((int(len(inputfiles)/2), 5))
    print(f"{len(table)=}")
    c = 0
    for i in range(len(table)):
        if table[i][0] != 0:
            matrix[c] = table[i]
            c += 1
    print(f"{c=}")
    assert c == int(len(inputfiles)/2)

    print(f"\n{table=}\n")
    print(f"{matrix=}\n")

    df = pd.DataFrame(data=matrix,
                      columns=["k",
                               "unique $k$-mers / all valid genome positions",
                               "unique $k$-mers / all genome positions",
                               "strongly unique $k$-mers / all valid genome positions",
                               "strongly unique $k$-mers / all genome positions"])

    scatterplot = plt.figure(figsize=(7, 4.5), tight_layout=True)
    colors = ["green", "orange", "blue", "red"]
    markers = ['o', 'v', 's', 'd']
    for i in range(4):
        plt.plot(df["k"], df[df.columns[i+1]], marker=markers[i], markersize=6,
                 color=colors[i], linestyle="--", label=df.columns[i+1])
    plt.legend(loc='lower right')
    # plt.suptitle("Percetage of [strongly] unique k-mers")
    # plt.title("(A genome position is valid, if an N-free k-mer starts there.)")
    plt.xlabel("$k$")
    plt.ylabel("%")
    plt.xticks(np.arange(19, 32, 2))
    plt.yticks(np.arange(5, 90, 5))
    plt.grid(axis='y', linestyle=':')
    scatterplot.figure.savefig(output, transparent=True)


def main():
    create_kmer_coverage_plot(snakemake.input.logfiles, snakemake.output[0])


if __name__ == "__main__":
    main()
