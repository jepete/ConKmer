import matplotlib.pyplot as plt
# from brokenaxes import brokenaxes

'''
example file content:
s           h:m:s   max_rss     max_vms     max_uss     max_pss     io_in   io_out    mean_load   cpu_time
236.3599    0:03:56 22119.58    25473.89    22107.25    22111.66    0.00    10776.16    723.35    1705.32
'''


def main():
    threads = [1, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28]
    windowsizes = [5, 99, 3999]  # 499
    z_markers = {"5": "o", "99": "s", "499": "d", "3999": "^"}

    scatterplot = plt.figure(figsize=(8.5, 5.0))  # , tight_layout=True)
    # baxes = brokenaxes(ylims=((0,85),(330,440)), hspace=.04)  # Why does it not work anymore?

    for z in windowsizes:
        cputimes_np = []
        cputimes_own = []
        for t in threads:
            with open(f"/scratch/conkmer/logs/GRCh38-primary_call_strongly-unique_without-preindex-intersection_readsBS0169_{z}_8_with-GC-correction_k27_t{t}_np.log", "r") as np_file, \
                 open(f"/scratch/conkmer/logs/GRCh38-primary_call_strongly-unique_without-preindex-intersection_readsBS0169_{z}_8_with-GC-correction_k27_t{t}_own.log", "r") as own_file:
                np_lines = np_file.read().splitlines()
                own_lines = own_file.read().splitlines()
                assert np_lines and own_lines
                cputime_np = cputime_own = 0.0  # CPU time from Snakemake is a bad idea
                counter_np = counter_own = 0
                while np_lines:
                    np_line = np_lines.pop(0)
                    if " time (seconds): " not in np_line:
                        continue
                    np_values = np_line.split()
                    cputime_np += float(np_values[-1])/60.0
                    counter_np += 1
                cputimes_np.append(cputime_np/counter_np)
                while own_lines:
                    own_line = own_lines.pop(0)
                    if " time (seconds): " not in own_line:
                        continue
                    own_values = own_line.split()
                    cputime_own += float(own_values[-1])/60.0
                    counter_own += 1
                cputimes_own.append(cputime_own/counter_own)
                print(f"{z=}; {t=}: average CPU times: np: {(cputime_np/counter_np):.2f} minutes; own: {(cputime_own/counter_own):.2f} minutes")
        plt.plot(threads, cputimes_np, marker=z_markers[str(z)], color="black", linestyle="--", label=f"{z=} numpy.median")
        plt.plot(threads, cputimes_own, marker=z_markers[str(z)], color="blue", linestyle="-", label=f"{z=} own implementation")

    plt.legend(loc='upper left')
    # baxes.axs[1].set_xticks(threads)
    plt.xticks(threads)
    # plt.grid(axis='y', linestyle=':')
    plt.xlabel("number of threads")
    plt.ylabel("CPU time [minutes]")
    scatterplot.figure.savefig("call_benchmark_cputime_reduced_bax.pdf", transparent=True)


if __name__ == "__main__":
    main()
