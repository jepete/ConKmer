import numpy as np
import matplotlib.pyplot as plt

"""
# TOTAL TIME for weak k-mers: 287.5 sec
total time [sec]: 466.0 sec
"""


def main():

    subtables = []
    totaltimes = []
    build_fill_times = []
    weaktimes = []

    for i in range(4, 33, 2):
        filename = f"/scratch/conkmer/logs/GRCh38-primary_preindex_strongly-unique_k27_t{i}.log"
        with open(filename, "r") as file:
            lines = file.read().splitlines()
            assert lines
            while lines:
                line = lines.pop(0)
                values = line.split()
                if "TOTAL TIME for weak k-mers" in line:
                    weaktime = float(values[6])
                    weaktimes.append(weaktime)
                elif "total time [sec]" in line:
                    totaltime = float(values[3])
                    totaltimes.append(totaltime)
                elif "Time to build and fill preindex" in line:
                    build_fill_time = float(values[6])
                    build_fill_times.append(build_fill_time)
            subtables.append(i - 1)
            print(f"number of subtables = {i - 1}: build and fill time = {build_fill_time:.2f} s; weak marking time = {weaktime:.2f} s; total wall clock time = {totaltime:.2f} s")

    scatterplot = plt.figure(figsize=(7.5, 3.5), tight_layout=True)
    plt.plot(subtables, totaltimes, marker="o", color="black", linestyle="--", label="total")  # execute the whole preindex step")
    plt.plot(subtables, weaktimes, marker="s", color="blue", linestyle="--", label="mark weak contiguous 27-mers")
    plt.plot(subtables, build_fill_times, marker="v", color="green", linestyle="--", label="build and fill preindex")
    plt.legend(loc='upper right')  # , title='wall clock time to')
    plt.xlabel("number of subtables ($T$)")
    plt.ylabel("ellapsed wall clock time [sec]")
    plt.xticks(np.arange(3, 33, 2))
    plt.yticks(np.arange(0, 500, 50))
    plt.grid(axis='y', linestyle=':')
    scatterplot.figure.savefig("preindex_subtables_time_from_logs.pdf", transparent=True)


if __name__ == "__main__":
    main()
