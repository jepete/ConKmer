import numpy as np
import matplotlib.pyplot as plt

"""
example file content:
s           h:m:s   max_rss     max_vms     max_uss     max_pss     io_in   io_out    mean_load   cpu_time
236.3599    0:03:56 22119.58    25473.89    22107.25    22111.66    0.00    10776.16    723.35    1705.32
233.7266    0:03:53 22120.17    25473.90    22106.57    22110.96    0.02    10776.14    718.98    1675.80
234.5903    0:03:54 22121.36    25473.95    22107.73    22112.08    0.00    10776.14    717.79    1678.14
"""


def main():

    pairs = [("preindex", "preindex_strongly-unique"),
             ("index", "index_strongly-unique_without-preindex-intersection"),
             ("count", "count_strongly-unique_without-preindex-intersection_readsBS0169"),
             ("call", "call_strongly-unique_without-preindex-intersection_readsBS0169_99_8_with-GC-correction")]

    for step, filename_part in pairs:
        subtables = []
        walltimes = []
        cputimes = []

        for i in range(4, 33, 2):
            counter = 0
            walltime = cputime = 0.0
            with open(f"/scratch/conkmer/benchmarks/GRCh38-primary_{filename_part}_k27_t{i}.txt", "r") as file:
                lines = file.read().splitlines()
                assert lines
                while lines:
                    line = lines.pop(0)
                    if "max" in line:  # header
                        continue
                    values = line.split()
                    walltime += float(values[0]) / 60.0
                    cputime += float(values[9]) / 60.0  # CPU time from Snakemake can be a bad idea
                    counter += 1
                average_walltime = walltime / counter
                subtables.append(i - 1)
                walltimes.append(average_walltime)
                cputimes.append(cputime / counter)
                print(f"number of subtables = {i - 1}: average wall clock time = {average_walltime:.2f} minutes; "
                      f"average CPU time: {(cputime/counter):.2f} minutes; repetitions: {counter}")

        scatterplot = plt.figure(figsize=(7.5, 3.5), tight_layout=True)
        plt.plot(subtables, cputimes, marker="^", color="black", linestyle="--", label="CPU time")
        plt.xlabel("number of subtables ($T$)")
        plt.ylabel("CPU time [minutes]")
        plt.xticks(np.arange(3, 33, 2))
        # plt.yticks(np.arange(0, 26.1, 2))
        plt.grid(axis='y', linestyle=':')
        scatterplot.figure.savefig(f"{step}_subtables_cpu-time_from_benchmarks.pdf", transparent=True)

        scatterplot = plt.figure(figsize=(7.5, 3.5), tight_layout=True)
        plt.plot(subtables, walltimes, marker="o", color="black", linestyle="--", label="wall time")
        plt.xlabel("number of subtables ($T$)")
        plt.ylabel("wall clock time [minutes]")
        plt.xticks(np.arange(3, 33, 2))
        # plt.yticks(np.arange(0, 121, 10))
        # plt.yticks(np.arange(0, 28.1, 2))
        plt.grid(axis='y', linestyle=':')
        scatterplot.figure.savefig(f"{step}_subtables_wall-clock-time_from_benchmarks.pdf", transparent=True)


if __name__ == "__main__":
    main()
