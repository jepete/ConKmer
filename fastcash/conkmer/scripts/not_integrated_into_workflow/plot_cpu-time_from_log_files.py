import numpy as np
import matplotlib.pyplot as plt

"""
User time (seconds): 715.34
System time (seconds): 12.07
Percent of CPU this job got: 273%
Elapsed (wall clock) time (h:mm:ss or m:ss): 4:26.04
"""


def main():

    pairs = [("/scratch/conkmer/logs/GRCh38-primary_preindex_strongly-unique_k27_t", "preindex_subtables_cpu-time_from_logs.pdf"),
             ("/scratch/conkmer/logs/GRCh38-primary_index_strongly-unique_without-preindex-intersection_k27_t", "index_subtables_cpu-time_from_logs.pdf"),
             ("/scratch/conkmer/logs/GRCh38-primary_count_strongly-unique_without-preindex-intersection_readsBS0169_k27_t", "count_subtables_cpu-time_from_logs.pdf"),
             ("/scratch/conkmer/logs/GRCh38-primary_call_strongly-unique_without-preindex-intersection_readsBS0169_99_8_with-GC-correction_k27_t", "call_subtables_cpu-time_from_logs.pdf")]

    for filename, plotname in pairs:
        subtables = []
        cputimes = []

        for i in range(4, 33, 2):
            counter = 0
            cputime = 0.0
            with open(f"{filename}{i}.log", "r") as file:
                lines = file.read().splitlines()
                assert lines
                while lines:
                    line = lines.pop(0)
                    if " time (seconds): " not in line:
                        continue
                    values = line.split()
                    cputime += float(values[-1]) / 60.0
                    counter += 1
                subtables.append(i - 1)
                cputimes.append(cputime / counter)
                print(f"number of subtables = {i - 1}: average CPU time: {(cputime / counter):.2f} minutes")

        scatterplot = plt.figure(figsize=(7.5, 3.5), tight_layout=True)
        plt.plot(subtables, cputimes, marker="^", color="black", linestyle="--", label="CPU time")
        plt.xlabel("number of subtables ($T$)")
        plt.ylabel("CPU time [minutes]")
        plt.xticks(np.arange(3, 33, 2))
        # plt.yticks(np.arange(0, 26.1, 2))
        plt.grid(axis='y', linestyle=':')
        scatterplot.figure.savefig(plotname, transparent=True)


if __name__ == "__main__":
    main()
