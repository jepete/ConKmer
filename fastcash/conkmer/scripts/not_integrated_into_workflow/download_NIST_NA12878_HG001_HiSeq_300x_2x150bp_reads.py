import sys
import subprocess
import time
import os
from multiprocessing.dummy import Pool


def download_file(url, md5sum, counter):
    counter += 1
    allowed_tries = 5
    if counter <= allowed_tries:
        try:
            # ftp://ftp-trace.ncbi.nih.gov/ReferenceSamples/giab/data/NA12878/NIST_NA12878_HG001_HiSeq_300x/131219_D00360_005_BH814YADXX/Project_RM8398/Sample_U0a/U0a_CGATGT_L001_R2_001.fastq.gz
            output_directory = url[url.find(start := "/NA12878/") + len(start):url.find("/U")]

            output_directory = f"/scratch/conkmer/download/{output_directory}"  # f"{sys.argv[2]}/{output_directory}"

            if not os.path.isdir(output_directory):
                os.makedirs(output_directory)

            subprocess.run(f"wget {url} -P {output_directory}", shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)

            filename = url.split("/")[-1]

            path = f"{output_directory}/{filename}"

            result = subprocess.run(f"md5sum {path}", capture_output=True, text=True, shell=True)

            if result.stdout.split()[0] != md5sum:
                print(f"\tMD5 of just downloaded file {path} differs. Deleting and downloading it again ...")
                os.remove(path)
                download_file(url, md5sum, counter)

        except Exception as e:
            print(e)
            seconds = 30
            print(f"\nThere was the previous error. Going to sleep {seconds} seconds and trying it again (might help in case of connection problems).")
            time.sleep(seconds)
            download_file(url, md5sum, counter)
    else:
        print(f"\tTried to download {url} {allowed_tries} times, but MD5 always differed "
              f"or the above mentioned errors occured (might be printed afterwards due to multithreading)")  #; now skipping")
        sys.exit(-1)


def download_launcher(url_md5_pair):
    url = url_md5_pair[0]
    md5sum = url_md5_pair[1]
    path = url.partition("/NA12878/")[-1]
    path_parts = path.split("_")

    if not ("05" in path_parts[6] or "06" in path_parts[6] or "07" in path_parts[6] or "08" in path_parts[6]):
        return  # should be commented out to download the full 300x set

    path = "/scratch/conkmer/download/" + path  # sys.argv[2] + path

    if os.path.exists(path):
        result = subprocess.run(f"md5sum {path}", capture_output=True, text=True, shell=True)
        if result.stdout.split()[0] != md5sum:
            print(f"\tMD5 of existing file {path} differs. Deleting and downloading it again ...")
            os.remove(path)
            download_file(url, md5sum, 0)
    else:
        download_file(url, md5sum, 0)


def get_url_md5_pairs(filesindex):
    url_md5_pairs = []
    with open(filesindex, "r") as file:
        lines = file.read().splitlines()
        assert lines
        line = lines.pop(0)
        # for line in file.readlines():
        while lines:
            line = lines.pop(0)
            line_entrys = line.split()
            assert len(line_entrys) == 5

            # FASTQ
            url = line_entrys[0]
            md5sum = line_entrys[1]
            url_md5_pairs.append((url, md5sum))

            # PAIRED_FASTQ
            url = line_entrys[2]
            md5sum = line_entrys[3]
            url_md5_pairs.append((url, md5sum))

    return url_md5_pairs


def main():
    url_md5_pairs = get_url_md5_pairs("/scratch/conkmer/sequence.index.NA12878_Illumina300X_wgs_09252015.tsv")  # sys.argv[1])  # "sequence.index.NA12878_Illumina300X_wgs_09252015.tsv"
    nthreads = 1  # use 1 when most files already exist and 12 or more for downloading
    print(f"Running with {nthreads} {'threads' if nthreads > 1 else 'thread'}\n")
    # https://stackoverflow.com/questions/31784484/how-to-parallelize-file-downloads
    Pool(nthreads).map(download_launcher, url_md5_pairs)
    # download_launcher(url_md5_pairs[3])  # for testing
    print("\nAll threads finally finished.")


if __name__ == "__main__":
    main()
