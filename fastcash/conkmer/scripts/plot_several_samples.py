import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def plot(inputfiles, bp_per_cn, bp_cnu2, intervals_cnu2):
    samples = snakemake.params.samples
    samples_dict = dict()
    # map samples to ints
    for i, sample in enumerate(samples):
        samples_dict[sample] = i
    max_cn = snakemake.params.max_cn

    delly_bp_per_cn_matrix = np.zeros((len(samples), max_cn+2), dtype=np.uint32)
    conkmer_bp_per_cn_matrix = np.zeros((len(samples), max_cn+2), dtype=np.uint32)
    delly_bp_cnu2 = np.zeros(len(samples), dtype=np.uint32)
    conkmer_bp_cnu2 = np.zeros(len(samples), dtype=np.uint32)
    delly_intervals_cnu2 = np.zeros(len(samples), dtype=np.uint32)
    conkmer_intervals_cnu2 = np.zeros(len(samples), dtype=np.uint32)

    for statfile in inputfiles:
        statfile_parts = statfile.split("_")
        sample = statfile_parts[2]  # bad idea if file names change
        with open(statfile, "r") as file:
            lines = file.read().splitlines()
            assert lines
            while lines:
                line = lines.pop(0)
                if "Sum of the base pairs of all ConKmer intervals with a CN != 2: " in line:
                    line_values = line.split(" ")
                    value = int(line_values[-1].replace(',', ''))
                    conkmer_bp_cnu2[samples_dict[sample] - 1] = value
                elif "Sum of the base pairs of all Delly intervals with a CN != 2: " in line:
                    line_values = line.split(" ")
                    value = int(line_values[-1].replace(',', ''))
                    delly_bp_cnu2[samples_dict[sample] - 1] = value
                elif "Number of ConKmer intervals with a CN != 2: " in line:
                    line_values = line.split(" ")
                    value = int(line_values[-1].replace(',', ''))
                    conkmer_intervals_cnu2[samples_dict[sample] - 1] = value
                elif "Number of Delly intervals with a CN != 2: " in line:
                    line_values = line.split(" ")
                    value = int(line_values[-1].replace(',', ''))
                    delly_intervals_cnu2[samples_dict[sample] - 1] = value
                for cn in range(0, max_cn + 1):
                    if f"Sum of the base pairs of all Delly intervals with a CN of {cn}: " in line:
                        line_values = line.split(" ")
                        value = int(line_values[-1].replace(',', ''))
                        delly_bp_per_cn_matrix[samples_dict[sample]][cn] = value
                    elif f"Sum of the base pairs of all ConKmer intervals with a CN of {cn}: " in line:
                        line_values = line.split(" ")
                        value = int(line_values[-1].replace(',', ''))
                        conkmer_bp_per_cn_matrix[samples_dict[sample]][cn] = value
                    if f"Sum of the base pairs of all Delly intervals with a CN >= {max_cn + 1}: " in line:
                        line_values = line.split(" ")
                        value = int(line_values[-1].replace(',', ''))
                        delly_bp_per_cn_matrix[samples_dict[sample]][max_cn+1] = value
                    elif f"Sum of the base pairs of all ConKmer intervals with a CN >= {max_cn + 1}: " in line:
                        line_values = line.split(" ")
                        value = int(line_values[-1].replace(',', ''))
                        conkmer_bp_per_cn_matrix[samples_dict[sample]][max_cn + 1] = value

    for i in range(0, len(conkmer_bp_cnu2)):
        print(f"{conkmer_bp_cnu2[i]=} / {delly_bp_cnu2[i]=} = {(conkmer_bp_cnu2[i]/delly_bp_cnu2[i]):.4f}")
    basepairs = {'ConKmer': conkmer_bp_cnu2,
                 'Delly': delly_bp_cnu2}
    df = pd.DataFrame(basepairs)
    ax = df.plot(kind='barh', figsize=(8.0, 4.0), grid=True, legend=True, logx=False, xlim=(0, 275_000_001), alpha=1.0)
    ax.set(xlabel=r"number of bp with a CN $\neq$ 2", ylabel="in-house sample number")
    plt.yticks(np.arange(0, 10, 1), ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'])
    plt.xticks(np.arange(0, 270_000_001, 25_000_000))  # optimized for z = 3,999 and otherwise default parameters
    handles, labels = ax.get_legend_handles_labels()
    plt.legend(reversed(handles), reversed(labels), loc='upper left')
    ax.set_axisbelow(True)
    plt.tight_layout()
    plt.savefig(bp_cnu2, transparent=True)

    print(f"\n{delly_intervals_cnu2=}\n{conkmer_intervals_cnu2=}\n")
    intervals = {'ConKmer': conkmer_intervals_cnu2,
                 'Delly': delly_intervals_cnu2}
    df = pd.DataFrame(intervals)
    ax = df.plot(kind='barh', figsize=(8.0, 4.0), grid=True, legend=True, logx=False, xlim=(0, 4_100), alpha=1.0)
    ax.set(xlabel=r"number of intervals with a CN $\neq$ 2", ylabel="in-house sample number")
    plt.yticks(np.arange(0, 10, 1), ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'])
    plt.xticks(np.arange(0, 4_101, 500))  # optimized for z = 3,999 and otherwise default parameters
    handles, labels = ax.get_legend_handles_labels()
    plt.legend(reversed(handles), reversed(labels), loc='upper left')
    ax.set_axisbelow(True)
    plt.tight_layout()
    plt.savefig(intervals_cnu2, transparent=True)

    df_delly = pd.DataFrame(delly_bp_per_cn_matrix)
    df_conkmer = pd.DataFrame(conkmer_bp_per_cn_matrix)
    print(f"\ndelly_bp_per_cn:\n{df_delly}\n\nconkmer_bp_per_cn:\n{df_conkmer}\n")
    delly_stats = df_delly.describe()
    conkmer_stats = df_conkmer.describe()
    print(f"\ndelly_stats:\n{delly_stats}\n\nconkmer_stats:\n{conkmer_stats}\n")
    delly_matrix = []
    conkmer_matrix = []
    for i in range(max_cn+1, -1, -1):
        delly_matrix.append(df_delly[i].to_numpy())
        conkmer_matrix.append(df_conkmer[i].to_numpy())
    plt.figure(figsize=(3.8, 7.0), tight_layout=True)
    # based on https://www.geeksforgeeks.org/how-to-create-boxplots-by-group-in-matplotlib/
    # create a boxplot for two arrays separately, the position specifies the location of the particular box in the graph.
    flierprops = dict(marker='o', markerfacecolor='black', linestyle='none', markeredgecolor='none', alpha=0.5)
    meanprops = dict(marker='_', color='green')
    delly_plot = plt.boxplot(delly_matrix, positions=np.array(np.arange(len(delly_matrix)))*2.0-0.35, widths=0.6, flierprops=flierprops, showmeans=True, meanprops=meanprops)
    conkmer_plot = plt.boxplot(conkmer_matrix, positions=np.array(np.arange(len(conkmer_matrix)))*2.0+0.35, widths=0.6, flierprops=flierprops, showmeans=True, meanprops=meanprops)

    # each plot returns a dictionary, ax.setp() function assigns the color code for all properties of the box plot
    def define_box_properties(plot_name, color_code, label):
        for k, v in plot_name.items():
            plt.setp(plot_name.get(k), color=color_code)
        # use plot function to draw a small line to name the legend.
        plt.plot([], c=color_code, label=label)
        plt.legend()
    # setting colors for each groups
    define_box_properties(delly_plot, 'orange', 'Delly')
    define_box_properties(conkmer_plot, 'blue', 'ConKmer')
    x_labels = []
    x_labels.append(r"$\geq$" + str(max_cn+1))
    for i in range(max_cn, -1, -1):
        x_labels.append(str(i))
    plt.yticks([10**i for i in range(0, 10)], rotation=90)
    plt.xticks(np.arange(0, len(x_labels) * 2, 2), x_labels)
    plt.yscale('symlog')
    plt.ylim(-0.2, 5*(10**9))
    plt.xlabel("copy number", rotation=180)
    plt.ylabel("number of basepairs")
    plt.xticks(rotation=90)
    plt.grid(axis='both', linestyle=':')
    for x in range(0, max_cn+1):
        plt.axvline(x=x*2+1, color='black')
    plt.tight_layout()
    plt.savefig(bp_per_cn, transparent=True)


def main():
    plot(snakemake.input.statfiles, snakemake.output.bp_per_cn, snakemake.output.bp_cnu2, snakemake.output.intervals_cnu2)


if __name__ == "__main__":
    main()
