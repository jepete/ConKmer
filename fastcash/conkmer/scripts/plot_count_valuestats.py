import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def plot_histogramm():
    with open(snakemake.input.valuestats, "r") as file:
        lines = file.read().splitlines()
        table = np.zeros((len(lines), 2), dtype=np.uint32)
        if lines:
            line = lines.pop(0)
            for i, line in enumerate(lines):
                line_entrys = line.split()
                count = int(line_entrys[0][:-1])
                table[i][0] = count
                occurrences = int(line_entrys[1])
                table[i][1] = occurrences
            k = snakemake.wildcards.k
            df = pd.DataFrame(data=table, columns=["$k$-mer count", "occurrences"])
            count_zero = df["occurrences"][0]
            max_count = df["$k$-mer count"].iloc[-1]
            if max_count == 0:  # remove empty last line
                df = df[:-1]
            max_count = df["$k$-mer count"].iloc[-1]
            count_too_high = df["occurrences"].iloc[-1]
            # Hash table value statistics after conkmer count\n\nused reads: {snakemake.params.reads}\n
            scatterplot = df.plot.scatter(x="$k$-mer count", y="occurrences", logx=True, logy=True,
                                          title=f"There are {count_zero:,}  {k}-mers with a count of 0 (not plotted)"
                                                f"\nand {count_too_high:,}  {k}-mers with a count of {max_count:,} or above (rightmost dot).",
                                          xlabel=f"{k}-mer count", ylabel=f"number of distinct {k}-mers",
                                          figsize=(9, 5.5), grid='both', s=2, alpha=0.5)
            plt.tight_layout()
            plt.grid(axis='both', linestyle=':')
            scatterplot.set_xlim(0.901, 1*(10**5))
            scatterplot.set_ylim(0.901, 2*(10**8))
            scatterplot.set_axisbelow(True)
            scatterplot.figure.savefig(snakemake.output.scatterplot, transparent=True)

            upper_bound = snakemake.params.counts_to_plot
            reduced_df = df.head(upper_bound + 1)

            plt.figure(figsize=[10, 6], tight_layout=True)
            plt.grid(axis='y', which='both', linewidth=1, linestyle=':')
            plt.margins(x=0.01, tight=True)
            barplot = plt.bar(x=reduced_df["$k$-mer count"], height=reduced_df["occurrences"], log=True)

            # just text formatting
            sigma = df['occurrences'][(upper_bound+1):].sum()
            kmertype = "unique" if snakemake.params.weak else "strongly unique"
            text = f"There are {sigma:,} {kmertype} {k}-mers with a count above {upper_bound}."

            plt.title(f"{text}")
            plt.ylabel(f"number of distinct {k}-mers")
            plt.xlabel(f"{k}-mer count")
            stepsize = 10 if upper_bound < 200 else 50
            plt.xticks(np.arange(0, len(reduced_df["$k$-mer count"]) + 1, stepsize))
            ax = plt.gca()
            ax.set_axisbelow(True)
            plt.savefig(snakemake.output.barplot, transparent=True)


def main():
    plot_histogramm()


if __name__ == "__main__":
    main()
