import numpy as np

from collections import namedtuple

from numba import njit, int64, uint64, boolean


WindowBuffer = namedtuple("WindowBuffer", [
    "array",
    "median_array",

    "size",
    "dynamics",
    "median_functionality",
    "block_offset",

    "add_value",
    "get_median",
    "get_oldest_value",
    "get_newest_value",
    "get_middle_value"
    ])


def create_windowbuffer(size, datatype, blocksize, median_functionality=False):
    """
    Initialize and return a cyclic buffer of 'size' elements of type 'datatype'.
    If median_functionality=True, a sorted copy is maintained additionally

    :param size: size of the buffer (in case of windows the window size)
    :param datatype: the data type of the values to store inside this buffer
    :param blocksize: size of the protected middle part of the sorted buffer
    :param median_functionality: flag indicating if it should be possible to
        determine the median of the values in this cyclic buffer in constant time
        (which sometimes requires sorting after insertion)
    """
    # some values change dynamicly: offset, tolerance_left, tolerance_right, median_shift
    dynamics = np.zeros(4, dtype=np.int64)
    array = np.zeros(size, dtype=datatype)
    block_offset = int64(blocksize // 2)
    size = int64(size)  # otherwise int64 % int = float64 due to numpy

    @njit(nogil=True, inline='always', locals=dict(
        current_index=int64, need_to_resort=boolean,
        leftmost_blockindex=uint64, rightmost_blockindex=uint64))
    def add_value(window, value, dynamics, median_buffer=None):
        """
        Adds the value 'value' to the cyclic buffer 'window'.
        If the buffer is already full, the oldest value is overwritten.
        :param window: the buffer to which the value has to be added
        :param value: the value to add to the buffer 'window'
        :param dynamics: some dynamically changing values to save some parameters
                         dynamics[0]: offset (offset % size = next insertion position)
                         dynamics[1]: tolerance_left
                         dynamics[2]: tolerance_right
                         dynamics[3]: median_shift
        :param median_buffer: representation of 'window' to infer the median from
                              in constant time, therfore partly sorted
        """
        current_index = dynamics[0] % size
        assert current_index >= 0
        leaving_val = window[current_index]
        window[current_index] = value
        dynamics[0] += 1
        if median_functionality:
            assert median_buffer is not None, "This is a cyclic buffer with median functionality, \
                                               you need to provide the median_buffer"
            leftmost_bl_el = median_buffer[(size // 2) - block_offset]
            rightmost_bl_el = median_buffer[(size // 2) + block_offset]
            need_to_resort = 0
            if isclose(leaving_val, leftmost_bl_el) or isclose(value, leftmost_bl_el):
                dynamics[1] -= 1
            if isclose(leaving_val, rightmost_bl_el) or isclose(value, rightmost_bl_el):
                dynamics[2] -= 1
            if dynamics[1] < 0 or dynamics[2] < 0:
                need_to_resort = 1
            if ((leaving_val < leftmost_bl_el or isclose(leaving_val, leftmost_bl_el)) and
               (value < leftmost_bl_el or isclose(value, leftmost_bl_el))) or\
               ((leaving_val > rightmost_bl_el or isclose(leaving_val, rightmost_bl_el)) and
               (value > rightmost_bl_el or isclose(value, rightmost_bl_el))):
                pass  # case 1: median does not change
                # because old and new elements are both on the same side outside the block
            elif (leaving_val < leftmost_bl_el or isclose(leaving_val, leftmost_bl_el)) and (value > rightmost_bl_el or isclose(value, rightmost_bl_el)):  # case 2a
                dynamics[3] += 1
            elif (leaving_val > rightmost_bl_el or isclose(leaving_val, rightmost_bl_el)) and (value < leftmost_bl_el or isclose(value, leftmost_bl_el)):  # case 2b
                dynamics[3] -= 1
            else:  # case 3: at least leaving_val or value in the block -> resort
                need_to_resort = 1
            if dynamics[3] > block_offset or dynamics[3] < -block_offset:
                need_to_resort = 1
            if need_to_resort > 0:
                temp_buffer = np.sort(window)  # RESORT
                # overwrite median_buffer element wise (alternative to returning median_buffer)
                median_buffer[:] = temp_buffer[:]
                dynamics[3] = 0
                dynamics[1] = 0
                leftmost_blockindex = uint64((size // 2) - block_offset)
                leftmost_bl_el = median_buffer[leftmost_blockindex]
                # count how many elements to the left are equal leftmost_bl_el
                for j in range(leftmost_blockindex-1, -1, -1):
                    if isclose(median_buffer[j], leftmost_bl_el):
                        dynamics[1] += 1
                    else:
                        break
                dynamics[2] = 0
                rightmost_blockindex = uint64((size // 2) + block_offset)
                rightmost_bl_el = median_buffer[rightmost_blockindex]
                for i in range(rightmost_blockindex+1, size, 1):
                    if isclose(median_buffer[i], rightmost_bl_el):
                        dynamics[2] += 1
                    else:
                        break

    @njit(nogil=True)
    def get_median(median_buffer, dynamics):
        """
        Returns the median of the given sorted median_buffer.
        Not to be confused with 'get_middle_value'.
        :param median_buffer: numpy array to infer the median from
                              in constant time, therfore partly sorted
        :param dynamics: some dynamically changing values to save some parameters
                         dynamics[0]: offset (offset % size = next insertion position)
        """
        assert median_functionality, "This method should only be called for a sorted buffer."
        index = int((size // 2) + dynamics[3])
        median = median_buffer[index]
        if size % 2 == 0:
            median = (median + median_buffer[index + 1]) / 2
        return median

    @njit(nogil=True)
    def get_oldest_value(window, dynamics):
        """
        Returns the oldest value of the window that has not yet been overwritten
        :param window: numpy array to treat as cyclic buffer
        :param dynamics: some dynamically changing values to save some parameters
                         dynamics[0]: offset (offset % size = next insertion position)
        """
        assert dynamics[0] >= size, "The buffer needs to be filled to get the oldest value from it."
        return window[int(dynamics[0] % size)]

    @njit(nogil=True)
    def get_newest_value(window, dynamics):
        """
        Returns the latest, most recently added value of the window
        :param window: numpy array to treat as cyclic buffer
        :param dynamics: some dynamically changing values to save some parameters
                         dynamics[0]: offset (offset % size = next insertion position)
        """
        index = int(max(0, dynamics[0]-1) % size)
        return window[index]

    @njit(nogil=True)
    def get_middle_value(window, dynamics):
        """
        Returns the value in the middle of the unsorted window.
        Not to be confused with 'get_median'.
        This method returns only the element that was added at windowsize/2 steps ago.
        :param window: numpy array to treat as cyclic buffer
        :param dynamics: some dynamically changing values to save some parameters
                         dynamics[0]: offset (offset % size = next insertion position)
        """
        assert size % 2 == 1, "The buffer contains an even number of elements, \
                               therefore the middle element cannot be determined."
        assert dynamics[0] >= size, "The buffer needs to be filled to get the middle value from it."
        index = int((dynamics[0]-int(size//2)) % size)
        return window[index]

    @njit(nogil=True)
    def isclose(a, b, epsilon=1e-9):
        """
        Unfortunatly, there is no numba support for numpy.isclose() or math.isclose():
        https://github.com/numba/numba/issues/5977
        """
        return abs(a - b) <= epsilon

    cb = WindowBuffer(array=array, median_array=(np.sort(array) if median_functionality else None),
                      size=size, dynamics=dynamics, median_functionality=median_functionality,
                      block_offset=block_offset, add_value=add_value, get_median=get_median,
                      get_oldest_value=get_oldest_value, get_newest_value=get_newest_value,
                      get_middle_value=get_middle_value)

    return cb
