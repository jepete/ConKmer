"""
conkmer_main.py
ConKmer: COpy Numbers from a strongly unique K-MERs
"""

import argparse
from importlib import import_module  # dynamically import subcommand

from .._version import VERSION, DESCRIPTION
from ..parameters import set_threads
from ..fastcash_main import (
    add_hash_arguments,
    dump,
    optimize,
    )


# 1. preindex
def preindex(p):
    """
    Creates an index (hash table) of all k-mers of given reference genome(s)
    by finding all (strongly unique) k-mers of the input genome(s),
    and store values according to the given value set.

    valueset strongunique.py:
    00: strongly unique
    01: non-unique (but no Hamming-distance-1 neighbor)
    10: weakly unique
    11: non-unique (and has at least one Hamming-distance-1 neighbor)
    """
    # required arguments: --genome and --index
    p.add_argument("--preindex", "-i", metavar="ZARR", required=True,
        help="name of the resulting preindex ZARR folder (output)")
    p.add_argument("--genome", "-g", metavar="FASTA", nargs="+", required=True,
        help="reference FASTA file(s) of the reference genome")
    # optional arguments
    p.add_argument("-T", "--threads", "-j", metavar="INT", type=int,
        help="calculate weak kmers with this number of threads")
    p.add_argument("-c", "--groupprefixlength", metavar="INT", type=int, default=1,
        help="calculate weak k-mers in groups with common prefix of this length [1]")
    p.add_argument("--longwalkstats", action="store_true",
        help="show detailed random walk length statistics")
    p.add_argument('-v', '--valueset', nargs='+',
        default=['strongunique'], metavar="PARAMETER",  # see e. g. fastcash/values/strongunique.py
        help="value set with arguments, implemented in values.{VALUESET}.py")
    add_hash_arguments(p)


# 2. index
def index(p):
    """
    Convert [strongly] unique k-mers from reference genome index (hash table from 1.) to a bitvector
    along each chromosome of the reference genome indicating desired k-mers.
    Building reduced index containing only [strongly] unique k-mers 0-initialized with a 16 bits counter
    """
    # required arguments:
    p.add_argument("--preindex", "-x", metavar="ZARR", required=True,
        help="name of the preindex ZARR folder to process (input)")
    p.add_argument("--index", "-i", metavar="ZARR", required=True,
        help="name of the resulting (reduced) index ZARR folder (output)")
    p.add_argument("--genome", "-g", metavar="FASTA", nargs="+", required=True,
        help="reference FASTA file(s) of the reference genome (input)")
    p.add_argument("--bitvectors", "-o", required=True,
        help="name of the resulting bitvectors ZARR folder (output)")
    p.add_argument("--gcvectors", required=True,
        help="name of the resulting GC conent vectors ZARR folder (output)")
    # optional arguments
    p.add_argument('-v', '--valueset', nargs='+',
        default=['count', '65536'], metavar="PARAMETER",  # see fastcash/values/count.py
        help="value set with arguments ['count 65536']")  # 65536 -> 16 bits
    p.add_argument("--weakunique", "-w", action="store_true",
        help="consider weakly unique AND strongly unique k-mers")
    p.add_argument("--gcwindow", metavar="INT", type=int, default=401,
        help="window size to compute GC content from")
    p.add_argument("--preindexTwo", metavar="ZARR",
        help="name of the second preindex ZARR folder to process (input)")
    p.add_argument("--gapstatistics", "-G", metavar="TSV",
        help="name of the resulting strong unique k-mer gap statistics TSV file")
    p.add_argument("--weakgapstatistics", "-W", metavar="TSV",
        help="name of the resulting unique k-mer gap statistics TSV file")
    p.add_argument("--debuglonggaps", "-D", metavar="LEN", type=int, default=0,
        help="for debugging: if LEN > 0, print long gaps (>= LEN) and their properties")
    add_hash_arguments(p)


# 3. count
# Count in FASTQ sample those k-mers that are present in the reduced index resulting from 2.
# Write a new index containing the k-mers from the reduced index from 2. ([strongly] unique
# in the reference) together with corresponding k-mer counts from FASTQ input
def count(p):
    """
    Count k-mers in input FASTQ reads.
    """
    # required arguments
    p.add_argument("--index", "-i", metavar="ZARR", required=True,
        help="name of the index ZARR folder containing the [strongly] unique k-mers from the reference genome")
    p.add_argument("--fastq", "-q", metavar="FASTQ", required=True,
        help="single or paired-end FASTQ file(s) to count k-mers from")
    p.add_argument("--countindex", metavar="ZARR", required=True,
        help="name of the resulting counts index ZARR folder (output)")
    p.add_argument("--genome", "-g", metavar="FASTA", nargs="+", required=True,
        help="reference FASTA file(s) of the reference genome for count vector construction")
    p.add_argument("--countvecs", metavar="ZARR", required=True,
        help="name of the resulting ZARR folder containing count vector(s) along the genome (output)")
    # optional argument
    p.add_argument("--valuestats", default="conkmer_count_valuestatistics.txt",
        help="name (*.txt), where the value statistics are written to (output)")


# 4. call
# For each chromosome, take the k-mers starting at positions indicated with 1-bits
# ([strongly unique] k-mers) in the bitvector from 2. and build a k-mer counting vector
# plus an optional positions vector
# Find breakpoints in this k-mer counting vector indicating positions of copy number changes
# Write intervals of constant copy numbers
def call(p):
    """
    Call CNVs from indexed strongly unique k-mers and sample k-mer counts (both ZARR input)
    """
    # required arguments
    p.add_argument("--bitvectors", metavar="ZARR", required=True,
        help="name of the input bitvectors ZARR folder where [strongly] unique k-mer start positions are marked")
    p.add_argument("--countvecs", metavar="ZARR", required=True,
        help="name of the input ZARR folder containing count vector(s) along the genome")
    p.add_argument("--gcvecs", metavar="ZARR", required=True,
        help="name of the input ZARR folder created during ConKmer index containing the GC conent vectors")
    p.add_argument("--ccnipath", "-o", required=True,
        help="path into which the CN intervals per sequence (typically chromosome) are written")
    p.add_argument("--ccnifile", required=True,
        help="name of the output file containing constant copy number intervals (ccni)")
    p.add_argument("--gcplot", required=True,
        help="name of the output filename for the plot of local GC content vs. k-mer counter")
    p.add_argument("--corrected_countvecs", metavar="ZARR", required=True,
        help="name of the resulting ZARR folder containing corrected count vector(s) along the genome (output)")
    p.add_argument("--cvstats", required=True,
        help="name of the resulting .txt file containing count value statistics after GC correction (output)")
    # optional arguments
    p.add_argument("--windowsize", metavar="INT", type=int, default=99,
        help="number of k-mer counters per window to take the median from; only odd values supported [99]")
    p.add_argument("--blocksize", metavar="INT", type=int,
        help="size of protected block inside window (infered from windowsize if not given); integer divided by 2 in implementation")
    p.add_argument("--factorws", metavar="INT", type=int, default=10,
        help="genomic regions where 'windowsize' unique successive k-mers span more than this factor * windowsize are excluded from results [10]")
    p.add_argument("--coverage", metavar="INT", type=int,
        help="manually overwrite estimated coverage")
    p.add_argument("--threshold", metavar="INT", type=int,
        help="manually overwrites coverage // 4 (difference between left- and right-sided window to consider their middle as possible breakpoint)")
    p.add_argument("--do_gc_correction", action="store_true",
        help="correct k-mer counts according to the bias of the local GC content")
    p.add_argument("--lowergclimit", metavar="INT", type=int, default=20,
        help="lower local GC content limit in percentage to correct")
    p.add_argument("--uppergclimit", metavar="INT", type=int, default=65,
        help="upper local GC content limit in percentage to correct")
    p.add_argument("-T", "-j", "--threads", metavar="INT", type=int, default=24,
        help="maximum number of worker threads for CNV calling [24]")
    p.add_argument("--maxcount", metavar="INT", type=int, default=65536,
        help="highest count in countindex to determinde number of bits per entry in countvector (only relevant if < 256 or > 65536)")
    p.add_argument("--maxcount_plot", metavar="INT", type=int, default=100,
        help="maximum counter to include into local GC content plot [100]")


# main argument parser
def get_argument_parser():
    """
    return an ArgumentParser object
    that describes the command line interface (CLI)
    of this application
    """
    p = argparse.ArgumentParser(
        description=DESCRIPTION,
        epilog="by Algorithmic Bioinformatics, Saarland University.")
    p.add_argument("--version", action="version", version=VERSION,
        help="show version and exit")

    subcommands = [
        ("preindex",
         "build a k-mer index for the reference genome marking all [strongly] uniques",
         preindex,
         "conkmer_preindex", "main"),
        ("index",
         "initialize count index and build bit vectors from k-mer index for the reference genome",
         index,
         "conkmer_index", "main"),
        ("count",
         "count k-mers in a given index",
         count,
         "conkmer_count", "main"),
        ("call",
         "call CNVs from indexed strongly unique k-mers and sample k-mer counts",
         call,
         "conkmer_call", "main"),
        # imported from srhash
        ("dump",
         "dump all k-mers from a given hash table",
         dump,
         ".dump", "main"),
        ("optimize",
         "optimize the assignment of given k-mer dump",
         optimize,
         ".optimize", "main"),
    ]
    # add global options here
    # (none)
    # add subcommands to parser
    sps = p.add_subparsers(
        description="The ConKmer application supports the following commands. "
        "Run 'conkmer COMMAND --help' for detailed information on each command.",
        metavar="COMMAND")
    sps.required = True
    sps.dest = 'subcommand'
    for (name, helptext, f_parser, module, f_main) in subcommands:
        if name.endswith('!'):
            name = name[:-1]
            chandler = 'resolve'
        else:
            chandler = 'error'
        sp = sps.add_parser(name, help=helptext,
            description=f_parser.__doc__, conflict_handler=chandler)
        sp.set_defaults(func=(module, f_main))
        f_parser(sp)
    return p


def main(args=None):
    p = get_argument_parser()
    pargs = p.parse_args() if args is None else p.parse_args(args)
    set_threads(pargs, "threads")  # limit number of threads in numba/prange
    (module, f_main) = pargs.func
    m = import_module("."+module, __package__)
    mymain = getattr(m, f_main)
    mymain(pargs)
