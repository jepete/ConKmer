import numpy as np
from numba import njit, uint16, uint64, float32
import matplotlib.pyplot as plt
from math import ceil, floor
from scipy.optimize import curve_fit
import seaborn as sns

from fastcash.lowlevel.intbitarray import intbitarray
from fastcash.lowlevel.debug import define_debugfunctions
debugprint, timestamp = define_debugfunctions(debug=True, times=True)


def compile_fill_matrix_per_chr(chr_size, gc_get, maxcount_plot, cutoffs,
                                numpy=True, count_get=None):
    """
    Compiles and returns a function fill_matrix_per_chr
    :param chr_size: chromosome/sequence size
    :param gc_get: getter for GC conent value vector
    :param maxcount_plot: maximum k-mer counter to include into local GC content plot
    :param cutoffs: GC value interval to correct
                    (values outside this interval are not corrected for any GC bias)
    :param numpy: flag indicating whether the k-mer counts will be provided as a numpy array
    :param count_get: getter for k-mer count vector (only for raw k-mer counts)
    """
    assert numpy or ((count_get is not None) and (not numpy)),\
        "count getter needed if it is not a numpy array"

    @njit(nogil=True, locals=dict(gc_value=uint16,
                                  counter=uint16))
    def fill_matrix_per_chr(gca, cta, matrix, stats):
        """
        Fills chromosome-wise a matrix of GC content times k-mer counter
        which will later be plotted as a heatmap
        param gca: (intbit)array with local GC values
        param cta: (intbit)array with corresponding k-mer counts
        param matrix: count occurences of (gc_value, kmer_count) pairs therein
        param stats: counter for k-mers having a k-mer count larger than maxcount_plot
        """
        for i in range(0, chr_size):
            gc_value = gc_get(gca, i)
            if gc_value == 0:
                continue
            stats[1] += 1
            stats[2] += gc_value
            if gc_value < cutoffs[0] or gc_value > cutoffs[1]:
                stats[3] += 1
            if numpy:
                counter = uint16(round(cta[i]))
            else:
                counter = count_get(cta, i)
            if counter > maxcount_plot:
                stats[0] += 1
                continue
            if counter < 1:
                stats[5] += 1
                continue
            matrix[counter][gc_value] += 1
            stats[4] += 1
    return fill_matrix_per_chr


@njit(nogil=True)
def sigmoid(x, L, x0, s, b):
    """
    https://en.wikipedia.org/wiki/Logistic_function

    :param x: x (here: local GC value: number of G and C bases inside gc_windowsize around a k-mer)
    :param L: is responsible for scaling the output range from [0,1] to [0,L]
    :param x0: is the point in the middle of the Sigmoid, i.e. the point where sigmoid should
                originally output the value 1/2 [since if x=x0, we get 1/(1+exp(0)) = 1/2].
    :param s: is responsible for scaling the input, which remains in (-inf,inf)
    :param b: adds bias to the output and changes its range from [0,L] to [b,L+b]

    source: https://stackoverflow.com/questions/55725139/fit-sigmoid-function-s-shape-curve-to-data-using-python
    """
    y = L / (1 + np.exp(-s*(x-x0))) + b
    return (y)


def plot_gcvecs_vs_counts(name_gcvec_countvec, maxcount_plot, window_size, filename_gc_plot,
                          mask, percentage_int_cutoffs, fit=False, gc_corrected=False):
    """
    :param name_gcvec_countvec: dict assigning each sequence name a tuple of its corresponding
                                vector with local GC content values and its k-mer count vector
    :param maxcount_plot: maximum k-mer counter to include into local GC content plot
    :param window_size: size of the window around each [strongly] unique k-mer
                        to compute the local GC content from
    :param filename_gc_plot: output filename of the plot of local GC content vs. k-mer counter
    :param mask: k-mer mask (contgiuous or gapped)
    :param percentage_int_cutoffs: GC content to plot and correct
           (values outside this interval are not corrected for any GC bias)
    :param fit: flag indicating whether to fit sigmoid function through
                lower quartile, upper quartile and medians per GC value column
    :param gc_corrected: flag indicating whether k-mer counts are GC corrected
                         (and therefore provided as numpy array instead of intbitvector)
    """
    matrix = np.zeros((maxcount_plot+1, window_size+1), dtype=np.uint64)

    # stats[0]: counter > maxcount_plot, [1]: gc_value > 0, [2]: total gc_value sum,
    # stats[3]: gc_value outside cutoffs, [4]: inserted into matrix, [5]: k-mer counter < 1
    stats = np.zeros(6, dtype=np.uint64)
    cutoffs = np.array([floor((percentage_int_cutoffs[0]/100)*window_size),
                        ceil((percentage_int_cutoffs[1]/100)*window_size)])

    for triple in name_gcvec_countvec.items():
        gcvec = triple[1][0]
        countvec = triple[1][1]
        assert gcvec.size == countvec.size,\
            f"{gcvec.size=} but {countvec.size=} for sequence {triple[0]}"
        if gc_corrected:
            fill_matrix_per_chr = compile_fill_matrix_per_chr(countvec.size, gcvec.get,
                                                              maxcount_plot, cutoffs, numpy=True)
            fill_matrix_per_chr(gcvec.array, countvec, matrix, stats)
        else:
            fill_matrix_per_chr = compile_fill_matrix_per_chr(countvec.size, gcvec.get, maxcount_plot,
                                                              cutoffs, numpy=False, count_get=countvec.get)
            fill_matrix_per_chr(gcvec.array, countvec.array, matrix, stats)

    if gc_corrected:
        # print max value of matrix:
        print(f"\nmax. value in the GC corrected matrix: {np.max(matrix)}\n")

    k = str(mask.k) if mask.k == mask.w else f"({mask.k},{mask.w})"
    plt.figure(figsize=(9.55, 5.25), tight_layout=True)

    print(f"Average GC content over all {window_size} bp windows with a strongly unique {k}-mer "
          f"in their center: {(stats[2]/(stats[1]*window_size))*100:.2f} %\n{stats[3]:,} "
          f"({(stats[3]/stats[1])*100:.2f} % of {stats[1]:,}) of all these {window_size} "
          f"bp windows have a GC content < {percentage_int_cutoffs[0]} % or "
          f"> {percentage_int_cutoffs[1]} % (not shown)")

    hm = sns.heatmap(matrix, cmap="YlOrRd", vmin=0, vmax=1_400_001)  # vmax may need to be adjusted 1_650_001

    xtick_labels = np.arange(percentage_int_cutoffs[0], percentage_int_cutoffs[1]+1, 5)
    xtick_locations = [(label / 100) * window_size for label in xtick_labels]
    plt.xticks(xtick_locations, xtick_labels, rotation='horizontal')

    hm.set_xlim(cutoffs)
    hm.invert_yaxis()
    hm.set(xlabel='local GC content in %', ylabel='$k$-mer counter')
    # [%, rounded to the next nearest integer]

    # compute values for median line (and lower/upper quartile per matrix column)
    array_length = cutoffs[1]-cutoffs[0]
    lower_quartile_coordinates = np.zeros(array_length, dtype=np.uint16)
    median_coordinates = np.zeros(array_length, dtype=np.uint16)
    upper_quartile_coordinates = np.zeros(array_length, dtype=np.uint16)
    occurrence_counter = np.zeros(2, dtype=np.uint64)

    for i, y in enumerate(range(cutoffs[0], cutoffs[1])):
        occurrence_counter[0] = 0
        for x in range(0, maxcount_plot-1):
            # sum up the number of occurrences of k-mer count x in GC content column y
            occurrence_counter[0] += matrix[x][y]
        # determine the number of values we need to pass from smallest to largest in order to get the respective value
        lower_quartile = uint64(round(occurrence_counter[0]*0.25))
        median = uint64(round(occurrence_counter[0]*0.5))
        upper_quartile = uint64(round(occurrence_counter[0]*0.75))
        occurrence_counter[1] = 0
        for x in range(0, maxcount_plot):
            occurrence_counter[1] += matrix[x][y]
            if occurrence_counter[1] >= lower_quartile:
                lower_quartile_coordinates[i] = x
                # set lower_quartile to the max uint64 value in order to
                # never again get into this case for this GC content y
                lower_quartile = uint64(-1)
            if occurrence_counter[1] >= median:
                median_coordinates[i] = x
                median = uint64(-1)
            if occurrence_counter[1] >= upper_quartile:
                upper_quartile_coordinates[i] = x
                upper_quartile = uint64(-1)
            if gc_corrected and x == 77 and y == 129:
                print(f"{occurrence_counter[0]=}; {occurrence_counter[1]=}; {median=}; {i=}; {y=}")

    hm_bbox = hm.get_position()
    ax2 = hm.twinx()
    ax2.set_ylim(hm.get_ylim())

    x = np.linspace(cutoffs[0], cutoffs[1], cutoffs[1]-cutoffs[0])

    if fit:
        # https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.curve_fit.html
        # this is a mandatory initial guess
        p0_lqs = [max(lower_quartile_coordinates), np.median(x), 1, min(lower_quartile_coordinates)]
        p0_med = [max(median_coordinates), np.median(x), 1, min(median_coordinates)]
        p0_uqs = [max(upper_quartile_coordinates), np.median(x), 1, min(upper_quartile_coordinates)]
        try:
            popt_lqs, _ = curve_fit(sigmoid, x, lower_quartile_coordinates, p0_lqs, method='dogbox')
            popt_med, pcov = curve_fit(sigmoid, x, median_coordinates, p0_med, method='dogbox')
            popt_uqs, _ = curve_fit(sigmoid, x, upper_quartile_coordinates, p0_uqs, method='dogbox')

            y_lq_fit = sigmoid(x, *popt_lqs)
            y_med_fit = sigmoid(x, *popt_med)
            y_uq_fit = sigmoid(x, *popt_uqs)

            ax2.plot(x, y_lq_fit, color='blue')
            ax2.plot(x, y_med_fit, color='green')
            ax2.plot(x, y_uq_fit, color='black')

        except RuntimeError:
            popt_med = None
    else:
        popt_med = None

    ax2.scatter(x, lower_quartile_coordinates, s=1, c='blue', marker='.')
    ax2.scatter(x, median_coordinates, s=1, c='green', marker='.')
    ax2.scatter(x, upper_quartile_coordinates, s=1, c='black', marker='.')

    ax2.grid(False)
    plt.subplots_adjust(bottom=hm_bbox.y0, top=hm_bbox.y1)
    plt.yticks(rotation=0)
    plt.tight_layout()
    plt.savefig(filename_gc_plot, transparent=True)

    return stats, popt_med, median_coordinates


def compile_correct_counts(n, gc_get, cv_get, cv_set, median_kmercounter_avg_gc,
                           median_kmercounters, cutoffs):
    """
    :param n: sequence length in bp
    :param gc_get: getter for GC content value vector
    :param cv_get: getter for k-mer count vector
    :param cv_set: setter for k-mer count vector
    :param median_kmercounter_avg_gc: the median k-mer counter for the average GC content value
    :param median_kmercounters: median k-mer counters for each GC conent value
    :param cutoffs: GC value interval to correct
                    (values outside this interval are not corrected for any GC bias)
    """
    mkc_size = median_kmercounters.size

    @njit(nogil=True, locals=dict(gc_value=uint16,
          corrected_counter=float32, counter=uint64,
          med_kmercounter=uint16))
    def correct_counts(gca, cva, stats, cc_vector):
        """
        :param gca: local GC content value intbitarray
        :param cva: k-mer count intbitarray
        :param stats: numpy array with three positions to collect statistics
        :param cc_vector: numpy floats array to store the GC corrected counts
        """
        for i in range(0, n):
            gc_value = gc_get(gca, i)
            if gc_value == 0:
                continue
            if gc_value < cutoffs[0] or gc_value > cutoffs[1]:
                stats[0] += 1
                continue
            counter = cv_get(cva, i)
            assert gc_value < mkc_size
            med_kmercounter = median_kmercounters[gc_value]
            if med_kmercounter == 0:
                stats[1] += 1
                continue
            # actual GC content bias correction:
            corrected_counter = counter * (median_kmercounter_avg_gc / med_kmercounter)

            cc_vector[i] = corrected_counter
            stats[2] += 1

    return correct_counts


def correct_for_gc_bias(gc_stats, popt_med, name_gcvec_countvec, gcwindow_size,
                        percentage_int_cutoffs, median_coordinates):
    """
    :param gc_stats: numpy array of length 6 filled in fill_matrix_per_chr
        [0]: k-mer counter > maxcount_plot, [1]: gc_value > 0, [2]: total gc_value sum,
        [3]: gc_value inside cutoffs, [4]: inserted into matrix, [5]: k-mer counter < 1
    :param popt_med: optimized parameters by scipy.optimize curve_fit for sigmoid function
    :param name_gcvec_countvec: dict assigning each sequence name a tuple of its corresponding
                                vector with local GC content values and its k-mer count vector
    :param gcwindow_size: size of the window around each [strongly] unique k-mer
                          used for computing local GC content
    :param percentage_int_cutoffs: GC content to plot and correct
                                   (values outside this interval are not corrected for any GC bias)
    :param median_coordinates: median counts for every GC value (only those inside the specified
                        interval are set) to use in case popt_med is None (no fit was possible)
    """
    median_kmercounters = np.zeros(gcwindow_size, dtype=np.uint16)
    cutoffs = np.array([floor((percentage_int_cutoffs[0]/100)*gcwindow_size),
                        ceil((percentage_int_cutoffs[1]/100)*gcwindow_size)])

    if popt_med is None:  # no fitted curve
        offset = 0
        for i in range(0, gcwindow_size):
            if i < cutoffs[0] or i >= cutoffs[1]:
                offset += 1
            else:
                assert (i-offset) >= 0 and (i-offset) < median_coordinates.size
                median_kmercounters[i] = median_coordinates[i-offset]
    else:
        for i in range(0, gcwindow_size):
            median_kmercounters[i] = sigmoid(i, *popt_med)

    average_gc_value = round(gc_stats[2]/gc_stats[1])
    median_kmercounter_avg_gc = median_kmercounters[average_gc_value]
    # sigmoid(average_gc_value, *popt_med)
    stats = np.zeros(3, dtype=np.uint64)
    gc_corrected_counts = dict()

    for triple in name_gcvec_countvec.items():
        name = triple[0]
        gcvec = triple[1][0]
        countvec = triple[1][1]
        cc_vector = np.zeros(gcvec.size, dtype=np.float32)
        correct_counts = compile_correct_counts(gcvec.size, gcvec.get, countvec.get,
                                                countvec.set, median_kmercounter_avg_gc,
                                                median_kmercounters, cutoffs)
        correct_counts(gcvec.array, countvec.array, stats, cc_vector)
        gc_corrected_counts[name] = cc_vector

    print(f"Corrected {stats[2]:,} k-mer counts.\n "
          f" Skipped {stats[0]:,} k-mer counts because their GC content was below "
          f"{percentage_int_cutoffs[0]} % or above {percentage_int_cutoffs[1]} % "
          f"(too sparse data for correction).\n "
          f" Skipped {stats[1]:,} k-mer counts because the median k-mer counter "
          f"for their GC content was 0 and therefore no computation possible.")

    return gc_corrected_counts


def gc_stats_correction(countvecs, info_cv, gcvecs, info_gc, maxcount_plot, filename_gc_plot,
                        mask, lower_gc_limit, upper_gc_limit):
    """
    create a dict name: (GCintbitarray, countvec) name_gcvec_countvec,
    where GCintbitarray contains the GC content values and
    countvec the k-mer counts at the corresponding positions

    :param countvecs: intbitarray containing the local GC content values
                      for each [strongly] unique k-mer start position
    :param info_cv: dictionary containing counterbits and another dict names_to_sizes_cv
    :param gcvecs: local GC content vectors
    :param info_gc: dict containing gcwindow_size, gc_content_bits and a dict names_to_sizes_gc
    :param maxcount_plot: maximum k-mer counter to include into local GC content plot
    :param filename_gc_plot: output filename of the plot of local GC content vs. k-mer counter
    :param mask: k-mer mask (contgiuous or gapped)
    """
    print(f"{filename_gc_plot=}", flush=True)

    names_to_sizes_cv = info_cv['names_to_sizes']
    counterbits = int(info_cv['counterbits'])

    names_to_sizes_gc = info_gc['names_to_sizes']
    gcwindow_size = int(info_gc['gcwindow_size'])
    gc_content_bits = int(info_gc['gc_content_bits'])

    name_to_countvec = dict()
    name_gcvec_countvec = dict()

    # no_curve_fitting = upper_gc_limit-lower_gc_limit > 55 or lower_gc_limit < 15 or upper_gc_limit > 75

    for name, array in countvecs.items():
        # skip scaffolds etc.
        # if 'U00096' in name: no_curve_fitting = True
        if ('_' in name) or ('.' in name and 'U00096' not in name) or ('M' in name):
            continue
        size = names_to_sizes_cv[name]
        countarray = intbitarray(size, counterbits, init=array)
        name_to_countvec[name] = countarray

    for name, array in gcvecs.items():
        size = names_to_sizes_gc[name]
        gcarray = intbitarray(size, gc_content_bits, init=array)
        name_gcvec_countvec[name] = (gcarray, name_to_countvec[name])

    percentage_int_cutoffs = (lower_gc_limit, upper_gc_limit)

    # call plot_gcvecs_vs_counts to get popt_med (optimized parameters)
    gc_stats, popt_med, median_coordinates = \
        plot_gcvecs_vs_counts(name_gcvec_countvec, maxcount_plot, gcwindow_size, filename_gc_plot,
                              mask, percentage_int_cutoffs, fit=False, gc_corrected=False)  # fit=not no_curve_fitting

    gc_corrected_counts = correct_for_gc_bias(gc_stats, popt_med, name_gcvec_countvec,
                                              gcwindow_size, percentage_int_cutoffs,
                                              median_coordinates)

    # replace k-mer counts by GC corrected k-mer counts
    for name, corr_counts in gc_corrected_counts.items():
        name_gcvec_countvec[name] = (name_gcvec_countvec[name][0], corr_counts)

    # plot corrected counts
    filename_gc_plot = filename_gc_plot.replace("kmer-counter", "kmer-counter_corrected")
    gc_stats_corrected, _, _ = plot_gcvecs_vs_counts(name_gcvec_countvec, maxcount_plot,
                                                     gcwindow_size, filename_gc_plot, mask,
                                                     percentage_int_cutoffs, fit=False, gc_corrected=True)

    return gc_stats, gc_corrected_counts
