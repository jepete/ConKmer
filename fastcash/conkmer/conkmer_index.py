"""
conkmer_index:
convert an index (hash table) to a bit vector along the genome
and create a new (reduced) index containing only [strongly] unique k-mers
"""
from os.path import exists
from importlib import import_module
from math import ceil, log2
import numpy as np
from numba import njit, uint16, uint64, int16, int64, byte

from ..lowlevel.bitarray import bitarray
from ..lowlevel.intbitarray import intbitarray
from ..hashio import load_hash, save_hash
from ..srhash import get_npages
from ..kmers import compile_positional_kmer_processor
from ..io.fastaio import all_fasta_seqs
from ..parameters import get_valueset_parameters, parse_parameters
from ..zarrutils import save_to_zgroup
from ..lowlevel.debug import define_debugfunctions
debugprint, timestamp = define_debugfunctions(debug=True, times=True)


DEFAULT_HASHTYPE = "3c_fbcbvb"


def compile_to_bitvec(get_value_and_choice, bv):
    """
    Compiles and return a function to_bitvec
    :param get_value_and_choice: function from hash table namedtuple (key present if choice > 0)
    :param bv: bit vector
    """
    setbit = bv.setquick  # see bitarray.py:120

    @njit(nogil=True, locals=dict(onebit=uint64))
    def to_bitvec(ht, code, pos, bva, bitcounter, validcounter):
        """
        :param ht: hash table
        :param code: canonical k-mer integer code
        :param pos: position inside current sequence
        :param bva: bit vector array
        :param bitcounter: numpy array with one position for counting bits set
                           (how many k-mers are [strongly] unique)
        :param validcounter: numpy array with one position for counting valid
                             (N-free) k-mers (= number this function gets called)
        """
        validcounter[0] += 1
        value, choice = get_value_and_choice(ht, code)
        if choice > 0:  # key present
            setbit(bva, pos, 1)  # pos is set in kmers.py:273
            bitcounter[0] += 1
    return to_bitvec


def compile_compute_local_gc_content(seq, seq_length, window_size):
    """
    Compiles and returns a function compute_local_gc_content
    :param seq: genomic sequence encoded as bytearray
    :param seq_length: length of seq (in bp)
    :param window_size: window size to compute the gc content for

    window_size has to be smaller than 65536 (2^16)
    and should be larger than k.
    The window encompasses window_size//2 bases to the left of the
    k-mer and (window_size//2)-k bases to the right of the k-mer.
    """
    assert window_size < 2**16  # since uint16 is used for gc_sum
    offset_left = window_size // 2
    offset_right = offset_left if window_size % 2 == 0 else offset_left+1

    @njit(nogil=True, locals=dict(gc_sum=uint16,
          one_byte=byte, r=byte, temp=byte, res=byte))
    def compute_local_gc_content(index, counter):
        """
        :param index: current k-mer index around which to compute GC content
        :param counter: to count windows too close at sequence ends and
                        sequence windows with a non-ACGT character
        """
        if index-offset_left < 0:
            counter[0] += 1
            return -1.0
        if index+offset_right > seq_length:
            counter[1] += 1
            return -1.0
        gc_sum = uint16(0)
        for i in range(index-offset_left, index+offset_right+1):
            base = seq[i]
            # count occurences of C (encoded as 1) plus G (encoded as 2)
            if base == 1 or base == 2:
                gc_sum += 1
            if base > 3:  # non-ACGT character
                counter[2] += 1
                return -1.0
        return gc_sum
    return compute_local_gc_content


def compile_pick_kmers_of_interest(kmer_vecsize, get, compute_local_gc_content, set):
    """
    Compiles and returns a function pick_kmers_of_interest
    param kmer_vecsize: size of the intbitarray storing [strongly] unique flags
                        at corresponding sequence positions
    param get: getter function of the named tuple of kmer intbitarray
    param compute_local_gc_content: function pointer to compute the GC content
                                    around a given sequence index
    param set: setter function of the named tuple of GC content intbitarray
    param counter: numpy array as counter to pass into :param compute_local_gc_content
    """
    @njit(nogil=True, locals=dict(gc_sum=int16))
    def pick_kmers_of_interest(kiba, gciba, counter):
        """
        param kiba: k-mer intbitarray.array
        param gciba: GC content inbitarray.array
        """
        for i in range(0, kmer_vecsize):
            kmerstate = get(kiba, i)
            if kmerstate > 0:
                gc_sum = compute_local_gc_content(i, counter)
                if gc_sum < 0:
                    continue
                set(gciba, i, gc_sum)
    return pick_kmers_of_interest


def bit_and_gc_vectors_from_fasta(fastas, gcwindow_size, shp, h, rcmode):
    """
    Return lists of bitvectors containing a bit array for each FASTA sequence.
    In a bit array, each bit represents a k-mer starting position.

    :param fastas: list of FASTA files
    :param gcwindow_size: window size to compute GC content from
    :param shp: k-mer size or shape (tuple)
    :param h: hash data structure, here initialized count index
    :param rcmode: reverse complement mode from 'f', 'r', 'both', 'min', 'max'
    """
    print(f"# Creating bitvector from FASTA, using rcmode={rcmode}")
    get_value_and_choice = h.get_value_and_choice
    table = h.hashtable
    both = (rcmode == "both")
    bitvectors = []
    bitcounter = np.zeros(1, dtype=np.uint64)
    validcounter = np.zeros(1, dtype=np.uint64)
    gc_content_bits = int(ceil(log2(gcwindow_size)))
    gcvectors = []
    total_skipped = np.zeros(3, dtype=np.uint64)
    total_seq_len = uint64(0)

    def value_from_name(name, onetwo):
        n = name.decode()
        if n.startswith("NC_"):
            return 1  # We consider all sequences whose names begin with NC_
        if '_' in n or 'M' in n or 'EBV' in n:
            return 0  # The prefixes NT_ and NW_ denote scaffolds
        if '.' in n and 'U00096' not in n:
            return 0  # ecoli: n = "U00096.3"
        return 1

    for (name, sq, _, _) in all_fasta_seqs(fastas, value_from_name, both, skipvalue=0, progress=True):
        seq_len = len(sq)
        total_seq_len += seq_len

        # bit vectors
        bv = bitarray(seq_len, quickaccess=1)
        to_bitvec = compile_to_bitvec(get_value_and_choice, bv)
        _, process_kmers = compile_positional_kmer_processor(shp, to_bitvec, rcmode)
        # Calling process_kmers(ht, seq, start, end, b1, [b2])
        # will call to_bitvec(ht, code, pos, b1, [b2]) for every valid k-mer
        process_kmers(table, sq, 0, len(sq), bv.array, bitcounter, validcounter)
        bitvectors.append((name, bv))

        # GC content vectors
        gc_content_vec = intbitarray(bv.size, gc_content_bits)
        counter = np.zeros(3, dtype=np.uint64)
        compute_local_gc_content = compile_compute_local_gc_content(sq, seq_len, gcwindow_size)
        pick_kmers_of_interest = compile_pick_kmers_of_interest(bv.size, bv.get, compute_local_gc_content, gc_content_vec.set)
        pick_kmers_of_interest(bv.array, gc_content_vec.array, counter)
        print(f"sequence {name}: {counter[0]:,} {shp}-mers were too close at the start and "
              f"{counter[1]:,} too close at the end of the sequence to wrap a {gcwindow_size} "
              f"bp window around them.\n{counter[2]:,} {shp}-mers were skipped due to a base "
              f"not in 'ACGTacgt' in the {gcwindow_size} bp window.", flush=True)
        gcvectors.append((name, gc_content_vec))
        total_skipped[0] += counter[0]
        total_skipped[1] += counter[1]
        total_skipped[2] += counter[2]
    timestamp(msg=f"Skipped {total_skipped[0]+total_skipped[1]+total_skipped[2]:,} {shp}-mers in "
                  f"total while computing GC contect vectors: "
                  f"{total_skipped[0]:,} {shp}-mers were too close at the start and "
                  f"{total_skipped[1]:,} too close at the end of the sequence "
                  f"to wrap a {gcwindow_size} bp window around them.\n"
                  f"{total_skipped[2]:,} {shp}-mers were skipped due to a base "
                  f"not in 'ACGTacgt' in the {gcwindow_size} bp window.")

    return bitvectors, gcvectors, gc_content_bits, bitcounter, validcounter, total_seq_len


def write_bitvectors_to_zarr(k, shp, bitvectors, outname):
    """
    :param k: k-mer size
    :param shp: k-mer shape
    :param bitvectors: bit vectors indicating [strongly] unique k-mer start positions
    :param outname: output filename
    """
    names_to_sizes = dict([(name.decode(), uint64(bv.size)) for name, bv in bitvectors])
    info = dict(k=k, shp=shp, names_to_sizes=names_to_sizes)
    save_to_zgroup(outname, 'info', **info)
    onebit = {name.decode(): bv.array for name, bv in bitvectors}
    save_to_zgroup(outname, 'onebit', **onebit)


def write_gcvectors_to_zarr(gcvectors, gcwindow_size, outname):
    """
    :param gcvectors: intbitvector of genome size where each position contains the count of G+C
            bases for the window around it if a [strongly] unique k-mer starts there or 0 otherwise
    :param gcwindow_size: size of the window around each [strongly] unique k-mer
                          to compute the local GC content from
    :param outname: output filename
    """
    # recompute number of bits used to store count of G+C bases corresponding to the GC window size
    gc_content_bits = int(ceil(log2(gcwindow_size)))
    names_to_sizes = dict([(name.decode(), uint64(v.size)) for name, v in gcvectors])
    info = dict(gcwindow_size=gcwindow_size, gc_content_bits=gc_content_bits,
                names_to_sizes=names_to_sizes)
    save_to_zgroup(outname, 'info', **info)
    gc_content = {name.decode(): v.array for name, v in gcvectors}
    save_to_zgroup(outname, 'gc_content', **gc_content)


def compile_collect_gaps(name, n, get, maxgap, debug=0):
    """
    Compiles and returns a function collect_gaps
    :param name: sequence name
    :param n: sequence length
    :param get: bit vector quick getter
    :param maxgap: maximum gap size supported
    :param debug: report all gaps equal or larger this value (if debug != 0)
    """
    @njit(locals=dict(g=int64, last=int64))
    def collect_gaps(bva, gps):
        """
        :param bva: bit vector array
        :param gps: gaps numpy array of size maxgap+1
        """
        last = -1
        for i in range(0, n):
            kmerstate = get(bva, i)
            if kmerstate > 0:
                g = min((i - last) - 1, maxgap)
                if debug and g >= debug:
                    start = 0 if last < 1 else last
                    endpos = i
                    print("#   long gap: ", name, g, start, endpos, endpos-start)
                gps[g] += 1
                last = i
                continue
        g = min((n - last) - 1, maxgap)
        gps[g] += 1
    return collect_gaps


def do_gapstatistics(bitvectors, fname, maxgap=31_000_000, debug=0):
    """
    :param bitvectors: bit vectors
    :param fname: file name
    :param maxgap: maximum gap size to report
    :param debug: report all gaps equal or larger this value (if debug != 0)
    """
    if debug:
        print("\n# Gap statistics:")
        print(f"# sequence name, gap length (currently capped at {maxgap:,}, incl. invalid), "
              f"gap start position, gap end position, real gap length")
    gaps = np.zeros(maxgap+1, dtype=np.uint64)
    for name, bv in bitvectors:
        name = name.decode()
        collect_gaps = compile_collect_gaps(name, bv.size, bv.getquick, maxgap, debug)
        collect_gaps(bv.array, gaps)
    # we now have all gaps
    cgaps = np.cumsum(gaps[::-1])[::-1]
    with open(fname, "wt") as fgap:
        print("gap\tfrequency\tcumulative", file=fgap)
        for g in range(gaps.size):
            freq = gaps[g]
            cfreq = cgaps[g]
            if freq:
                print(g, freq, cfreq, sep="\t", file=fgap)


def create_reduced_index(args, info, h):
    """
    Return an empty hashtable of appropriate size to store all [strongly] unique k-mers
    from the reference genome present in the preindex using the valueset for counting and
    [optionally new] (max.) fill ratio from command line. All other parameters stay unchanged
    and are therefore set according to info of preindex.

    :param args: parsed command line arguments
    :param info: preindex info
    :param h: hash data structure, here preindex
    """
    # Obtain parameters from args.preindex / existing hashtable h
    k = int(info['k'])
    shp = info['shp']
    hashtype = info['hashtype'].decode("ASCII")
    aligned = bool(info['aligned'])
    hashfuncs = info['hashfuncs'].decode("ASCII")
    nfingerprints = int(info['nfingerprints'])
    subtables = info['subtables']
    maxwalk = int(info['maxwalk'])
    pagesize = int(info['pagesize'])

    # obtain the parameters
    shortcutbits = args.shortcutbits
    P = get_valueset_parameters(args.valueset, shp=shp)
    (values, valuestr, _, _, parameters) = P
    debugprint(f"# Imported value set '{valuestr}'.")
    debugprint(f"# Dataset parameters: {parameters}")
    parameters = parse_parameters(parameters, args)
    debugprint(f"# Updated parameters: {parameters}")  # only relevant for fill
    (_, _, _, _, _, _, fill) = parameters
    valuehist, _, _, _ = h.get_statistics(h.hashtable)
    valuehist_total = np.sum(valuehist, axis=0)
    if args.weakunique:
        nobjects = valuehist_total[0] + valuehist_total[2]
    else:
        nobjects = valuehist_total[0]
    total = np.sum(valuehist_total)

    # create a new hash table to build the reduced index for counting
    if hashtype == "default":
        hashtype = DEFAULT_HASHTYPE
    if subtables:
        if not hashtype.startswith("s"):
            hashtype = "s" + hashtype  # make it a subtable hash type
    else:
        if hashtype.startswith("s"):
            hashtype = hashtype[1:]  # make it a non-subtable hash type

    hashmodule = import_module("..hash_" + hashtype, __package__)
    debugprint(f"# Use hash type {hashtype}")
    build_hash = hashmodule.build_hash
    universe = int(4**k)
    nvalues = values.NVALUES
    value_update = values.update  # see fastcash/values/count.py
    n = get_npages(nobjects, pagesize, fill) * pagesize
    debugprint(f"# Allocating reduced hash table for up to {n:,} objects ({nobjects:,} needed), "
               f"functions '{hashfuncs}'...")
    h_new = build_hash(universe, n, subtables, pagesize,
                       hashfuncs, nvalues, value_update,
                       aligned=aligned, nfingerprints=nfingerprints,
                       maxwalk=maxwalk, shortcutbits=shortcutbits)
    print(f"# Memory for reduced hash table: {h_new.mem_bytes/(2**20):.3f} MB")
    return h_new, valuestr, fill, total


def compile_fill_reduced_index(h, info, h_new, weakunique, h_preTwo):
    """
    Return a compiled function 'fill_reduced_index' that takes the new (reduced) index
    and fills it with all [strongly] unique k-mers from the reference present in preindex
    :param h: preindex (hash table)
    :param info: info of preindex
    :param h_new: new (reduced) index to store [strongly] unique k-mers
    :param weakunique: args.weakunique flag indicating to additionally consider weak unique k-mers
    :param h_preTwo: second preindex (optional) -> add a k-mer only into count index
                     if it is strongly unique in both preindices
    """
    ht = h.hashtable
    subtables = info['subtables']
    npages = int(info['npages'])
    pagesize = int(info['pagesize'])
    is_slot_empty_at = h.private.is_slot_empty_at
    get_value_at = h.private.get_value_at
    get_signature_at = h.private.get_signature_at
    get_subkey_from_page_signature = h.private.get_subkey_from_page_signature
    get_key_from_subtable_subkey = h.private.get_key_from_subtable_subkey
    store_new = h_new.store_new
    second_preindex = False
    if h_preTwo is not None:
        second_preindex = True
        ht_p2 = h_preTwo.hashtable
        get_value_and_choice = h_preTwo.get_value_and_choice

    @njit(nogil=True, locals=dict(
            page=int64, slot=int64, value=uint64,
            sig=uint64, subkey=uint64, key=uint64))
    def fill_reduced_index(ht_new, counter):
        """
        :param ht_new: new hash table (empty count index)
        :param counter: numpy array with one position to count elements inserted into ht_new
        """
        for subtable in range(subtables):
            for page in range(npages):
                for slot in range(pagesize):
                    if is_slot_empty_at(ht, subtable, page, slot):
                        break
                    value = get_value_at(ht, subtable, page, slot)
                    # Check value
                    if value != 0:  # not strongly unique
                        if not weakunique:
                            continue  # we are not interested in weak k-mers -> abort as early as possible
                        if value != 2:  # not weakly unique
                            continue  # neither strongly unique nor weakly unique k-mer
                    sig = get_signature_at(ht, subtable, page, slot)
                    subkey = get_subkey_from_page_signature(page, sig)
                    key = get_key_from_subtable_subkey(subtable, subkey)
                    if second_preindex:
                        val, choice = get_value_and_choice(ht_p2, key)
                        if choice == 0:  # key not found
                            continue
                        if val != 0:
                            if not weakunique:
                                continue
                            if value != 2:  # not weakly unique
                                continue
                    # Hash key in h_new with 0-initialized counter
                    store_new(ht_new, key, 0)
                    counter[0] += 1

    return fill_reduced_index


def main(args):
    starttime = timestamp(msg=f"Starting ConKmer index: reading preindex {args.preindex}")
    assert 0 < args.gcwindow < 2**16, f"GC window size needs to be smaller than {2**16:,} "\
                                      f"and larger 0."
    assert exists(args.preindex), f"It seems that the preindex file {args.preindex} does not exist."

    # Load hash table (preindex)
    h, values, info = load_hash(args.preindex)
    k = int(info['k'])
    shp = info['shp']
    rcmode = info.get('rcmode', values.RCMODE)
    if isinstance(rcmode, bytes):
        rcmode = rcmode.decode("ASCII")
    for g in args.genome:
        assert exists(g), f"It seems that the genome file {g} does not exist."
    assert 4**k == h.universe, f"Error: k={k}; 4**k={4**k}; but universe={h.universe}"

    timestamp(msg="Building reduced index (hash table) ...")
    # Initialize the new, reduced index (hash table)
    h_new, valuestr, fill, total = create_reduced_index(args, info, h)
    ht_new = h_new.hashtable
    counter = np.zeros(1, dtype=np.uint64)
    insertiontime = timestamp(msg="Begin filling reduced hashtable")
    strongly = "" if args.weakunique else " strongly"

    if args.preindexTwo is not None:  # intersect with second preindex
        readtime = timestamp(msg=f"Reading second preindex {args.preindexTwo} ...")
        assert exists(args.preindexTwo), f"It seems that the second preindex file "\
                                         f"{args.preindexTwo} does not exist."
        h_preTwo, values_preTwo, info_preTwo = load_hash(args.preindexTwo)
        shp2 = info_preTwo['shp']
        assert shp == shp2, f"{shp=} for preindex {args.preindex} but {shp2=} for optional "\
                            f"second preindex {args.preindexTwo}"
        k2 = int(info_preTwo['k'])
        assert k == k2, f"{k=} for preindex {args.preindex} but k={k2} for optional "\
                        f"second preindex {args.preindexTwo}"
        rcmode_preTwo = info_preTwo.get('rcmode', values_preTwo.RCMODE)
        if isinstance(rcmode_preTwo, bytes):
            rcmode_preTwo = rcmode_preTwo.decode("ASCII")
        assert rcmode == rcmode_preTwo
        timestamp(readtime, msg=f"Seconds needed to read second preindex {args.preindexTwo}")
        fill_reduced_index = compile_fill_reduced_index(h, info, h_new, args.weakunique, h_preTwo)
        fill_reduced_index(ht_new, counter)
        timestamp(insertiontime, msg=f"Seconds needed to insert {counter[0]:,}{strongly} unique "
                                     f"{k}-mers (intersection of {args.preindex} and "
                                     f"{args.preindexTwo}) into {args.index}.")
    else:
        fill_reduced_index = compile_fill_reduced_index(h, info, h_new, args.weakunique, None)
        fill_reduced_index(ht_new, counter)
        timestamp(insertiontime, msg=f"Seconds needed to insert {counter[0]:,}{strongly} "
                                     f"unique {k}-mers from {args.preindex} into {args.index}.")

    # build bit vectors:
    meantime = timestamp(msg="Begin construction of bit vectors and GC content vectors")
    bitvectors, gcvectors, gc_content_bits, bitcounter, validcounter, total_seq_len = \
        bit_and_gc_vectors_from_fasta(args.genome, args.gcwindow, shp, h_new, rcmode)
    # bitvectors is a list with bitvectors, one for each FASTA sequence.
    timestamp(msg=f"Set {bitcounter[0]:,} bits in total to 1.")
    print(f"# computed GC content vectors and {len(bitvectors)} bit vectors in")
    timestamp(meantime, msg="seconds")
    timestamp(meantime, msg="minutes", minutes=True)

    summ = sum(bv.popcount(bv.array) for name, bv in bitvectors)  # count 1s in bitvectors
    print(f"\n# Total number of{strongly} unique {k}-mers: {summ:,}")
    print(f"# At {summ:,} of all {int(total_seq_len):,} positions of the given genome"
          f" ($={((summ/total_seq_len)*100):.2f} %) starts a{strongly} unique {k}-mer.")
    print(f"# {summ:,} of all {validcounter[0]:,} N-free {k}-mers of the given genome "
          f"($={((summ/validcounter[0])*100):.2f} %) are{strongly} unique.")
    print(f"# {summ:,} of all {total:,} distinct {k}-mers of the given genome "
          f"({((summ/total)*100):.2f} %) are{strongly} unique.")

    bv_writing_time = timestamp(msg=f"Writing bit vectors file to {args.bitvectors}")
    write_bitvectors_to_zarr(k, shp, bitvectors, args.bitvectors)
    timestamp(bv_writing_time, msg="Finished writing bit vectors file after")

    gcvec_writing_time = timestamp(msg=f"Writing GC content vectors file to {args.gcvectors}")
    write_gcvectors_to_zarr(gcvectors, args.gcwindow, args.gcvectors)
    timestamp(gcvec_writing_time, msg="Finished writing GC content vectors file after")

    if args.gapstatistics:
        gapstats_time = timestamp(msg="Starting gap statistics ...")
        do_gapstatistics(bitvectors, args.gapstatistics, maxgap=31000000, debug=args.debuglonggaps)
        timestamp(gapstats_time, msg="Finished gap statistics after")

    index_writing_time = timestamp(msg=f"Writing (reduced) index file '{args.index}' ...")
    save_hash(args.index, h_new, valuestr,
              additional=dict(k=k, shp=shp, walkseed=args.walkseed,
                              rcmode=rcmode, fillrate=fill, valueset=valuestr))
    timestamp(index_writing_time, msg="Finished writing of index file to disk after")
    timestamp(starttime, msg="total time [sec]")
    timestamp(starttime, msg="total time [min]", minutes=True)
    timestamp(msg="Done.")
