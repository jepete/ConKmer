"""
conkmer.count:
count k-mers in reads (fastq)
The loaded index (hash table) contains only k-mers that are [strongly] unique in the reference.
If a reads k-mer (key) is not present in this hash table, it is ignored/skipped (not counted).
"""
import sys
from os.path import exists
import numpy as np
from numba import njit, uint64
from math import ceil, log2

from ..mathutils import print_histogram
from ..hashio import load_hash, save_hash
from ..lowlevel.intbitarray import intbitarray
from ..io.fastaio import all_fasta_seqs
from ..kmers import compile_positional_kmer_processor
from ..builders_subtables_filter import parallel_build_from_fastq_filter
from ..zarrutils import save_to_zgroup
from ..mask import create_mask
from ..lowlevel.debug import define_debugfunctions
debugprint, timestamp = define_debugfunctions(debug=True, times=True)


def compile_count_non_zero_positions(cv):
    """
    Compiles and returns a function count_non_zero_positions
    :param cv: count vector
    """
    getvalue = cv.get
    size = cv.size

    @njit(nogil=True)
    def count_non_zero_positions(cva, pos_counter):
        """
        :param cva: count vector array
        :param pos_counter: numpy array with one position to count non-zero values in count vectors
        """
        for i in range(size):
            value = getvalue(cva, i)
            if value > 0:
                pos_counter[0] += 1

    return count_non_zero_positions


def compile_to_countvec(get_value, cv, maxvalue, w):
    """
    Compiles and returns a function to_countvec
    :param get_value: function taking a hash table and a key, returning the corresponding value
    :param cv: count vector
    :param maxvalue: maximum possible count value
    :param w: number of gaps (_) + significant positions (#)
              of the used [gapped] k-mer mask (=k for contiguous k-mers)
    """
    setvalue = cv.set

    @njit(nogil=True, locals=dict(value=uint64))
    def to_countvec(ht, code, pos, cva, last_pos, last_values):
        """
        :param ht: hash table
        :param code: canonical k-mer integer code
        :param pos: position inside current sequence
        :param cva: count vector array
        """
        value = get_value(ht, code)
        assert value <= maxvalue
        # Take the count not only for the start position of the k-mer, but for all w positions
        # of the mask. Calculate the median of all counts at each position
        # (sometimes there is more, sometimes less data).
        if pos - last_pos >= w:  # no valid k-mer for at least the last w genome positions
            last_values[:] = np.nan  # fill entire array with NANs ...
            last_values[pos % w] = value  # ... except the new value
        else:
            for i in range(last_pos[0], pos):
                # fill all positions since last valid k-mer with NANs
                last_values[i % w] = np.nan
            last_values[pos % w] = value
            # compute numpy median from all values in last_values != NAN
            value = int(np.nanmedian(last_values))
        setvalue(cva, pos, value)
        last_pos[0] = pos
    return to_countvec


def countvectors_from_fasta(fastas, mask, h, rcmode, maxvalue, skipvalue=(-1)):
    """
    For each chromosome, take the k-mers starting at positions indicated with 1-bits
    ([strongly] unique k-mers) in the bitvector from 2. and build a k-mer counting vector.
    Returns a list countvectors. This list contains a (bit) array for each FASTA sequence.
    These arrays contain for each genome position where a [strongly] unique k-mer starts
    the number of occurences of this k-mer in the sequencing reads processed in the previous step.

    :param fastas: list of FASTA files
    :param mask: k-mer mask (includes k, w, shape etc.)
    :param h: hash data structure
    :param rcmode: reverse complement mode (from 'f', 'r', 'both', 'min', 'max')
    :param maxvalue: highest count to store
                     (used to compute number of necessary bits per genome base position)
    :param skipvalue: value that skips a FASTA entry
    """
    print(f"# Creating countvector(s) from FASTA, using rcmode={rcmode}")
    get_value = h.get_value
    table = h.hashtable
    both = (rcmode == "both")
    countvectors = []
    min_needed_bits = int(ceil(log2(maxvalue)))
    # Ensure that counterbits is a power of two. Otherwise quickaccess can't deal with it
    counterbits = 2**(ceil(log2(min_needed_bits)))
    total_seq_len = uint64(0)
    non_zero_positions = uint64(0)

    def value_from_name(name, onetwo):
        n = name.decode()
        if n.startswith("NC_"):
            return 1  # We consider all sequences whose names begin with NC_
        if '_' in n or 'M' in n:  # we are currently not interested in mitochondrial DNA
            return 0  # The prefixes NT_ and NW_ denote scaffolds
        if '.' in n and 'U00096' not in n:
            return 0  # ecoli: n = "U00096.3"
        if 'EBV' in n:
            return 0  # exclude contig of Epstein-Barr virus sequence
        return 1

    for (name, sq, _, _) in all_fasta_seqs(fastas, value_from_name, both, skipvalue=0, progress=True):
        n = len(sq)
        total_seq_len += n
        last_pos = np.zeros(1, dtype=np.uint64)
        last_values = np.zeros(mask.w, dtype=np.int32)
        # Be careful: quickaccess can only deal with a power of 2
        cv = intbitarray(n, counterbits)  # countvector
        print(f"{mask.w=}")
        to_countvec = compile_to_countvec(get_value, cv, maxvalue, mask.w)
        _, kmer_process = compile_positional_kmer_processor(mask.tuple, to_countvec, rcmode)
        cva = cv.array
        kmer_process(table, sq, 0, len(sq), cva, last_pos, last_values)
        countvectors.append((name, cv))
        pos_counter = np.zeros(1, dtype=np.uint64)
        count_non_zero_positions = compile_count_non_zero_positions(cv)
        count_non_zero_positions(cva, pos_counter)
        non_zero_positions += pos_counter[0]
        print(f"Sequence {name.decode()}: For {pos_counter[0]:,} of {n:,} genome base positions "
              f"({((pos_counter[0]/n)*100):.2f} %), there is a k-mer count greater than 0.")
    return countvectors, counterbits, total_seq_len, non_zero_positions


def write_countvecs_to_zarr(k, shp, countvectors, counterbits, coverage, outname):
    """
    :param k: k-mer size
    :param shp: k-mer shape
    :param countvectors: count vectors
    :param counterbits: number of bits used for counting (max counter = 2^counterbits⁻1)
    :param coverage: estimated average coverage of used reads
    :param outname: output filename
    """
    names_to_sizes = dict([(name.decode(), uint64(b.size)) for name, b in countvectors])
    info = dict(k=k, shp=shp, counterbits=counterbits,
                coverage=coverage, names_to_sizes=names_to_sizes)
    save_to_zgroup(outname, 'info', **info)
    counts = {name.decode(): b.array for name, b in countvectors}
    save_to_zgroup(outname, 'counts', **counts)


def main(args):
    starttime = timestamp(msg="ConKmer count: start counting k-mers in fastq file(s)")
    # Load hash table (index)
    assert exists(args.index), f"It seems that the index file {args.index} does not exist."
    assert exists(args.fastq), f"It seems that the FASTQ path {args.fastq} does not exist."
    for g in args.genome:
        assert exists(g), f"It seems that the genome file {g} does not exist."

    print(f"# Loading index file from {args.index} ...")
    h, values, info = load_hash(args.index)
    k = int(info['k'])
    shp = info['shp']
    rcmode = info.get('rcmode', values.RCMODE)
    if isinstance(rcmode, bytes):
        rcmode = rcmode.decode("ASCII")

    # count k-mers from FASTQ file(s)
    counttime = timestamp(msg=f"Begin {k}-mer counting from FASTQ file(s) in {args.fastq}")
    # number of threads will be number of subtables + 1
    total, failed, walkstats = parallel_build_from_fastq_filter(args.fastq, shp, h, (1, 1),
                                                                update=h.private.update_existing_ssk,
                                                                rcmode=rcmode, maxfailures=-1)
    # maxfailures=-1 results in the largest possible uint64 value
    # update_existing_ssk is update_existing with allow_new=False
    # (see hash_s3c_fbcbvb.py:274, 614, 755), ssk stands for subtable subkey
    assert total > 0, f"No valid k-mers found at {args.fastq}"  # abort early
    timestamp(counttime, msg=f"Minutes needed to process {total:,} {k}-mers in total "
                             f"({failed:,} {k}-mers were unsuccessfully processed)", minutes=True)

    statstime = timestamp(msg="Collecting statistics ...")
    valuehist, fillhist, choicehist, shortcuthist = h.get_statistics(h.hashtable)
    valuehist_sum = np.sum(valuehist, axis=0)

    # Write value statistics (count: #occurrence) to .txt file
    print(f"Value statistics are written to {args.valuestats} ...\n")
    orig_stdout = sys.stdout  # store original stdout object
    sys.stdout = open(args.valuestats, 'w')  # redirect all prints to this file
    print_histogram(valuehist_sum, shorttitle="values", fractions="%", emptyline=False,
                    title="Value statistics (value: number of occurrences (%)):")
    sys.stdout.close()  # ordinary file object
    sys.stdout = orig_stdout  # restore print commands to interactive prompt

    coverage = np.argmax(valuehist_sum[1:])+1
    print(f"estimated {coverage=}\n")

    print_histogram(np.sum(fillhist, axis=0), title="Page fill statistics:",
                    shorttitle="fill", fractions="%", average=True, nonzerofrac=True)
    print_histogram(np.sum(choicehist, axis=0), title="Choice statistics:",
                    shorttitle="choice", fractions="%+", average="+")

    # multiply the values (counts) with the number of their occurence
    countersum = np.arange(len(valuehist_sum)).dot(valuehist_sum)
    timestamp(statstime, msg="Seconds needed for statistics")
    print(f"{countersum:,} of {total:,} processed {k}-mers ({((countersum/total)*100):.2f}"
          f" %) were already present in the loaded index ([strongly] unique in the reference).")

    countindex_writing_time = timestamp(msg=f"Writing countindex file {args.countindex} ...")
    save_hash(args.countindex, h, info['valueset'], info)
    timestamp(countindex_writing_time, msg="Finished writing of countindex file to disk")

    assert 4**k == h.universe, f"Error: k={k}; 4**k={4**k}; but universe={h.universe}"
    mask = create_mask(shp)

    # build count vectors
    cvtime = timestamp(msg="Begin count vector construction")
    countvectors, counterbits, total_seq_len, non_zero_positions = \
        countvectors_from_fasta(args.genome, mask, h, rcmode, values.NVALUES-1)
    timestamp(cvtime, msg="Minutes needed to finish count vector construction", minutes=True)
    print(f"# For {non_zero_positions:,} of {uint64(total_seq_len):,} genome base positions "
          f"({((non_zero_positions/total_seq_len)*100):.2f} %), there is a k-mer count greater than 0.")

    # Write countvectors to disk according to bitvectors in conkmer_index
    writetime = timestamp(msg="Writing count vectors to disk ...")
    write_countvecs_to_zarr(k, shp, countvectors, counterbits, coverage, args.countvecs)
    timestamp(writetime, msg="Seconds needed to write count vectors to disk")

    timestamp(starttime, msg="total time [sec]")
    timestamp(starttime, msg="total time [min]", minutes=True)
    timestamp(msg="Done.")
