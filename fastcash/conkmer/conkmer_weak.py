"""
conkmer_weak:
Efficiently find weak k-mers
Mainly uses xengsort_weak.py
"""
from numba import njit, int64, uint64

from ..parameters import get_valueset_parameters
from .xengsort_weak import calculate_weak_set, have_hamming_dist_one


# only function from xengsort_weak.py that differs for conkmer
def compile_mark_weak_kmers_in_suffix_conkmer(
        block_prefix_length,  # Configure search for HD 1 neighbors only in blocks
        # where the prefix of this length is constant
        k,  # the k-mer size (must be >= prefix_length)
        WEAKMASK,  # bit mask with a single 1-bit that marks weak k-mers
        VALUEBITS  # number of bits to store values
        ):
    """
    Compile and return a function 'mark_weak_kmers_in_suffix(codes)'
    that examines a sorted array of aggregated k-mer encodings and values
    and decides which k-mers are weak.
    Array 'codes' stores both kmer codes and values: [kmercode|value]
    The number of bits for the value is VALUEBITS.
    """
    if k <= block_prefix_length:
        raise ValueError(f"{k=} <= {block_prefix_length=}")
    suffix_bits = 2*k - 2*block_prefix_length + VALUEBITS
    assert VALUEBITS >= 1, "VALUEBITS has to be at least 1"

    @njit(nogil=True,  locals=dict(
            ncodes=int64, start=int64, end=int64,
            pos=int64, pos2=int64, prefix=uint64,
            element=uint64, sec_element=uint64))
    def mark_weak_kmers_in_suffix(codes):
        """
        :param codes: array storing both kmer codes and values: [kmercode|value]
        """
        ncodes = codes.size
        start = end = 0
        while start < ncodes - 1:  # nothing to do if start == ncodes-1
            prefix = codes[start] >> suffix_bits
            # assert end == start
            while (end < ncodes) and (prefix == uint64(codes[end] >> suffix_bits)):
                end += 1
            for pos in range(start, end):
                element = codes[pos] >> VALUEBITS
                found = False
                for pos2 in range(pos+1, end):
                    sec_element = codes[pos2] >> VALUEBITS
                    if have_hamming_dist_one(element, sec_element):
                        found = True
                        codes[pos2] |= WEAKMASK
                if found:
                    codes[pos] |= WEAKMASK
            start = end
    return mark_weak_kmers_in_suffix


def mark_weak_kmers(h, args):
    """
    :param h: hash table named tuple
    :param args: parsed command line parameters
    """
    # obtain the parameters (again)
    P = get_valueset_parameters(args.valueset, shp=args.kmersize, rcmode=args.rcmode)
    (values, valuestr, rcmode, mask, parameters) = P
    k = mask.k
    nextchars = 3
    vbits = values.bits  # see fastcash/values/strongunique.py
    assert vbits == 2
    # the function does its own timestamp/debug output
    # call goes to xengsort_weak.py, line 346
    calculate_weak_set(h, k, args.groupprefixlength, nextchars, rcmode=rcmode, threads=args.threads,
                       WEAKMASK=uint64(2), VALUEBITS=vbits, value_weak_bit=2,
                       cmwkis_function=compile_mark_weak_kmers_in_suffix_conkmer)
    # VALUEBITS=2 according to fastcash/values/strongunique.py
    # WEAKMASK=uint64(2) because for strongunique.py the bit with value 2 indicates weakness
