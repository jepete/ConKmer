"""
conkmer_call: CNV calling
"""
import os
from os.path import exists
import numpy as np
from numba import njit, uint16, uint32, uint64, float32, boolean
import subprocess
from concurrent.futures import ThreadPoolExecutor, as_completed

from ..lowlevel.intbitarray import intbitarray
from .gc_correction_and_plotting import gc_stats_correction
from .windowbuffer import create_windowbuffer
from ..zarrutils import load_from_zgroup, save_to_zgroup
from ..mask import create_mask
from ..lowlevel.debug import define_debugfunctions
debugprint, timestamp = define_debugfunctions(debug=True, times=True)


def compile_possible_breakpoints(size, bv_get, window_size, blocksize, threshold, factor_ws):
    """
    Compiles and returns the function possible_breakpoints.
    :param size: size of the current count int bit vector (typically the size of a chr)
    :param bv_get: getter for the bit vector
                   where starting positions of strongly unique k-mers are marked
    :param window_size: number of counters to compute the median from
    :param blocksize: number of counters in the middle of the window, wherein the median is
                      allowed to shift, but resorting has to be done in case of changes
    :param threshold: count threshold to consider left and right window medians
                      as prominent median difference / possible CNV breakpoint
    :param factor_ws: a gap is inserted if the leftmost index of the left window and the current
                index (rightmost of right window) differ by more than factor_ws * window_size
    """
    # Create cyclic window buffers and references to their methods needed
    window = create_windowbuffer(window_size, np.float32, blocksize,
                                 median_functionality=True)
    wind_add = window.add_value
    wind_get_median = window.get_median

    indices = create_windowbuffer(window_size, np.uint64, blocksize)
    ind_add = indices.add_value
    ind_get_middle = indices.get_middle_value

    future_medians = create_windowbuffer(window_size, np.float32, blocksize)
    fm_add = future_medians.add_value
    fm_get_oldest = future_medians.get_oldest_value

    indices_future_medians = create_windowbuffer(window_size, np.uint64, blocksize)
    ifm_add = indices_future_medians.add_value
    ifm_get_oldest = indices_future_medians.get_oldest_value

    @njit(nogil=True, locals=dict(factor_ws=uint64, already_inside_gap=boolean,
          kmers_since_last_breakpoint=uint64, breakpoint_counter=uint64,
          gip_counter=uint64, count=float32, right_median=float32, diff=float32,
          index_right_median=uint64, left_median=float32, leftmost_index=uint64))
    def possible_breakpoints(bit_array, countvec, bpi, gip, wind, wind_dynamics,
                             mb, ind, ind_dynamics, fm, fm_dynamics, ifm, ifm_dynamics, counter):
        """
        :param bit_array: bit array where [strongly] unique k-mer start positions are marked with 1
        :param countvec: float32 numpy array with [strongly] unique k-mer counts
        :param bpi: numpy array to store possible CNV breakpoints indices
        :param gip: gap index pairs: two-dimensional numpy array to store start and end indices
                    of gaps (segments with a too low density of [strongly] unique k-mers)
        :param wind: window cyclic buffer
        :param wind_dynamics: dynamic values of window cyclic buffer 'wind'
        :param mb: median_buffer: representation of 'wind' to infer the median from
                                  in constant time, therfore partly sorted
        :param ind: genomic positions (indices) corresponding to values in window 'wind'
        :param ind_dynamics: dynamic values of indices cyclic buffer 'ind'
        :param fm: future medians (cyclic buffer)
        :param fm_dynamics: dynamic values of future medians cyclic buffer 'fm'
        :param ifm: indices of future medians (cyclic buffer)
        :param ifm_dynamics: dynamic values of indices of future medians cyclic buffer 'ifm'
        :param counter: numpy array to collect some statistics
                        counter[0]: count all k-mers we have a count for
        """
        already_inside_gap = 0
        kmers_since_last_breakpoint = 0
        breakpoint_counter = 0  # Unfortunatly, numba does not support x = y = z = 0
        gip_counter = 0  # gap index pairs counter

        for i in range(0, size):
            if bv_get(bit_array, i):
                count = countvec[i]
                wind_add(wind, count, wind_dynamics, mb)  # add count to the window
                ind_add(ind, i, ind_dynamics)  # add the current index to the indices
                counter[0] += 1
                if counter[0] < window_size:
                    continue  # right window not full yet

                right_median = wind_get_median(mb, wind_dynamics)
                index_right_median = ind_get_middle(ind, ind_dynamics)
                # add median of right window to future medians (left window)
                fm_add(fm, right_median, fm_dynamics)
                # add the index of the median of the right window
                # to the indices of future medians (left window)
                ifm_add(ifm, index_right_median, ifm_dynamics)
                if counter[0] < 2*window_size:
                    continue  # left window not full yet

                left_median = fm_get_oldest(fm, fm_dynamics)
                leftmost_index = ifm_get_oldest(ifm, ifm_dynamics)
                diff = right_median - left_median

                # check if leftmost_index and the current index i are more than
                # factor_ws times the window size apart from each other:
                if (i - leftmost_index) > (factor_ws * window_size):
                    if not already_inside_gap:  # start the gap
                        already_inside_gap = 1
                        # check if the last breakpoint index is larger than the current one
                        while bpi[breakpoint_counter-1] >= leftmost_index:
                            assert breakpoint_counter >= 1
                            breakpoint_counter -= 1  # make sure that it will be overwritten
                        # set an artificial breakpoint
                        bpi[breakpoint_counter] = leftmost_index  # set an artificial breakpoint
                        breakpoint_counter += 1
                        gip[0][gip_counter] = leftmost_index
                else:  # window_size [strongly] unique k-mers are distributed over not more than factor_ws*window_size base pairs -> assume sufficient data density
                    if already_inside_gap:  # end the gap
                        already_inside_gap = 0
                        bpi[breakpoint_counter] = leftmost_index  # set an artificial breakpoint
                        breakpoint_counter += 1
                        gip[1][gip_counter] = leftmost_index
                        gip_counter += 1
                    if diff > threshold or diff < -threshold:  # check if the median difference
                        # is larger than a threshold (typically coverage // 4)
                        # define minimal difference between two breakpoints
                        # (all below not detectable with window_size)
                        if kmers_since_last_breakpoint > (window_size / 2):  # good idea?
                            bpi[breakpoint_counter] = index_right_median
                            breakpoint_counter += 1
                        kmers_since_last_breakpoint = 0
                    else:
                        kmers_since_last_breakpoint += 1
        if already_inside_gap:  # end the gap
            gip[1][gip_counter] = size

    return possible_breakpoints, window, indices, future_medians, indices_future_medians


def compile_median_count(get_bv):
    """
    Compiles and returns the function median_count
    :param get_bv: getter for bitvector
    """
    @njit(nogil=True, locals=dict(index_counter=uint32))
    def median_count(array, start, end, bva):
        """
        Computes the median value from index start to index end in the numpy array 'array'
        at strongly unique k-mer start positions
        :param array: numpy array, here: containing the [gc corrected] k-mer counts
        :param start: start index
        :param end: end index
        :param bva: bit vector array
        """
        assert end > start
        index_counter = 0
        kmer_counts = np.zeros(end-start, dtype=float32)
        for i in range(start, end):
            if get_bv(bva, i):
                kmer_counts[index_counter] = array[i]
                index_counter += 1
        if index_counter > 0:
            median_kmer_count = np.median(kmer_counts[:index_counter])
            return median_kmer_count
        else:
            print("WARNING: There are no [strongly] unique k-mers in the given window: \
                   start, end", start, end)
            return 0
    return median_count


def write_constant_copy_numbers(coverage, name, size, countvec, bv,
                                breakpoint_indices, ccnifile, gip):
    """
    Intervals of constant copy numbers are written to the given TSV file 'ccnifile'.
    :param coverage: estimated coverage or overwritten by manually set parameter
    :param name: name of the sequence / chromosome
    :param size: size of the sequence / chromosome in bp
    :param countvec: numpy array containing the [gc corrected] k-mer counts
    :param breakpoint_indices: indices of possible CNV breakpoints
    :param ccnifile: output TSV file where the constant copy number intervals are written
    :param gip: gap index pairs: two-dimensional numpy array storing start and end indices of gaps
                (segments with a too low density of k-mers) which should be excluded from output
    """
    assert countvec.size > 0
    chrom = "" if "chr" in name else "chr"
    median_count = compile_median_count(bv.get)

    with open(ccnifile, "w") as outfile:
        startpos = 0  # coordinates are 0-based, half-open
        next_startpos_to_write = 0
        last_cn_int = -1
        gip_counter = 0  # gap index pair (gip) counter
        for breakp in breakpoint_indices:
            if startpos == breakp:  # skip empty intervals
                continue

            cn_float = (median_count(countvec, startpos, breakp, bv.array)*2)/coverage
            cn_int = int(round(cn_float))
            if cn_int != last_cn_int and next_startpos_to_write != startpos:  # start new interval -> write old interval
                length = startpos-next_startpos_to_write
                if length >= 50:
                    cn = (median_count(countvec, next_startpos_to_write, startpos, bv.array)*2)/coverage
                    int_cn = int(round(cn))
                    output = f"{chrom}{name}\t{next_startpos_to_write}\t{startpos}\t"\
                             f"{length}\t{int_cn}\t{cn:.2f}\n"
                    outfile.write(output)
                next_startpos_to_write = startpos
            last_cn_int = cn_int

            gap_startpos = gip[0][gip_counter]
            if breakp == gap_startpos:  # gap started (all gap start positions are included in breakpoint_indices)
                gap_endpos = gip[1][gip_counter]
                gip_counter += 1
                length = gap_startpos-next_startpos_to_write
                if length >= 50:
                    cn_float = (median_count(countvec, next_startpos_to_write, gap_startpos, bv.array)*2)/coverage
                    cn_int = int(round(cn_float))
                    output = f"{chrom}{name}\t{next_startpos_to_write}\t{gap_startpos}\t"\
                             f"{gap_startpos-next_startpos_to_write}\t{cn_int}\t{cn_float:.2f}\n"
                    outfile.write(output)
                last_cn_int = -1
                next_startpos_to_write = gap_endpos
                startpos = gap_endpos  # set the next interval start position
            else:
                startpos = breakp  # set the next interval start position

        if size != gip[1][gip_counter] and next_startpos_to_write != size:
            cn_float = (median_count(countvec, next_startpos_to_write, size, bv.array)*2)/coverage
            cn_int = int(round(cn_float))
            output = f"{chrom}{name}\t{next_startpos_to_write}\t{size}\t"\
                         f"{size-next_startpos_to_write}\t{cn_int}\t{cn_float:.2f}\n"
            outfile.write(output)


def do_breakpoints_and_CN_intervals(name, size, bitarray, coverage, countvec, windowsize,
                                    blocksize, threshold, factorws, ccnipath):
    """
    Called for each sequence/chromosome.
    Initializes some arrays, calls the function that determines possible breakpoints of CN change
    and the function that writes intervals of constant copy numbers using those breakpoints.
    :param name: sequence name
    :param bitarray: bit array where [strongly] unique k-mer start positions are marked with a 1
    :param windowsize: size of the sliding window for median count computation
    :param factorws: genomic regions where 'windowsize' successive k-mers span more than
                     this factor * windowsize are excluded from results
    :param ccnipath: folder to store TSV file of constant copy number intervals (ccni)
    """
    # it was bitarray, but bitarray constructor does not support init yet
    bv = intbitarray(size, 1, init=bitarray)
    size_breakpoints_indices = int(size // windowsize)  # upper bound, much less in practice
    breakpoint_indices = np.zeros(size_breakpoints_indices, dtype=np.uint32)
    gip = np.zeros((2, size_breakpoints_indices), dtype=np.uint32)
    counter = np.zeros(1, dtype=np.uint64)
    ccnipath += f"/{name}_CN_intervals.tsv"

    possible_breakpoints, window, indices, future_medians, indices_future_medians = \
        compile_possible_breakpoints(size, bv.get, windowsize, blocksize, threshold, factorws)

    possible_breakpoints(bv.array, countvec, breakpoint_indices, gip, window.array,
                         window.dynamics, window.median_array, indices.array, indices.dynamics,
                         future_medians.array, future_medians.dynamics,
                         indices_future_medians.array, indices_future_medians.dynamics, counter)

    breakpoint_indices = breakpoint_indices[breakpoint_indices != 0]

    if breakpoint_indices.size < 1:
        return
    write_constant_copy_numbers(coverage, name, size, countvec, bv,
                                np.sort(breakpoint_indices), ccnipath, gip)


def counts_to_breakpoints(bitvectors, countvectors, counterbits,
                          coverage, args, names_to_sizes_cv, gapped):
    """
    Checks, initializes and prints some parameters, then starting threads to compute
    intervals of constant copy numbers in parallel for different sequences/chromosomes.
    Afterwards, the TSV files (one per sequence) are concatenated to a single final one.
    :param bitvectors: dict where sequence names map to bitvectors
                       indicating where [strongly] unique k-mers start
    :param countvectors: dict where sequence names map to countvector
                         containing [strongly] unique k-mer counts from used reads
    :param counterbits: number of bits used for each counter (max. counter = 2^counterbits - 1)
    :param args: parsed command line arguments
    :param names_to_sizes_cv: dict mapping sequence names to vector sizes
    :param gapped: flag indicating whether we are dealing with gapped k-mers (w > k)
    """
    assert args.factorws >= 2, "It is not possible that two windows of z [strongly] unique "\
                               "k-mer counters span less than 2*z bp."

    if args.coverage:
        if coverage / args.coverage < 0.8 or coverage / args.coverage > 1.2:
            print(f"WARNING: estimated coverage = {coverage} and {args.coverage=}, "
                  f"but setting coverage to {args.coverage=}.")
        coverage = args.coverage
    if not args.threshold:
        threshold = max(1, coverage // 4)  # good idea?
    else:
        threshold = args.threshold

    if not args.blocksize:
        # best (optimal) fit for windows of size 99, 199, 299, 399 and 499 (lowest resort rate):
        blocksize_raw = round(0.030 * args.windowsize + 7.430)
        if blocksize_raw >= args.windowsize:
            blocksize_raw = args.windowsize // 2
        blocksize = blocksize_raw if blocksize_raw % 2 == 0 else blocksize_raw + 1
    else:
        blocksize = args.blocksize

    if blocksize > args.windowsize:
        print(f"WARNING: {blocksize=} > {args.windowsize=}, therefore setting blocksize to "
              f"args.windowsize. This means that the window must be re-sorted for each counter, "
              f"which can result in a long runtime, please reconsider the choice of parameters.")
        blocksize = args.windowsize

    if not os.path.isdir(args.ccnipath):  # ccni: constant copy number intervals
        os.makedirs(args.ccnipath)

    n_sequences = 0
    for name, array in bitvectors.items():
        if ('_' in name) or (('.' in name) and ('U00096' not in name)) or ('M' in name) or ('hs37d5' in name):
            print(f"WARNING: Filtering necessary due to sequence {name}")
            continue
        n_sequences += 1

    if args.threads:
        n_sequences = min(args.threads, n_sequences)

    print(f"\nParameters:\n{args.windowsize=}\n{args.factorws=}\n{args.blocksize=}\n{blocksize=}"
          f"\n{args.coverage=}\n{coverage=}\n{args.threshold=}\n{threshold=}\n{args.ccnipath=}\n", flush=True)

    with ThreadPoolExecutor(max_workers=n_sequences) as executor:
        futures = [
            executor.submit(do_breakpoints_and_CN_intervals, name, names_to_sizes_cv[name],
                            bitarray, coverage, countvectors[name], args.windowsize, blocksize,
                            threshold, args.factorws, args.ccnipath)
            for name, bitarray in bitvectors.items()]
        for fut in as_completed(futures):
            pass
        # done with all futures

    # Concatenate all files in the folder args.ccnifile in lexicographical ascending order
    subprocess.run(f"cat {args.ccnipath}/* > {args.ccnifile}",
                   shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    print(f"wrote tsvfile {args.ccnifile=}\n")


def write_countvecs_to_zarr(countvectors, info, outname):
    """
    :param countvectors: count vectors
    :param info: info dictionary as created originally (in conkmer_count)
    :param outname: output filename
    """
    save_to_zgroup(outname, 'info', **info)
    save_to_zgroup(outname, 'counts', **countvectors)


def compile_fill_count_value_stats(n):
    """
    Compiles and returns a function fill_count_value_stats
    :param n: sequence length in bp
    :param get: getter for k-mer count intbitvector
    """
    @njit(nogil=True, locals=dict(count=float32))
    def fill_count_value_stats(cva, count_value_stats):
        """
        Rounds each (GC corrected) k-mer count to its next nearest integer
        and counts how often each integer rounded k-mer count occurs.
        This function gets called once per sequence (typically a chromosome).
        :param cva: k-mer count vector array
        :param count_value_stats: numpy array to count different k-mer counts
        """
        for i in range(0, n):
            count = cva[i]
            if count < 0.001:
                continue
            count_value_stats[uint16(round(count))] += 1

    return fill_count_value_stats


def write_valuestats_corrected_countvecs(countvectors, names_to_sizes_cv, counterbits, outname):
    """
    :param countvectors: k-mer count intbitvectors
    :param names_to_sizes_cv: dictionary mapping sequence names
                              to corresponding sequence lengths in bp
    :param counterbits: number of bits used for each k-mer counter
    :param outname: output file name
    """
    count_value_stats = np.zeros(2**counterbits, dtype=np.uint64)
    for name, array in countvectors.items():
        if ('_' in name) or ('.' in name and 'U00096' not in name) or ('M' in name) or ('hs37d5' in name):
            continue
        size = names_to_sizes_cv[name]
        fill_count_value_stats = compile_fill_count_value_stats(size)
        fill_count_value_stats(array, count_value_stats)

    # print count_value_stats to outname
    with open(outname, "w") as outfile:
        outfile.write("Value statistics after GC correction (value: number of occurrences):\n")
        for i in range(0, count_value_stats.size):
            if count_value_stats[i] != 0:  # != 0 just for better traceability
                outfile.write(f"{i}: {count_value_stats[i]}\n")


def compile_intbitvec_to_numpyarray(getter):
    """
    Compiles and returns a function intbitvec_to_numpyarray
    :param getter: getter for intbitvector array
    """
    @njit(nogil=True)
    def intbitvec_to_numpyarray(intbitvec_array, size, numpy_array):
        """
        Writes each value from 'intbitvec_array' to 'numpy_array' if both are of equal size.
        :param intbitvec_array: intbitvector array
        :param size: size of 'intbitvec_array'
        :param numpy_array: target numpy array of the copy process
        """
        assert size == numpy_array.size
        for i in range(0, size):
            numpy_array[i] = getter(intbitvec_array, i)

    return intbitvec_to_numpyarray


def main(args):
    starttime = timestamp(msg="ConKmer call: start calling CNVs")
    assert args.windowsize % 2 == 1, "windowsize needs to be odd"
    assert args.lowergclimit >= 0, "lower GC limit needs to be non-negative"
    assert args.uppergclimit <= 100, "upper GC limit may not exceed 100 [%]"
    assert args.lowergclimit < args.uppergclimit, "lower GC limit needs to be smaller than upper GC limit"
    assert exists(args.bitvectors), f"It seems that the bit vectors ZARR folder"\
                                    f" {args.bitvectors} does not exist."
    assert exists(args.countvecs), f"It seems that the count vectors ZARR folder"\
                                   f" {args.countvecs} does not exist."
    assert exists(args.gcvecs), f"It seems that the GC content vectors ZARR folder"\
                                f" {args.gcvecs} does not exist."

    # Load bitvectors from index step and countvectors from count step
    bitvectors = load_from_zgroup(args.bitvectors, 'onebit')
    info_cv = load_from_zgroup(args.countvecs, 'info')
    names_to_sizes_cv = info_cv['names_to_sizes']
    counterbits = int(info_cv['counterbits'])
    coverage = int(info_cv['coverage'])
    shp = info_cv['shp']
    mask = create_mask(shp)
    gapped = mask.w > mask.k
    countvectors = load_from_zgroup(args.countvecs, 'counts')

    # load GC content vectors from index step
    info_gc = load_from_zgroup(args.gcvecs, 'info')
    gcvecs = load_from_zgroup(args.gcvecs, 'gc_content')

    if args.do_gc_correction:
        gc_time = timestamp(msg="Start plotting local GC content vs k-mer counts, correction "
                                "for GC bias and writing corrected countvectors to disk.")

        gc_stats, gc_corrected_countvecs = gc_stats_correction(countvectors, info_cv, gcvecs, info_gc,
                                                               args.maxcount_plot, args.gcplot, mask,
                                                               args.lowergclimit, args.uppergclimit)

        # write corrected countvectors to disk
        write_countvecs_to_zarr(gc_corrected_countvecs, info_cv, args.corrected_countvecs)
        timestamp(gc_time, msg="Finished plotting, GC bias correction and writing corrected countvecs.")
        print(f"{gc_stats[0]:,} k-mers and their local GC-content were not plotted "
              f"due to a k-mer counter above {args.maxcount_plot}.\n")

        # create value stats of corrected countvecs:
        cv_stats_time = timestamp(msg=f"Writing count value statistics of GC bias corrected k-mer counts to {args.cvstats}")
        write_valuestats_corrected_countvecs(gc_corrected_countvecs, names_to_sizes_cv,
                                             counterbits, args.cvstats)
        timestamp(cv_stats_time, msg="Finished writing count value statistics")
    else:
        gc_time = timestamp(msg="Start converting intbitvectors to numpy float arrays")
        # converting intbitvectors to numpy float arrays is not so nice, but
        # handling both at the same time would result in much more code overhead
        gc_corrected_countvecs = dict()
        for name, array in countvectors.items():
            size = names_to_sizes_cv[name]
            cc_vector = np.zeros(size, dtype=np.float32)
            count_intbitvec = intbitarray(size, counterbits, init=array)
            intbitvec_to_numpyarray = compile_intbitvec_to_numpyarray(count_intbitvec.get)
            intbitvec_to_numpyarray(count_intbitvec.array, size, cc_vector)
            gc_corrected_countvecs[name] = cc_vector

        timestamp(gc_time, msg="Finished converting intbitvectors to numpy float arrays")

    # Find breakpoints in this k-mer counting vector indicating positions of copy number changes
    ctb_time = timestamp(msg="Begin count vector to breakpoints part")
    counts_to_breakpoints(bitvectors, gc_corrected_countvecs, counterbits,
                          coverage, args, names_to_sizes_cv, gapped)
    timestamp(ctb_time, msg="Seconds needed to finish count vector to breakpoints part")
    timestamp(starttime, msg="total time [sec]")
    timestamp(starttime, msg="total time [min]", minutes=True)
    timestamp(msg="Done.")
