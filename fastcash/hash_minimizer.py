"""
hash_minimizer:
a hash table with subtables and three choices,
page layout is (low) ... [shortcutbits][slot]+ ... (high),
where slot is  (low) ... [signature value] ...     (high),
where signature is (low) [fingerprint choice] .....(high).
signature as bitmask: ccffffffffffffff (choice is at HIGH bits!)

This layout allows fast access because bits are separated.
It is memory-efficient if the number of values is a power of 2,
or just a little less.
"""
import numpy as np
from math import log
from numba import njit, uint64

from rich import print as rprint
from rich.panel import Panel

from fastcash.lowlevel.bitarray import bitarray
from fastcash.subtable_hashfunctions import get_hashfunctions, build_get_sub_subkey_from_key, parse_names
from fastcash.mathutils import bitsfor, xbitsfor
from fastcash.srhash import (
    get_nfingerprints,
    get_npages,
    get_nfingerprints,
    )


def build_hash(k: int, # k-mer size
               m: int, # minimizer size
               n: int, # number of slots over all subtables
       subtables: int, # number of subtables
        pagesize: int,
       hashfuncs: tuple[str, str, str, str],
         nvalues: int,
    update_value,
               *,
        aligned=False, nfingerprints=-1,
        maxwalk=500, shortcutbits=0, prefetch=False):
    """
    Allocate an array and compile access methods for a hash table.
    Return an SRHash object with the hash information.
    """

    # calculate the minimizer universe
    universe  = 4**m
    sub_universe = universe//(4**(int(log(subtables,4))))

    # Basic properties
    hashtype = "minimizer"
    choices = 3
    superkmersize = 2*k-m
    npages = get_npages(n//subtables, pagesize)
    nfingerprints = get_nfingerprints(nfingerprints, sub_universe, npages)

    """
    Calculate the necessary bits for one slot containing:

    - unique: True if the minimizer occurs more than once (1 bit)

    #signature
    - choice bit (2 bits)
    - minimizer: fingerprint of the minimizer

    - incomplete super-k-mer: two bits if the super-k-mer is not complete
      on the left or right side. Unused bits are set to 1 (for the left part
      starting at the most significant bit and for the right part at the least
      significant bit). The first 0 bit indicates the start of the super-k-mer (2 bits)
    - left and right of the super k-mer: store the part on the left and right side
      of the minimizer of one super-k-mer (2*k-m bit)

    - values: one value for each k-mer in a super-k-mer (k-m+1 values for a complete super-k-mer).
      ((k-m+1) * value bits)
    """

    minimizerbits, fminimizerbits = xbitsfor(nfingerprints)
    choicebits = bitsfor(choices)
    unique_bits = 1
    incomplete_bits = 2
    left_side_bits = right_side_bits = 2*(k-m)
    superkmerbits = unique_bits + choicebits + minimizerbits + incomplete_bits + left_side_bits + right_side_bits

    ## value bits
    nvalues_per_superkmer = k-m+1
    vbits = bitsfor(nvalues) # bits to store the value of one k-mer
    valuebits = vbits * nvalues_per_superkmer # bits to store one value for each k-mer in a super-k-mer

    slotbits = superkmerbits + valuebits
    neededbits = slotbits * pagesize + shortcutbits
    pagesizebits = nextpower(neededbits)  if aligned else neededbits
    subtablebits = int(npages * pagesizebits)
    subtablebits = (subtablebits // 512 + 1) * 512
    tablebits = subtablebits * subtables

    fprloss = pagesize * npages * (minimizerbits-fminimizerbits) / 2**23  # in MB
    print(f"# fingerprintbits: {fminimizerbits} -> {minimizerbits}; loss={fprloss:.1f} MB")
    print(f"# npages={npages}, slots={pagesize*npages}, n={n} per subtable")
    print(f"# bits per slot: {slotbits}; per page: {neededbits} -> {pagesizebits}")
    print(f"# subtable bits: {subtablebits};  MB: {subtablebits/2**23:.1f};  GB: {subtablebits/2**33:.3f}")
    print(f"# table bits: {tablebits};  MB: {tablebits/2**23:.1f};  GB: {tablebits/2**33:.3f}")
    print(f"# shortcutbits: {shortcutbits}")

    # allocate the underlying array
    hasharray = bitarray(tablebits, alignment=64)  # (#bits, #bytes)
    print(f"# allocated {hasharray.array.dtype} hash table of shape {hasharray.array.shape}")
    hashtable = hasharray.array  # the raw bit array
    get_bits_at = hasharray.get  # (array, startbit, nbits=1)
    set_bits_at = hasharray.set  # (array, startbit, value , nbits=1)
    hprefetch = hasharray.prefetch

    # get the hash functions
    if hashfuncs == "random":
        firsthashfunc = parse_names(hashfuncs, 1)[0]
    else:
        firsthashfunc, hashfuncs = hashfuncs.split(":", 1)
    get_subtable_subkey_from_key, get_key_from_subtable_subkey = build_get_sub_subkey_from_key(firsthashfunc, universe, subtables)
    hashfuncs, get_pf, get_subkey, get_subtable_page_fpr, get_key_from_subtale_page_fpr = get_hashfunctions(
            firsthashfunc, hashfuncs, choices, universe, npages, subtables)

    rprint(
        Panel(
            f"# [bold yellow]fingerprintbits[/bold yellow]: {fminimizerbits} -> {minimizerbits}; [bold yellow]loss[/bold yellow]={fprloss:.1f} MB\n"
            f"# [bold yellow]npages[/bold yellow]={npages}, [bold yellow]slots[/bold yellow]={pagesize*npages}, [bold yellow]n[/bold yellow]={n} per subtable\n"
            f"# [bold yellow]bits per slot[/bold yellow]: {slotbits}; per page: {neededbits} -> {pagesizebits}\n"
            f"# [bold yellow]subtable bits[/bold yellow]: {subtablebits};  MB: {subtablebits/2**23:.1f};  GB: {subtablebits/2**33:.3f}\n"
            f"# [bold yellow]table bits[/bold yellow]: {tablebits};  MB: {tablebits/2**23:.1f};  GB: {tablebits/2**33:.3f}\n"
            f"# [bold yellow]shortcutbits[/bold yellow]: {shortcutbits}\n"
            f"# [bold yellow]subtable hash function[/bold yellow]: {firsthashfunc}\n"
            f"# [bold yellow]final hash functions[/bold yellow]: {hashfuncs}\n",
            title="Table infos", border_style="red", expand=False)
    )
    get_ps = tuple([ compile_getps_from_getpf(get_pf[c], c+1, minimizerbits)
            for c in range(choices) ])
    get_pf1, get_pf2, get_pf3 = get_pf
    get_ps1, get_ps2, get_ps3 = get_ps
    get_key1, get_key2, get_key3 = get_subkey

    @njit(nogil=True, inline='always', locals=dict(
        page=uint64, startbit=uint64))
    def prefetch_page(table, subtable, page):
        startbit = subtable * subtablebits + page * pagesizebits
        hprefetch(table, startbit)

    # Define private low-level hash table accssor methods
    @njit(nogil=True, locals=dict(
        page=uint64, startbit=uint64, v=uint64))
    def get_shortcutbits_at(table, subtable, page):
        """Return the shortcut bits at the given page."""
        if shortcutbits == 0:
            return uint64(3)
        startbit = subtable * subtablebits + page * pagesizebits
        v = get_bits_at(table, startbit, shortcutbits)
        return v

    @njit(nogil=True, locals=dict(
        page=uint64, slot=uint64, startbit=uint64, u=uint64))
    def get_uniquebits_at(table, subtable, page, slot):
        """Return the unique bit at the given page and slot; 0 for unique, 1 for not unique"""
        startbit = subtable * subtablebits + page * pagesizebits + shortcutbits + slot * slotbits
        u = get_bits_at(table, startbit, unique_bits)
        return u

    @njit(nogil=True, locals=dict(
        page=uint64, slot=uint64, startbit=uint64, c=uint64))
    def get_choicebits_at(table, subtable, page, slot):
        """Return the choice at the given page and slot; choices start with 1."""
        startbit = subtable * subtablebits + page * pagesizebits + shortcutbits + slot * slotbits + unique_bits
        c = get_bits_at(table, startbit, choicebits)
        return c

    @njit(nogil=True, locals=dict(
        subtable=uint64, page=uint64, slot=uint64, kmer=uint64, v=uint64))
    def get_value_at(table, subtable:int, page:int, slot:int, kmer:int):
        """
        Return the value at the given page, slot
        the position of the kmer in the super-k-mer
        """
        if vbits == 0: return 0
        startbit = subtable * subtablebits + page * pagesizebits + slot * slotbits + minimizerbits + vbits * kmer
        v = get_bits_at(table, startbit, vbits)
        return v

    @njit(nogil=True, locals=dict(
            page=uint64, slot=uint64, value=uint64))
    def set_value_at(table, subtable:int, page:int, slot:int, kmer:int, value:int):
        if valuebits == 0: return
        """Set the value at the given page and slot."""
        startbit = subtable * subtablebits + page * pagesizebits + slot * slotbits + minimizerbits + vbits * kmer
        set_bits_at(table, startbit, value, vbits)

    return hashtable, get_value_at, set_value_at



######## Move to another file?
def compile_getps_from_getpf(get_pfx, choice, fprbits):
    @njit(nogil=True, inline='always', locals=dict(
            p=uint64, f=uint64, sig=uint64))
    def get_psx(code):
        (p, f) = get_pfx(code)
        sig = uint64((choice << uint64(fprbits)) | f)
        return (p, sig)
    return get_psx


if __name__ == "__main__":
    h, g, s = build_hash(25, 21, 2_000_000, 7, 6, "random", 2**16, None)
    s(h,3,2,3,1,10)
    print(g(h,3,2,3,1))










