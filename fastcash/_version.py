VERSION = "0.11.0"
__version__ = VERSION

DESCRIPTION = "The fastcash library implements fast (parallel) Cuckoo hashing, together with several applications."
