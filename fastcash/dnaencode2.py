
import numpy as np
import numba
from numba import jit, njit, void, uint8, int64, uint64, prange


# encoding DNA ###############################

def _get_table_dna_to_2bits(default=4):
    """new encoding as used by GATB"""
    b = np.full(256, default, dtype=np.uint8)
    b[ 97]=0  # a
    b[ 65]=0  # A
    b[ 99]=1  # c
    b[ 67]=1  # C
    b[103]=3  # g
    b[ 71]=3  # G
    b[116]=2  # t
    b[ 84]=2  # T
    b[117]=2  # u
    b[ 85]=2  # U
    return b
_TABLE_DNA_TO_2BITS = _get_table_dna_to_2bits()



@njit(nogil=True, locals=dict(i=int64))
def _dna_to_2bits(x, table):
    for i in range(x.size):
        x[i] = table[x[i]]

@njit(nogil=True)
def quick_dna_to_2bits(x):
    for i in range(len(x)):
        x[i] = _TABLE_DNA_TO_2BITS[x[i]]

@njit(parallel=True, nogil=True)
def parallel_dna_to_2bits(x):
    for i in prange(x.size):
        x[i] = _TABLE_DNA_TO_2BITS[x[i]]

## numba compile error
## @njit( ###__signature__ uint8[:],(uint8[:], uint8[:]), 
##     locals=dict(seq=uint8[:], table=uint8[:]))
def dna_to_2bits(seq, table=_TABLE_DNA_TO_2BITS):
    # we expect seq to be a bytearray
    #xx = np.array(seq, dtype=np.uint8)
    if type(seq) == bytes:
        xx = np.frombuffer(bytearray(seq), dtype=np.uint8)
    else:
        xx = np.frombuffer(seq, dtype=np.uint8)
    _dna_to_2bits(xx, table)
    return xx


_TABLE_BITS_TO_DNASTR = ["A","C","T","G"]
def qcode_to_dnastr(qcode, q, table=_TABLE_BITS_TO_DNASTR):
    qc = int(qcode)
    return "".join([ table[((qc >> (2*(q-i-1))) & 3)] for i in range(q)])

@njit(nogil=True, locals=dict(base=uint64))
def write_qcode_to_buffer(qcode, q, buf, start):
    for i in range(q):
        base = qcode >> (2*(q-i-1)) & 3
        buf[start+i] = uint8(base)


def _get_table_2bits_to_dna(default=4):
    b = np.full(256, 35, dtype=np.uint8)  # fill with b'#'
    b[0] = 65  # A
    b[1] = 67  # C
    b[2] = 84  # T
    b[3] = 71  # G
    b[default] = 78  # N
    return b
_TABLE_2BITS_TO_DNA = _get_table_2bits_to_dna()

@njit(nogil=True)
def twobits_to_dna_inplace(buf, start=0, end=0):
    if end <= 0:
        end = len(buf) - end
    for i in range(start, end):
        buf[i] = _TABLE_2BITS_TO_DNA[buf[i]]


########### reverse complements and canonical representation ##############

@njit(nogil=True, locals=dict(
        c1=uint8, c2=uint8, n=int64, zwei=uint8))
def revcomp_inplace(seq):
    n = seq.size
    zwei = uint8(2)
    for i in range((n+1)//2):
        j = n-1-i
        c1 = seq[i]
        c2 = seq[j]
        seq[j] = (c1^zwei)  if c1<4  else c1
        seq[i] = (c2^zwei)  if c2<4  else c2


@njit(nogil=True, locals=dict(
        c=uint8, n=int64, zwei=uint8, rc=uint8[:]))
def revcomp_to_buffer(seq, rc):
    n = seq.size
    zwei = uint8(zwei)
    for i in range(n):
        c = seq[n-1-i]
        rc[i] = (c^zwei) if c < 4  else c


@njit(nogil=True, locals=dict(
    rc=uint8[:]))
def revcomp(seq):
    rc = np.empty_like(seq, dtype=np.uint8)
    revcomp_to_buffer(seq, rc)
    return rc


@njit(nogil=True, locals=dict(
        code=uint64, rc=uint64, c=uint64))
def revcomp_code(code, k):
    rc = 0
    for i in range(k):
        c = (code & uint64(3)) ^ uint64(2)
        rc = (rc << 2) | c
        code >>= 2
    return rc



@njit(nogil=True, locals=dict(
        code=uint64, c=uint64))
def _get_rctable():
    rctable = np.zeros(256, dtype=np.uint64)
    for c in range(256):
        rctable[c] = revcomp_code(c, 4)
    return rctable
_RCTABLE = _get_rctable()


@njit(nogil=True, locals=dict(
        code=uint64, rc=uint64, c=uint64))
def revcomp_code_table(code, k):
    rc = 0
    while k >= 4:
        c = _RCTABLE[code & 255]
        rc = (rc << 8) | c
        code >>= 8
        k -= 4
    for i in range(k):
        c = (code & uint64(3)) ^ uint64(2)
        rc = (rc << 2) | c
        code >>= 2
    return rc

@njit(nogil=True, locals=dict(
        code=int64, rc=int64))
def canonical_code_min(code, q):
    rc = revcomp_code(code, q)
    return code  if code <= rc  else rc


def compile_revcomp_and_canonical_code(k, rcmode):
    """
    return pair of functions
    (_rc = revcomp_code_k, _cc = canonical_code_k)
    specialized for k-mer codes for the given value of k.
    It is expected that LLVM optimization does loop unrolling.
    """
    if k <= 0 or  k > 32:
        raise ValueError(f"k must be between 1 and 32, but is k={k}")

    @njit(nogil=True, locals=dict(
            code=uint64, rc=uint64, c=uint64))
    def _rc(code):
        rc = 0
        t = k // 4
        for i in range(t):
            c = _RCTABLE[code & 255]
            rc = (rc << 8) | c
            code >>= 8
        r = k % 4
        for i in range(r):
            c = (code & uint64(3)) ^ uint64(2)
            rc = (rc << 2) | c
            code >>= 2
        return rc
    
    if rcmode == "min":
        @njit(nogil=True,locals=dict(
                code=uint64, rc=uint64))
        def _cc(code):
            rc = _rc(code)
            return code if code <= rc  else rc
    elif rcmode == "max":
        @njit(nogil=True, locals=dict(
                code=uint64, rc=uint64))
        def _cc(code):
            rc = _rc(code)
            return code if code >= rc  else rc
    elif rcmode == "r":
        @njit(nogil=True, locals=dict(
                code=uint64, rc=uint64))
        def _cc(code):
            rc = _rc(code)
            return rc
    elif rcmode == "pty":
        if k % 2 == 0:
            raise ValueError(f"rcmode 'pty' only works for odd k, but k={k}")
        retmask = uint64((1 << (2*k-1)) - 1)
        @njit(nogil=True, locals=dict(
                code=uint64, rc=uint64, ret=uint64))
        def _cc(code):
            # code and rc have different parity.
            # We select the one with parity 0 as canonical.
            # Since we know the parity, we can save one bit (highest)
            # This is in contrast to the other schemes,
            # where we have 2k bits; here we have 2k-1 (for odd k).
            rc = _rc(code)
            ret =  code if (rc % 2) else rc
            return ret & retmask
    else:  # 'f', 'both', ...
        @njit(nogil=True, locals=dict(
                code=uint64, rc=uint64))
        def _cc(code):
            return code

    return _rc, _cc


# translation of a DNA buffer into codes
def compile_twobit_to_codes(k, rcmode, invalid=uint64(-1)):
    _, ccc = compile_revcomp_and_canonical_code(k, rcmode)

    @njit(locals=dict(code=uint64))
    def twobit_to_codes(seq, out, start=0, n=-1):
        if n == -1:
            n = len(seq) - k + 1 - start
        for i in range(start, start+n):
            code = 0
            for j in range(k):
                c = seq[i+j]
                if c >= 4:
                    out[i-start] = uint64(invalid)
                    break
                code = (code << 2) | c
            else:
                code = ccc(code)
                out[i-start] = code

    @njit(locals=dict(code=uint64))
    def twobit_to_code(seq, start):
        code = 0
        for j in range(k):
            c = seq[start+j]
            if c >= 4:
                return uint64(invalid)
            code = (code << 2) | c
        else:
            code = ccc(code)
            return code

    return twobit_to_codes, twobit_to_code
