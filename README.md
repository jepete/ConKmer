# DESCRIPTION

**ConKmer** is a method and tool to infer **Co**py **n**umbers from [strongly] unique ***k*-mer**s as descibed in the corresponding thesis.
It uses the fastCash library that provides **fast** **C**uckoo h**ash**ing for biological sequence data using code generation and just-in-time compilation.
FastCash is developed by Sven Rahmann and Jens Zentgraf.

The source code of ConKmer is contained in the subfolder `fastcash/conkmer/` and commented using Sphinx markup (e.g. `:param`).


# INSTALLATION

## Install conda

Follow the instructions under the following link if conda is not yet installed: https://conda.io/projects/conda/en/latest/user-guide/install/linux.html


## Clone repository

```bash
git clone https://gitlab.com/jepete/ConKmer.git
```

## Create and activate conda environment

To run ConKmer, a conda environment with the required libraries needs to be created.
A list of required libraries is provided in the `environment.yml` file in the cloned repository;
it can be used to create a new environment:

```bash
cd ConKmer
conda env create
```

which will create an environment named `conkmer` with the required dependencies, using the provided `environment.yml` file in the same directory.

Setting up the environment may take some time, as conda searches and downloads packages.
After all dependencies are resolved, the environment can be activated:

```bash
conda activate conkmer
```

## Install scripts in developer mode

To install ConKmer, the package installer for Python pip is used.
Run the following command inside the current directory with the `setup.py` file, to install ConKmer:

```bash
pip install -e .
```


# USAGE

## Specify path to a working directory

In the Snakefile is a variable `PATH_PREFIX` set in line 64.
There, the path to a working directory with several hundred GB of free space should be specified.
By default, this is `/scratch/conkmer/`.

## Provide short sequencing reads

ConKmer and BWA-MEM2 / Delly both expect gzipped FASTQ files at a specific location.

The config file of ConKmer (e.g. `ConKmer/fastcash/conkmer/configs/GRCh38.yaml`) has an entry `fastq_path`.
ConKmer expects all read sets to be at this path, where all reads from a sample must be in a correspondingly named folder.
The names of these sample folders must be added to the `READS` list in line 31 in the Snakefile, so that for `sample_n` in `READS` all gzipped FASTQ files can be found under the following path: `fastq_path/sample_n/`.
The `sample_n` folder must not contain any files other than the gzipped FASTQ files to be analysed.

The BWA-MEM2 Snakemake wrapper expects 1 (single-end) or 2 (paired-end) FASTQ files.
As used in the rule `bwa_mem2_mem`, it is expected that there are exactly two files (paired-end reads) at `fastq_path/sample_n/`: `1.fq.gz` and `2.fq.gz`.

## Run the Snakemake example workflow

```bash
snakemake --cores 16 --use-conda --configfile fastcash/conkmer/configs/GRCh38.yaml
```
