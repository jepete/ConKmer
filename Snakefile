# Snakefile demonstrating the use of
# * conkmer preindex
# * conkmer index
# * conkmer count
# * conkmer call
# * bwa_mem2
# * delly
# * some scripts (downloading, plotting, bedgraph construction, evaluation...)
# using a human reference genome and paired end reads.
# configfile: "configs/GRCh38.yaml" # -> provide config file via command line,
# e. g. /usr/bin/time -v snakemake --cores 16 --configfile configs/GRCh38.yaml --use-conda

# dictionary mapping k-mer shape identifiers to masks
MASKS = {
    "19": 19*"#",
    "21": 21*"#",
    "23": 23*"#",
    "25": 25*"#",
    "27": 27*"#",
    "29": 29*"#",
    "31": 31*"#",
    "27w39": "##_####_###__#_###_#_###_#__###_####_##",
    "27w35": "#######_####_##__#__##_####_#######",
}

# specify which k-mers to use
KMERSHAPES = ["27w39"]  # ["27w35", "27w39", "19", "21", "23", "25", "27", "29", "31"]
KMERTYPE = ["strongly-unique"]  # ["unique", "strongly-unique"]

# folder name of reads (included into file names)
READS = ["sample_n"]  # ["IHS01", "IHS02", "IHS03", "IHS04", "IHS05", "IHS06", "IHS07", "IHS08", "IHS09", "IHS10"]  # ["HG001-subdir005-008"]
# don't forget to change the fastq_path in the config file, if necessary

# window size z to compute the median in the call step (see thesis)
WINDOWSIZE = [config["window_size"]]  # [5, 25, 49, 99, 249, 499, 999, 1999, 3999, 9999]

SECOND_WINDOWSIZE = config["second_window_size"]  # 3999

# factor for gap insertion (see parameter alpha in the thesis)
FACTOR_WS = [config["factor_ws"]]  # [3, 5, 8, 20, 50, 9999999999]

# flags
DO_GC_CORRECTION = True
DO_PREINDEX_INTERSECTION = False

# strings to incorporate into file names depending on the flags
if DO_GC_CORRECTION:
    GC_CORRECTION_FLAG = "--do_gc_correction"
    FILESTRING_GC = "with-GC-correction"
else:
    GC_CORRECTION_FLAG = ""
    FILESTRING_GC = "no-GC-correction"

if DO_PREINDEX_INTERSECTION:
    PREINDEX_INTERSECTION = "-preindex-intersection"
else:
    PREINDEX_INTERSECTION = "out-preindex-intersection"

# Define reference sequence GRCh38: All human DNA from Ensembl
RELEASE = 108
ENSEMBL_HUMAN = f"ftp://ftp.ensembl.org/pub/release-{RELEASE}/fasta/homo_sapiens/dna/"

# path to the folder containing subfolders "ref", "results", "logs", "benchmarks" etc.
PATH_PREFIX = "/scratch/conkmer/"

# number of threads to use for benchmarking
# THREADS_TO_USE = [16]  # [1, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32]

# DO_WINDOW_INTERSECTION = False


# Define the final desired output files as input to rule "all"
rule all:
    input:
        [f"{PATH_PREFIX}results/{config['REFERENCE_NAME']}_constant-copy-number-intervals_{ktype}_with{PREINDEX_INTERSECTION}_reads{reads}_{wsize}_{fws}_{FILESTRING_GC}_k{k}.tsv"\
        for ktype in KMERTYPE for reads in READS for wsize in WINDOWSIZE for fws in FACTOR_WS for k in KMERSHAPES],  # final ConKmer output
        # scripts output for a single sample and one fixed parameter assignment in the order of the corresponding rules:
        [f"{PATH_PREFIX}results/{config['REFERENCE_NAME']}_percAcc-ideogram_{ktype}_with{PREINDEX_INTERSECTION}_k{k}.pdf"\
        for ktype in KMERTYPE for k in KMERSHAPES],  # in total there are three ideogram plots
        [f"{PATH_PREFIX}results/{config['REFERENCE_NAME']}_gaps_{ktype}_with{PREINDEX_INTERSECTION}_k{k}.pdf"\
        for ktype in KMERTYPE for k in KMERSHAPES],
        [f"{PATH_PREFIX}results/{config['REFERENCE_NAME']}_barplot_countvaluestats_{ktype}_with{PREINDEX_INTERSECTION}_reads{reads}_k{k}.pdf"\
        for ktype in KMERTYPE for reads in READS for k in KMERSHAPES],  # in addition, a scatter plot is output
        [f"{PATH_PREFIX}results/{config['REFERENCE_NAME']}_plot_raw_and gc_corrected_count_valuestats_{ktype}_with{PREINDEX_INTERSECTION}_reads{reads}_{wsize}_{fws}_{FILESTRING_GC}_k{k}.pdf"\
        for ktype in KMERTYPE for reads in READS for wsize in WINDOWSIZE for fws in FACTOR_WS for k in KMERSHAPES],
        # final Delly output
        #[f"{PATH_PREFIX}results/delly/CNVs_{reads}.tsv" for reads in READS],

        #[f"{PATH_PREFIX}results/stats/{config['REFERENCE_NAME']}_stats_{reads}_comp-conkmer-delly_{ktype}_with{PREINDEX_INTERSECTION}_{wsize}_{fws}_{FILESTRING_GC}_k{k}.txt"\
        #for reads in READS for ktype in KMERTYPE for wsize in WINDOWSIZE for fws in FACTOR_WS for k in KMERSHAPES],

        # comment in when using several samples:
        #[f"{PATH_PREFIX}results/{config['REFERENCE_NAME']}_stats_bp-per-cn_{ktype}_with{PREINDEX_INTERSECTION}_{wsize}_{fws}_{FILESTRING_GC}_k{k}.svg"\
        #for ktype in KMERTYPE for wsize in WINDOWSIZE for fws in FACTOR_WS for k in KMERSHAPES],  # there are three plots in total

        # comment in when using exact ten samples, e.g. IHS01 to IHS10 (see thesis):
        #[f"{PATH_PREFIX}results/{config['REFERENCE_NAME']}_ConKmer-Delly-intersection-of-10-sample-intersections_{ktype}_with{PREINDEX_INTERSECTION}_{wsize}_{fws}_{FILESTRING_GC}_k{k}.tsv"\
        #for ktype in KMERTYPE for wsize in WINDOWSIZE for fws in FACTOR_WS for k in KMERSHAPES],

        # comment in when using different combinations of the ConKmer call parameters z and alpha (see thesis):
        #[f"{PATH_PREFIX}results/{config['REFERENCE_NAME']}_stats_{reads}_comp-conkmer-delly-CNu2-bp_{ktype}_{FILESTRING_GC}_k{k}.pdf"\
        #for ktype in KMERTYPE for reads in READS for wsize in WINDOWSIZE for fws in FACTOR_WS for k in KMERSHAPES],  # there are eight plots in total



# ConKmer workflow ##############################


# A rule that demonstrates how to build a preindex with conkmer
# Note that you need to give an estimate of the number of k-mers in all input files.
# For human DNA and k=27, this is approx. 2.37 billion,
# so this number is given as size parameter via config file.
rule preindex:
    input:
        f"{PATH_PREFIX}ref/{config['REFGENOME']}"
    output:
        directory(PATH_PREFIX + "ref/{refname}_preindex_{ktype}_k{k}.zarr")
    log:
        PATH_PREFIX + "logs/{refname}_preindex_{ktype}_k{k}.log"
    benchmark:
        repeat(PATH_PREFIX + "benchmarks/{refname}_preindex_{ktype}_k{k}.txt", 1)
    params:
        size = config["size"],
        fill = config["allowed_fillrate"],
        subtables =  config["subtables"],  # for benchmarking: lambda wc: int(wc.threads)-1
        mask = lambda wc: MASKS[wc.k],
    threads: config["subtables"] + 2  # here only for calculating weak k-mers, otherwise controlled via subtables (+2 due to producer, python interpreter and zcat)
    shell:
        "(/usr/bin/time -v conkmer preindex --preindex {output} "
        " --genome <(zcat {input}) -n {params.size} "
        " --mask '{params.mask}' --fill {params.fill} "
        " --subtables {params.subtables} -T {threads}) &> {log}"  # for benchmarking: threads -> wildcards.threads


# A rule that demonstrates 'conkmer index'
rule index:
   input:
       preindex = PATH_PREFIX + "ref/{refname}_preindex_{ktype}_k{k}.zarr", # ktype does not matter here but it is necessary for snakemake
       genome   = f"{PATH_PREFIX}ref/{config['REFGENOME']}",
   output:
       index      = directory(PATH_PREFIX + "results/{refname}_index_{ktype}_with{intersect}_k{k}.zarr"),
       gapstats   = PATH_PREFIX + "results/{refname}_gaps_{ktype}_with{intersect}_k{k}.tsv",
       bitvectors = directory(PATH_PREFIX + "results/{refname}_bits_{ktype}_with{intersect}_k{k}.zarr"),
       gcvecs     = directory(PATH_PREFIX + "results/{refname}_local-GC-content_{ktype}_with{intersect}_k{k}.zarr"),
   log:
       PATH_PREFIX + "logs/{refname}_index_{ktype}_with{intersect}_k{k}.log"
   benchmark:
       repeat(PATH_PREFIX + "benchmarks/{refname}_index_{ktype}_with{intersect}_k{k}.txt", 1)
   params:
       weakunique = lambda wc: "--weakunique" if not "strongly" in wc.ktype else "",
       fill = config["allowed_fillrate"],
       gaplen = config["gaplen"],  # minimal gap length to report
       second_preindex = f"--preindexTwo {config['preindexTwo']}" if DO_PREINDEX_INTERSECTION else "",  # file existence has to be ensured manually
       gc_window_size = config["gc_window"],
   threads: config["subtables"] + 2  # controlled via subtables (+ 2 due to producer, python interpreter and zcat)
   shell:
       "(/usr/bin/time -v conkmer index --preindex {input.preindex} {params.second_preindex}"
       " --fill {params.fill} --gcvectors {output.gcvecs} {params.weakunique}"
       " --genome <(zcat {input.genome}) --index {output.index} --gcwindow {params.gc_window_size}"
       " --bitvectors {output.bitvectors} --gapstatistics {output.gapstats} "
       " --debuglonggaps {params.gaplen} --shortcutbits 2 ) &> {log}"
       # --shortcutbits 2 decreased the wall clock time of the count step from 60 to 58 minutes for 27w39


# A rule that demonstrates 'conkmer count'
rule count:
  input:
      index = PATH_PREFIX + "results/{refname}_index_{ktype}_with{intersect}_k{k}.zarr",
      fastqs_path = config['fastq_path'],
      genome = f"{PATH_PREFIX}ref/{config['REFGENOME']}",
  output:
      countindex   = directory(PATH_PREFIX + "results/{refname}_countindex_{ktype}_with{intersect}_reads{reads}_k{k}.zarr"),
      valuestats   = PATH_PREFIX + "results/{refname}_count_valuestats_{ktype}_with{intersect}_reads{reads}_k{k}.txt",
      countvectors = directory(PATH_PREFIX + "results/{refname}_raw_countvecs_{ktype}_with{intersect}_reads{reads}_k{k}.zarr"),
  log:
      PATH_PREFIX + "logs/{refname}_count_{ktype}_with{intersect}_reads{reads}_k{k}.log"
  benchmark:
      repeat(PATH_PREFIX + "benchmarks/{refname}_count_{ktype}_with{intersect}_reads{reads}_k{k}.txt", 1)
  params:
      zcat_options = config["zcat_options"]
  threads: config["subtables"] + 2 # controlled via subtables (+ 2 due to producer, python interpreter and zcat)
  shell:
      "(/usr/bin/time -v conkmer count --index {input.index} --genome <(zcat {input.genome})"
      " --countvecs {output.countvectors} --countindex {output.countindex} --valuestats {output.valuestats}"
      " --fastq <(zcat {params.zcat_options} {input.fastqs_path}/{wildcards.reads}/)) &> {log}"


# A rule that demonstrates 'conkmer call'
rule call:
   input:
       bitvectors = PATH_PREFIX + "results/{refname}_bits_{ktype}_with{intersect}_k{k}.zarr",
       countvectors = f"{PATH_PREFIX}" + "results/{refname}_raw_countvecs_{ktype}_with{intersect}_reads{reads}_k{k}.zarr",
       gcvecs = PATH_PREFIX + "results/{refname}_local-GC-content_{ktype}_with{intersect}_k{k}.zarr",
   output:
       tsvfile  = PATH_PREFIX + "results/{refname}_constant-copy-number-intervals_{ktype}_with{intersect}_reads{reads}_{wsize}_{fws}_{gc}_k{k}.tsv",
       ccnipath = directory(PATH_PREFIX + "results/{refname}_constant-copy-number-intervals_{ktype}_with{intersect}_reads{reads}_{wsize}_{fws}_{gc}_k{k}/"),
       gcplot   = PATH_PREFIX + "results/{refname}_gc-content_vs_kmer-counter_{ktype}_with{intersect}_reads{reads}_{wsize}_{fws}_{gc}_k{k}.pdf",
       # for GC bias corrected count value statistics, see parameter cvstats
   log:
       PATH_PREFIX + "logs/{refname}_call_{ktype}_with{intersect}_reads{reads}_{wsize}_{fws}_{gc}_k{k}.log"
   benchmark:
       repeat(PATH_PREFIX + "benchmarks/{refname}_call_{ktype}_with{intersect}_reads{reads}_{wsize}_{fws}_{gc}_k{k}.txt", 1)
   params:
       maxcount       = config["maxcount"],
       maxcount_plot  = config["maxcount_plot"],
       lower_gc_limit = config["lower_gc_limit"],
       upper_gc_limit = config["upper_gc_limit"],
       cvstats = lambda wc:f"--cvstats {PATH_PREFIX}results/{wc.refname}_countvaluestats-gc-corrected_{wc.ktype}_with{wc.intersect}_"\
                         f"reads{wc.reads}_{wc.wsize}_{wc.fws}_{wc.gc}_k{wc.k}.txt" if DO_GC_CORRECTION else "",
       ccv = lambda wc:f"--corrected_countvecs {PATH_PREFIX}results/{wc.refname}_corrected_countvecs_{wc.ktype}_"\
                     f"reads{wc.reads}_{wc.wsize}_{wc.fws}_{wc.gc}_k{wc.k}.zarr" if DO_GC_CORRECTION else "",
       # ccv: corrected count vectors
   threads: config["subtables"] + 2  # to fit to the other ConKmer rules
   shell:
       "(/usr/bin/time -v conkmer call --maxcount {params.maxcount} --maxcount_plot {params.maxcount_plot}"
       " --windowsize {wildcards.wsize} --gcplot {output.gcplot} --ccnipath {output.ccnipath}"
       " --ccnifile {output.tsvfile} --bitvectors {input.bitvectors} {params.cvstats}"
       " {GC_CORRECTION_FLAG} --factorws {wildcards.fws} --threads {threads} {params.ccv}"  # for benchmarking: threads -> wildcards.threads
       " --countvecs {input.countvectors} --gcvecs {input.gcvecs} --lowergclimit {params.lower_gc_limit}"
       " --uppergclimit {params.upper_gc_limit}) &> {log}"



# SCRIPTS ##############################
# scripts folder has to be symlinked: ln -s /path/to/scripts PATH_PREFIX/scripts


# Plotting densities of [strongly] unique k-mer starting positions along
# each chromosome of ensembl GRCh38 release 106
rule plot_ideogram:
    input:
        bitvectors = PATH_PREFIX + "results/{refname}_bits_{ktype}_with{intersect}_k{k}.zarr",
    output:
        ideogram_gaps = PATH_PREFIX + "results/{refname}_gaps-ideogram_{ktype}_with{intersect}_k{k}.pdf",
        ideogram_perc = PATH_PREFIX + "results/{refname}_perc-ideogram_{ktype}_with{intersect}_k{k}.pdf",
        ideogram_perc_acc = PATH_PREFIX + "results/{refname}_percAcc-ideogram_{ktype}_with{intersect}_k{k}.pdf",
    params:
        weak = lambda wc: not "strongly" in wc.ktype,
        stepsize = config["stepsize_ideograms"],
    script:
        "fastcash/conkmer/scripts/plot_ideograms.py"


# Plotting frequencies of gaps (no [strongly] unique k-mers)
rule plot_gapstats:
    input:
        gapstats = PATH_PREFIX + "results/{refname}_gaps_{ktype}_with{intersect}_k{k}.tsv",
    output:
        gapplot  = PATH_PREFIX + "results/{refname}_gaps_{ktype}_with{intersect}_k{k}.pdf",
    script:
        "fastcash/conkmer/scripts/plot_gapstats.py"


# Plotting the percentage of [strongly] unique k-mers in all k-mers for a given reference and different k
rule plot_kmer_cov:
    input:
        logfiles = expand(PATH_PREFIX + "logs/{refname}_index_{ktype}_withPreindexIntersection_k{k}.log",\
                        refname=config["REFERENCE_NAME"], ktype=["strongly-unique", "unique"], k=KMERSHAPES)
    output:
        PATH_PREFIX + "results/{refname}_kmer-coverage-plot_{ktype}.pdf"
    script:
        "fastcash/conkmer/scripts/plot_k-mer_cov.py"


# Plotting the value statistics in the count index after conkmer count:
# How often every count occurred (e. g. "there are n [strongly] unique
# k-mers in the reference that are counted m times in the used FASTQ file(s)")
rule plot_count_valuestats:
    input:
        valuestats  = PATH_PREFIX + "results/{refname}_count_valuestats_{ktype}_with{intersect}_reads{reads}_k{k}.txt"
    output:
        scatterplot = PATH_PREFIX + "results/{refname}_scatterplot_countvaluestats_{ktype}_with{intersect}_reads{reads}_k{k}.pdf",
        barplot     = PATH_PREFIX + "results/{refname}_barplot_countvaluestats_{ktype}_with{intersect}_reads{reads}_k{k}.pdf",
    params:
        reads = "{wildcards.reads}",
        counts_to_plot = config["maxcount_plot"],
        refname = config["REFERENCE_NAME"],
        weak = lambda wc: not "strongly" in wc.ktype,
    script:
        "fastcash/conkmer/scripts/plot_count_valuestats.py"


# Plotting the k-mer count value statistics after GC bias correction in conkmer call:
rule plot_count_valuestats_gc_corrected:
    input:
        raw_valuestats = PATH_PREFIX + "results/{refname}_count_valuestats_{ktype}_with{intersect}_reads{reads}_k{k}.txt",
        # gc_corrected_valuestats = PATH_PREFIX + "results/{refname}_countvaluestats-gc-corrected_{ktype}_with{intersect}_reads{reads}_{wsize}_{fws}_{gc}_k{k}.txt",
        tsvfile = PATH_PREFIX + "results/{refname}_constant-copy-number-intervals_{ktype}_with{intersect}_reads{reads}_{wsize}_{fws}_{gc}_k{k}.tsv",
        # tsvfile not needed but specified as input to ensure that countvecs_to_bedGraph is executed after the rule call
    output:
        combinedplot = PATH_PREFIX + "results/{refname}_plot_raw_and gc_corrected_count_valuestats_{ktype}_with{intersect}_reads{reads}_{wsize}_{fws}_{gc}_k{k}.pdf",
    params:
        reads = "{wildcards.reads}",
        counts_to_plot = config["maxcount_plot"],
        refname = config["REFERENCE_NAME"],
        weak = lambda wc: not "strongly" in wc.ktype,
        gc_corrected_valuestats = PATH_PREFIX + "results/{refname}_countvaluestats-gc-corrected_{ktype}_with{intersect}_reads{reads}_{wsize}_{fws}_{gc}_k{k}.txt",
    script:
        "fastcash/conkmer/scripts/plot_count_valuestats_gc_corrected.py"


# Intersects the intervals of constant copy number
# that were created with each of two different window sizes
# (does currently not integrate well into workflow):
rule intersect_second_windowsize:
    input:
        cn_intervals = PATH_PREFIX + "results/{refname}_constant-copy-number-intervals_{ktype}_with{intersect}_reads{reads}_{wsize}_{fws}_{gc}_k{k}.tsv",
        second_cn_intervals = PATH_PREFIX + "results/{refname}_constant-copy-number-intervals_{ktype}_with{intersect}_reads{reads}_" + \
                                str(config["second_window_size"]) + "_{gc}_k{k}.tsv",
    output:
        cn_intervals_intersection = PATH_PREFIX + "results/{refname}_constant-copy-number-intervals-intersection_{ktype}_with{intersect}_reads{reads}_{wsize}_{fws}_with"\
                                    + str(config["second_window_size"]) + "_{gc}_k{k}.tsv",
    params:
        do_intersection = False,  # DO_WINDOW_INTERSECTION,
        windowsize = config["window_size"],
        second_windowsize = config["second_window_size"],
    script:
        "fastcash/conkmer/scripts/intersect_second_windowsize.py"


# splitted into two rules (delly/conkmer) so that the filtering for delly is not redone everytime some conkmer parameters change
rule filter_delly_for_cn_unequal2:
    input:
        PATH_PREFIX + "results/delly/CNVs_{reads}.tsv",
    output:
        PATH_PREFIX + "results/delly/CNVs-CN-unequal2_{reads}.tsv",
    shell:
        "awk 'BEGIN{{FS=OFS=\"\\t\"}} {{if($5 != 2) {{print}}}}' {input} > {output}"


# filter for those ConKmer intervals with CN != 2
rule filter_conkmer_for_cn_unequal2:
    input:
        conkmer_tsv = PATH_PREFIX + "results/{refname}_constant-copy-number-intervals_{ktype}_with{intersect}_reads{reads}_{wsize}_{fws}_{gc}_k{k}.tsv",
    output:
        PATH_PREFIX + "results/{refname}_ConKmer-CN-unequal2-intervals_{ktype}_with{intersect}_reads{reads}_{wsize}_{fws}_{gc}_k{k}.tsv",
    shell:
        "awk 'BEGIN{{FS=OFS=\"\\t\"}} {{if($5 != 2) {{print}}}}' {input} > {output}"


# Intersect (10) Delly results (CN != 2) pairwise down to one file containing only those intersections for which the CN != 2 for all samples
rule intersect_ten_delly_results:
    input:
        files = expand(PATH_PREFIX + "results/delly/CNVs-CN-unequal2_{reads}.tsv", reads=READS),
    output:
        intersection = PATH_PREFIX + "results/delly/CNVs-same-CN-unequal2_intersection.tsv",
    params:
        temp_path = PATH_PREFIX + "results/delly/temp_intersections/"
    script:
        "fastcash/conkmer/scripts/intersect_ten_results.py"


# Intersect (10) ConKmer results (CN != 2) pairwise down to one file containing only those intersections for which the CN != 2 for all samples
rule intersect_ten_conkmer_results:
    input:
        files = expand(PATH_PREFIX + "results/{refname}_ConKmer-CN-unequal2-intervals_{ktype}_with{intersect}_reads{reads}_{wsize}_{fws}_{gc}_k{k}.tsv",
            refname=config["REFERENCE_NAME"], ktype=KMERTYPE, intersect=PREINDEX_INTERSECTION, reads=READS, wsize=WINDOWSIZE, fws=FACTOR_WS,
            gc=FILESTRING_GC, k=KMERSHAPES)  # adopt when using different values for one parameter so that it expands to exactly ten file names
    output:
        intersection = PATH_PREFIX + "results/{refname}_ConKmer-same-CN-unequal2-intersection_{ktype}_with{intersect}_{wsize}_{fws}_{gc}_k{k}.tsv",
    params:
        temp_path = lambda wc: f"{PATH_PREFIX}results/conkmer-temp-intersections_{wc.refname}_{wc.ktype}_intersection_{wc.wsize}_{wc.fws}_{wc.gc}_k{wc.k}/"
    script:
        "fastcash/conkmer/scripts/intersect_ten_results.py"


# intersect the intersections of Delly and ConKmer
rule intersect_intersections:
    input:
        delly_intersection   = PATH_PREFIX + "results/delly/CNVs-same-CN-unequal2_intersection.tsv",
        conkmer_intersection = PATH_PREFIX + "results/{refname}_ConKmer-same-CN-unequal2-intersection_{ktype}_with{intersect}_{wsize}_{fws}_{gc}_k{k}.tsv",
    output:
        PATH_PREFIX + "results/{refname}_ConKmer-Delly-intersection-of-10-sample-intersections_{ktype}_with{intersect}_{wsize}_{fws}_{gc}_k{k}.tsv",
    shell:
        "bedtools intersect -wo -a {input.delly_intersection} -b {input.conkmer_intersection} > {output}"


# Delly CNVs supported by ConKmer
rule overlaps_with_delly:
    input:
        conkmer_tsv = PATH_PREFIX + "results/{refname}_constant-copy-number-intervals_{ktype}_with{intersect}_reads{reads}_{wsize}_{fws}_{gc}_k{k}.tsv",
        delly_tsv   = PATH_PREFIX + "results/delly/CNVs_{reads}.tsv",
    output:
        overlaps = PATH_PREFIX + "results/{refname}_delly-overlaps-wo_{ktype}_with{intersect}_reads{reads}_{wsize}_{fws}_{gc}_k{k}.tsv",
    shell:
        "bedtools intersect -wo -a {input.delly_tsv} -b {input.conkmer_tsv} > {output.overlaps}"


# ConKmer CNVs supported by Delly
rule overlaps_with_conkmer:
    input:
        conkmer_tsv = PATH_PREFIX + "results/{refname}_constant-copy-number-intervals_{ktype}_with{intersect}_reads{reads}_{wsize}_{fws}_{gc}_k{k}.tsv",
        delly_tsv   = PATH_PREFIX + "results/delly/CNVs_{reads}.tsv",
    output:
        overlaps = PATH_PREFIX + "results/{refname}_conkmer-overlaps-wo_{ktype}_with{intersect}_reads{reads}_{wsize}_{fws}_{gc}_k{k}.tsv",
    shell:
        "bedtools intersect -wo -a {input.conkmer_tsv} -b {input.delly_tsv} > {output.overlaps}"


# compare Delly results to ConKmer results and vice versa and writing a comprehensive statistics file
rule compare_to_delly:
    input:
        delly_conkmer_intersection = PATH_PREFIX + "results/{refname}_delly-overlaps-wo_{ktype}_with{intersect}_reads{reads}_{wsize}_{fws}_{gc}_k{k}.tsv",
        conkmer_delly_intersection = PATH_PREFIX + "results/{refname}_conkmer-overlaps-wo_{ktype}_with{intersect}_reads{reads}_{wsize}_{fws}_{gc}_k{k}.tsv",
        delly_all   = PATH_PREFIX + "results/delly/CNVs_{reads}.tsv",
        conkmer_all = PATH_PREFIX + "results/{refname}_constant-copy-number-intervals_{ktype}_with{intersect}_reads{reads}_{wsize}_{fws}_{gc}_k{k}.tsv",
    output:
        stats = PATH_PREFIX + "results/stats/{refname}_stats_{reads}_comp-conkmer-delly_{ktype}_with{intersect}_{wsize}_{fws}_{gc}_k{k}.txt",
    params:
        max_cn = 6,  # max. CN to include into statistics, all above is summarized as >= max_cn + 1
    shell:
        "python3 fastcash/conkmer/scripts/compare_to_delly.py "
        "{input.delly_conkmer_intersection} {input.delly_all} {input.conkmer_all} {input.conkmer_delly_intersection} {output.stats} {params.max_cn}"


# to plot for several samples and one parameter combination of z and alpha (see thesis)
rule plot_stats_for_several_samples:
    input:
        statfiles = expand(PATH_PREFIX + "results/stats/{refname}_stats_{reads}_comp-conkmer-delly_{ktype}_with{intersect}_{wsize}_{fws}_{gc}_k{k}.txt",
                            refname=config["REFERENCE_NAME"], ktype=KMERTYPE, reads=READS, intersect=PREINDEX_INTERSECTION, wsize=WINDOWSIZE, fws=FACTOR_WS, 
                            gc=FILESTRING_GC, k=KMERSHAPES),
    output:
        bp_per_cn      = PATH_PREFIX + "results/{refname}_stats_bp-per-cn_{ktype}_with{intersect}_{wsize}_{fws}_{gc}_k{k}.svg",
        bp_cnu2        = PATH_PREFIX + "results/{refname}_stats_bp-cnu2_{ktype}_with{intersect}_{wsize}_{fws}_{gc}_k{k}.pdf",
        intervals_cnu2 = PATH_PREFIX + "results/{refname}_stats_intervals-cnu2_{ktype}_with{intersect}_{wsize}_{fws}_{gc}_k{k}.pdf",
    params:
        samples = READS,
        max_cn  = 6,  # max. CN to include into statistics, all above is summarized as >= max_cn + 1
    script:
        "fastcash/conkmer/scripts/plot_several_samples.py"


# to plot for one sample and different parameter combinations of z and alpha (see thesis)
rule plot_delly_conkmer_comparison:
    input:
        statfiles = expand(PATH_PREFIX + "results/stats/{refname}_stats_{reads}_comp-conkmer-delly_{ktype}_with{intersect}_{wsize}_{fws}_{gc}_k{k}.txt",
                            refname=config["REFERENCE_NAME"], ktype=KMERTYPE, reads=READS, intersect=PREINDEX_INTERSECTION, wsize=WINDOWSIZE, fws=FACTOR_WS, 
                            gc=FILESTRING_GC, k=KMERSHAPES),
    output:
        conkmer_bp                        = PATH_PREFIX + "results/{refname}_stats_{reads}_conkmer-bp_{ktype}_{gc}_k{k}.pdf",
        conkmer_bp_CNu2                   = PATH_PREFIX + "results/{refname}_stats_{reads}_conkmer-bp-CNu2_{ktype}_{gc}_k{k}.pdf",
        conkmer_intervals                 = PATH_PREFIX + "results/{refname}_stats_{reads}_conkmer-intervals_{ktype}_{gc}_k{k}.pdf",
        comp_conkmer_delly_allCN_bp       = PATH_PREFIX + "results/{refname}_stats_{reads}_comp-conkmer-delly-allCN-bp_{ktype}_{gc}_k{k}.pdf",
        comp_conkmer_cn_too_low_bp        = PATH_PREFIX + "results/{refname}_stats_{reads}_comp-conkmer-CN-too-low-bp_{ktype}_{gc}_k{k}.pdf",
        comp_conkmer_delly_CNu2_intervals = PATH_PREFIX + "results/{refname}_stats_{reads}_comp-conkmer-delly-CNu2-intervals_{ktype}_{gc}_k{k}.pdf",
        comp_conkmer_delly_CNu2_bp        = PATH_PREFIX + "results/{refname}_stats_{reads}_comp-conkmer-delly-CNu2-bp_{ktype}_{gc}_k{k}.pdf",
        comp_delly_conkmer_CNu2_bp        = PATH_PREFIX + "results/{refname}_stats_{reads}_comp-delly-conkmer-CNu2-bp_{ktype}_{gc}_k{k}.pdf",
    params:
        windowsizes = WINDOWSIZE,
        alphas = FACTOR_WS,
    script:
        "fastcash/conkmer/scripts/plot_delly_comparison.py"


# Write the non-zero counts from raw count arrays to a bedGraph file
# that can be converted to BigWig format (using bedGraphToBigWig)
# to display as a custom track in IGV
rule countvecs_to_bedgraph:
    input:
        tsvfile = PATH_PREFIX + "results/{refname}_constant-copy-number-intervals_{ktype}_with{intersect}_reads{reads}_{wsize}_{fws}_{gc}_k{k}.tsv",
        # tsvfile not needed but specified as input to ensure that countvecs_to_bedGraph is executed after the rule call
    output:
        folder  = directory(PATH_PREFIX + "results/{refname}_bedGraphs_gc_corrected_counts_{ktype}_with{intersect}_reads{reads}_{wsize}_{fws}_{gc}_k{k}")
    params:
        refname = config["REFERENCE_NAME"],
        countvectors = lambda wc:f"{PATH_PREFIX}results/{wc.refname}_corrected_countvecs_{wc.ktype}_reads{wc.reads}_"\
                               f"{wc.wsize}_{wc.fws}_{wc.gc}_k{wc.k}.zarr" if DO_GC_CORRECTION else \
                               f"{PATH_PREFIX}results/{wc.refname}_raw_countvecs_{wc.ktype}_reads{wc.reads}_k{wc.k}.zarr",
    script:
        "fastcash/conkmer/scripts/countvecs_to_bedGraph.py"


# Write the non-zero counts from bitvectors to a bedGraph file
# that can be converted to BigWig format (using bedGraphToBigWig)
# to display as a custom track in IGV
rule bitvectors_to_bedgraph:
    input:
        bitvectors = PATH_PREFIX + "results/{refname}_bits_{ktype}_with{intersect}_k{k}.zarr",
    output:
        folder  = directory(PATH_PREFIX + "results/{refname}_bedGraphs_bitvectors_{ktype}_with{intersect}_k{k}")
    params:
        refname = config["REFERENCE_NAME"],
    script:
        "fastcash/conkmer/scripts/bitvectors_to_bedgraph.py"


# This rule simply downloads the sequence of the reference genome from Ensembl using wget.
# This is not very elegant in terms of error recovery but should work.
rule download_ref:
    output:
        f"{PATH_PREFIX}ref/{config['REFGENOME']}"
    shell:
        f"wget {ENSEMBL_HUMAN}/{config['REFGENOME']} -P {PATH_PREFIX}ref/gzipped/ && "
        f"bgzip -c <(zcat {PATH_PREFIX}ref/gzipped/{config['REFGENOME']}) > {PATH_PREFIX}ref/{config['REFGENOME']}"
        # Delly needs reference genome bgzipped


# download T2T-CHM13 using wget
rule download_t2t:
    output:
        f"{PATH_PREFIX}t2t/chm13v2.0.fa.gz"
    shell:
        f"wget https://s3-us-west-2.amazonaws.com/human-pangenomics/T2T/CHM13/assemblies/analysis_set/chm13v2.0.fa.gz -P {PATH_PREFIX}t2t/"



# Delly workflow ##############################


# index reference genome
rule bwa_mem2_index:  # requires more than 64 GB RAM
    input:
        f"{PATH_PREFIX}ref/{config['REFGENOME']}",
    output:
        f"{PATH_PREFIX}ref/{config['REFGENOME']}.amb",
        f"{PATH_PREFIX}ref/{config['REFGENOME']}.ann",
        f"{PATH_PREFIX}ref/{config['REFGENOME']}.bwt.2bit.64",
        f"{PATH_PREFIX}ref/{config['REFGENOME']}.pac",
    log:
        f"{PATH_PREFIX}logs/{config['REFERENCE_NAME']}.log",
    benchmark:
       repeat(f"{PATH_PREFIX}benchmarks/{config['REFERENCE_NAME']}_bwa_mem2_index.txt", 1)
    wrapper:
        "v1.21.0/bio/bwa-mem2/index"


# align reads to indexed reference genome to run Delly on these alignments
rule bwa_mem2_mem:
    input:
       reads = [config['fastq_path'] + "{reads}/1.fq.gz", config['fastq_path'] + "{reads}/2.fq.gz"],
       idx = multiext(f"{PATH_PREFIX}ref/{config['REFGENOME']}", ".amb", ".ann", ".bwt.2bit.64", ".pac"),
    output:
       PATH_PREFIX + "fastqs/mapped/{reads}.bam",
    log:
       PATH_PREFIX + "logs/bwa_mem2_mem_{reads}.log",
    benchmark:
       repeat(PATH_PREFIX + "benchmarks/" + f"{config['REFERENCE_NAME']}" + "_bwa_mem2_mem_{reads}.txt", 1)
    params:
       extra      = r"-R '@RG\tID:{reads}\tSM:{reads}'",
       sort       = "samtools",   # can be 'none', 'samtools' or 'picard'
       sort_order = "coordinate",   # can be 'coordinate' (default) or 'queryname'
       sort_extra = "-m 2G",   # extra args for samtools/picard
       # https://github.com/samtools/samtools/issues/603
    threads: 8  # succesfully tested with 16 threads on a system with 128 GB RAM
    # and with 8 threads on a system with 64 GB RAM; was killed with 16 threads on the 64 GB system
    wrapper:
       "v1.17.3/bio/bwa-mem2/mem"


# download the mandatory mappability map for Delly
rule download_mappability_map:
    output:
        PATH_PREFIX + "ref/Homo_sapiens.GRCh38.dna.primary_assembly.fa.r101.s501.blacklist.gz",
    shell:
        f"wget https://gear.embl.de/data/delly/Homo_sapiens.GRCh38.dna.primary_assembly.fa.r101.s501.blacklist.gz -P {PATH_PREFIX}ref/"


# adopt the mappabilty map to the naming scheme of GRCh38 primary assembly
# (GRCh38 primary assembly has 1, 2, ... instead of chr1, chr2, ... as chromosome identifiers
# and Delly expects a matching naming scheme of reference genome and mappability map.)
rule adopt_mmap_to_GRCh38_primary:
    input:
        ref  = f"{PATH_PREFIX}ref/{config['REFGENOME']}",
        mmap = PATH_PREFIX + "ref/Homo_sapiens.GRCh38.dna.primary_assembly.fa.r101.s501.blacklist.gz",
    output:
        PATH_PREFIX + "ref/Homo_sapiens.GRCh38.dna.primary_assembly.fa.r101.s501.blacklist_adopted.gz",
    params:
        temp_name = f"{PATH_PREFIX}ref/Homo_sapiens.GRCh38.dna.primary_assembly.fa.r101.s501.blacklist",
    shell:
        "zcat {input.mmap} > {params.temp_name} && sed -i 's/>chr/>/g' {params.temp_name} && bgzip < {params.temp_name} > {output}"


# demonstrates the use of Delly using a self-written Snakemake wrapper
rule delly_cnv_bcf:
    input:
       ref  = f"{PATH_PREFIX}ref/{config['REFGENOME']}",  # has to be bgzipped
       alns = PATH_PREFIX + "fastqs/mapped/{reads}.bam",  # has to be indexed
       mappability_map = PATH_PREFIX + "ref/Homo_sapiens.GRCh38.dna.primary_assembly.fa.r101.s501.blacklist_adopted.gz",  # has to be bgzipped
    output:
       PATH_PREFIX + "results/delly/CNVs_{reads}.bcf",  # BCF: binary variant call format (VCF)
    params:
       extra = "--cnv-size 50 --segmentation ",  # optional parameters for delly (except -g, -x)
    log:
       PATH_PREFIX + "logs/delly_{reads}.log",
    benchmark:
        repeat(f"{PATH_PREFIX}benchmarks/{config['REFGENOME']}_delly_cnv_bcf_" + "{reads}.txt", 1)
    threads: 1
    # "Delly primarily parallelizes on the sample level. Hence, [threads] should be always smaller or equal to the number of input samples."
    # https://github.com/dellytools/delly/blob/main/README.md#delly-multi-threading-mode
    wrapper:
       f"file://fastcash/conkmer/wrapper/delly_cnv"


# convert the Delly output from BCF to BED like TSV
rule convert_delly_output:
    input:
       PATH_PREFIX + "results/delly/CNVs_{reads}.bcf",
    output:
       PATH_PREFIX + "results/delly/CNVs_{reads}.tsv",
    threads: 1
    shell:
       " bcftools query"
       " -f '%CHROM\\t%POS\\t%INFO/END\\t[%CN]\\t[%RDCN]\\n' {input} | "
       " awk 'BEGIN{{FS = OFS = \"\\t\"}} {{ $1 = \"chr\"$1; $3 = $3 FS $3-$2 }}1' "
       " > {output}"  # "chr" is added back since conkmer call does this too
